'''
	This is a TimeConvert Class designed to 
'''

from datetime import date, datetime
import time
import calendar

class TimeConvert(object):
 '''
   Convert a timestamp as a string ( i.e. u'01.10.2014 02:06') to seconds in UTC/Local zone format
 '''

 @staticmethod
 def convert(timestamp, format='%d.%m.%Y %H:%M', utc=True):
	''' convert method '''

	# convert to  local time zone
	time_object_local = time.mktime(datetime.strptime(timestamp, format).timetuple())

	if (utc): return calendar.timegm(time.gmtime(time_object_local))

	return time_object_local

 @staticmethod
 def difference_days(timestamp1,timestamp2,format='%d.%m.%Y %H:%M'):
	''' return difference in seconds '''
	time1 = TimeConvert.convert(timestamp1,format=format)
	time2 = TimeConvert.convert(timestamp2,format=format)

	diff = float(time2 - time1)
	diff_days = diff/(3600*24)	
	
	return diff_days

 # http://stackoverflow.com/questions/765797/python-timedelta-in-years
 @staticmethod
 def age(imestamp1,timestamp2,format='%d.%m.%Y'):
        ''' return age of the person with birthday 'timestamp2' at the moment 'timestamp1' '''
        try:
            ed =  time.strptime(imestamp1, format)
            ld =  time.strptime(timestamp2, format)
        except ValueError:
            return -100 # -100 year :)
        
        #switch dates if needed
        if  ld < ed:
            ld,  ed = ed,  ld            

        res = ld[0] - ed [0]
        if res > 0:
            if ld[1]< ed[1]:
                res -= 1
            elif  ld[1] == ed[1]:
                if ld[2]< ed[2]:
                    res -= 1
        return res


if __name__ == '__main__':

 #Test 1: time conversion
 timestamp=u'01.10.2014 02:06'
 timestamp_utc=TimeConvert.convert(timestamp)
 timestamp_local=TimeConvert.convert(timestamp,utc=False)
 print timestamp
 print timestamp_utc
 print timestamp_local
 print datetime.fromtimestamp(timestamp_local)
 print datetime.fromtimestamp(timestamp_utc)


 # Test 2: time difference in days
 timestamp2=u'01.10.2014 12:06'
 timestamp3=u'02.10.2014 02:06'
 timestamp4=u'01.10.2014 14:06'

 print TimeConvert.difference_days(timestamp,timestamp2)
 print TimeConvert.difference_days(timestamp3,timestamp)
 print TimeConvert.difference_days(timestamp3,timestamp4)

 # Test 3: birthdays

 birthday="28.02.1986"
 timestamp="28.02.2013"
 assert  TimeConvert.age(timestamp,birthday) == 27
 
 birthday="30.03.1979" # my age
 timestamp="24.02.2015" # at this moment
 assert  TimeConvert.age(timestamp,birthday) == 35 


