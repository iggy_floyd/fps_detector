# -*- coding: utf-8 -*-

# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import numpy as np
import time 
import codecs
import requests
import json



class database(object):
	
	def __ini__(self): return

	def read(self,filename):
		start = time.time()
		#self.data = np.char.lower(np.loadtxt(open(filename,"rb"),delimiter="\t",skiprows=0,dtype=np.str,usecols=(0,1,2,4,5), converters={4:lambda s: s if s is not None else 'nan'}))
		self.data = np.char.lower(np.genfromtxt(open(filename,"rb"),delimiter="\t",skiprows=0,dtype=np.str,usecols=(0,1,2,4,5), filling_values='NaN'))
		print "%d eintries has been loaded for %.3f sec."%(self.data.shape[0], time.time()-start)
		return self

	def search(self,txt,field):
		a= np.where(txt == self.data)
		b= filter(lambda x: x[1]==field,  zip(*a))
		return np.array([ self.data[c[0]].tolist()  for c in b])



class GeoCodeCity(object):


	filename = 'worldcitiespop.txt'

	start = time.time()	
	data = np.char.lower(np.genfromtxt(open(filename,"rb"),delimiter=",",skiprows=1,dtype=np.str,usecols=(0,1,5,6,8), filling_values='NaN'))
        print "%d eintries has been loaded for %.3f sec."%(data.shape[0], time.time()-start)

	@staticmethod
	def searchGeoCodes(city,field):
		city = city.lower()
		#print unicode(city).decode()
		a= np.where(city == GeoCodeCity.data)
#		a= np.where(np.core.defchararray.find(GeoCodeCity.data,city)>=0)
		print a
                b= filter(lambda x: x[1]==field,  zip(*a))
		res=np.array([ GeoCodeCity.data[c[0]].tolist()  for c in b])
		print res
		if (len(res)==0): return (None,None)
		lat = float(res[0][3]) if  res[0][3] !='NaN' else None
		long = float(res[0][4]) if  res[0][4] !='NaN' else None
                
                
                
		return (lat,long)
	
	@staticmethod
	def searchGeoCodes_v2(city,field):
		city = city.lower()
		#print unicode(city).decode()
		a= np.where(city == GeoCodeCity.data)
#		a= np.where(np.core.defchararray.find(GeoCodeCity.data,city)>=0)
		print a
                b= filter(lambda x: x[1]==field,  zip(*a))
		res=np.array([ GeoCodeCity.data[c[0]].tolist()  for c in b])
		print res
		if (len(res)==0): return (None,None)
		lat = float(res[0][3]) if  res[0][3] !='NaN' else None
		long = float(res[0][4]) if  res[0][4] !='NaN' else None
                
                
                
		return (lat,long)



if __name__ == "__main__":

	
#	db = database().read('allCountries.txt')
#	sys.exit()

#	print "eichwalde at pos 1", db.search('eichwalde',1)
#	print "eichwalde at pos 2", db.search('eichwalde',2)

#	print "eichwalde at pos 1", GeoCodeCity.searchGeoCodes('Eichwalde',1)
#	print "eichwalde at pos 2", GeoCodeCity.searchGeoCodes('Eichwalde',2)
	
#	print "Schönau at pos 1",GeoCodeCity.searchGeoCodes(u'Schönau',1)
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'Schönau',2)

#'\u00f6'


#	print  GeoCodeCity.data
#	print GeoCodeCity.data[31610]
#	print "Schönau at pos 1",GeoCodeCity.searchGeoCodes('Sch\u00f6nau',1)
	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes('Schonau',1)
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'Sch\u00f6nau'.encode('utf-8'),1)
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'G\u00f6tzis'.encode('utf-8'),1)
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'M\u00fcnchen'.encode('utf-8'),2)
#	print "Schönau at pos 2",GeoCodeCity_v2.searchGeoCodes(u'M\u00fcnchen'.encode('utf-8'),2)
        
        
        