import speedy
from speedy import zeromq

SERVER_PORT = 8090

client = speedy.Client(zeromq.client_socket(('127.0.0.1', SERVER_PORT)))

# requests are arbitrary python objects
request = { 'foo' : 123, 'bar' : 456 }

future = client.foo(request)

# Wait for the result.   If the server encountered an error,
# an speedy.RemoteException will be thrown. 
result = future.wait()
print result
