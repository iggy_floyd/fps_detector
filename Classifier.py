#! /usr/bin/python



#  File:   Classifier.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on May 5, 2015, 11:41:34 AM


'''

    Classifier contain a list of the sklearn classifiers.
    It trains them.
    It makes a prediction.
    It returns the prediction in a JSON format
    


'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$May 5, 2015 11:41:34 AM$"


# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

from sklearn.ensemble import RandomForestClassifier # random forest


from Logger import Logger,mylogging
import datetime
import time
import shutil
import dill
import os.path
import json



class Classifier(Logger):
    def __init__(self,process_name='fps_test'):
        #super(Logger, self).__init__()
        Logger.__init__(self)
        self.clfs = {}
        self.process_name = process_name if (type(process_name) is str) and (len(process_name)>0) else 'fps_fraud_classification' 
        
        self.addEventHandler('ClassifierAdd')        
        
        self.addEventHandler('ClassifierTrain')
        self.addEventHandler('ClassifierDump')
        self.addEventHandler('ClassifierRestore')
        self.addEventHandler('ClassifierBackup')
        
        self.status_ready=False
        
        
    def  addClassifier(self,name,clf): 
        

        if name is None: raise ValueError('CLASSIFIER_ADD_WRONG_NAME')
        if not((type(name) is str) or (type(name) is  unicode)): raise ValueError('CLASSIFIER_ADD_WRONG_NAME')
        if len(name) == 0: raise ValueError('CLASSIFIER_ADD_WRONG_NAME')
        
        if (clf is None): raise ValueError('CLASSIFIER_ADD_WRONGTYPE_PREDICTOR')
        
        self.clfs[name] = clf
        self.write2Log(type='ClassifierAdd',msg='the classifier %s of type %s is added'%(name,type(clf)))
        mylogging("Add classifier ",name,' ',repr(clf), "....")
        
        return self
    
    
    def fit(self,clfs,X,y):
        self.status_ready=False # to forbid predictions during the fit classifiers        
        for key in clfs:
            mylogging("Fit classifier ",key, "....")
            if not(key in self.clfs.keys()): continue            
            self.clfs[key].fit(X,y)
            
        self.write2Log(type='ClassifierTrain',msg='the classifiers %s were trained'%(self.process_name))
        self.status_ready=True
        #return self # not needed any more as we use proxy for background process
    
  
    def dump(self):
        
            if not(self.status_ready): raise ValueError('CLASSIFIER_NOT_READY')
            with open(self.process_name+'_classifiers.pkl', 'wb') as f:
                dill.dump(self.clfs,f)                        
            self.write2Log(type='ClassifierDump',msg='the classifier set %s was dumped'%self.process_name)            
            return self


    def restore(self):        
            if os.path.exists(self.process_name+'_classifiers.pkl'):
                with open(self.process_name+'_classifiers.pkl', 'rb') as f:
                    self.clfs=dill.load(f)                      
                self.write2Log(type='ClassifierRestore',msg='the classifier set %s was restored'%self.process_name)            
            else:
                self.write2Log(type='ClassifierRestore',msg="Error: the classifier set %s could n't be restored because there is no file"%self.process_name)            

            self.status_ready=True
            return self

    
    def backup(self,msg):
            # get a suffix for the backup file name
            
            if not(self.status_ready): raise ValueError('CLASSIFIER_NOT_READY')
            if not(os.path.exists(self.process_name+'_classifiers.pkl')): raise ValueError('CLASSIFIER_NOT_DUMPED')
                
            suffix =  datetime.datetime.now().strftime("_%H_%M_%d_%m_%Y")
            # backup
            try:                
                shutil.copy(self.process_name+'_classifiers.pkl', self.process_name + suffix +'_classifiers.pkl.bak')          
                # logging
                self.write2Log(type='ClassifierBackup',msg=msg)            
            except shutil.Error as e:
                print('Error: %s' % e)
                raise ValueError('CLASSIFIER_BACKUP_FAILER')
        
            return self
        


    def predict(self,X):
        
        
        preds={}
        for key in self.clfs.keys():            
            
            preds[key]=self.clfs[key].predict(X).tolist()
                        
        return json.dumps(preds)



    def predict_proba(self,X):
        
        preds={}
        for key in self.clfs.keys():            
            preds[key]=self.clfs[key].predict_proba(X).tolist()
                        
        return json.dumps(preds)


        

if __name__ == "__main__":
    
    
    filename = sys.argv[1]  if len(sys.argv)>1 else "fps_data_3.json"
    
    
    from DatasetTransformer import DatasetTransformer    
    from ModelFeatures import ModelFeatures
    
    # Test 0: create DatasetTransformer
    DT=DatasetTransformer()
    print "Process '%s' is started"%DT.process_name

    
    # Test 1: create a dataframe
    DT.initDataFrame(filename,update=False)

    # Test 2: transformers of the ModelFeature to train
    MF=ModelFeatures(dataframe=DT.dataframe)
    MF.initModelFeatures()
    
    # Test 3: create,train and predict 
    classifier = Classifier()
    classifier.addClassifier(
                 'RandomForest',
                  #RandomForestClassifier(max_features='log2', n_estimators=100, min_samples_split=1,  n_jobs=-1)
                  RandomForestClassifier(max_features='log2', n_estimators=50, min_samples_split=1,  n_jobs=-1)
            )

    _,train_cols,y,X=MF.getTrainSet('status')
    print train_cols
    classifier.fit(['RandomForest'],X,y)
    
    for i,x in enumerate(X):
        print "x=",x
        print "true=",y[i]
        print classifier.predict([x])
        print classifier.predict_proba([x])
     
    # Test 4: dump and restore
    classifier.dump()
    classifier.restore()
    
    for i,x in enumerate(X):
        print "x=",x
        print "true=",y[i]
        print classifier.predict([x])
        print classifier.predict_proba([x])
    
    # Test 5: backup#
    classifier.backup('for testing purpose')
    
    
    # Test 6:   JSON proceeding 
    with open(filename,'r') as f:
        for line in f:
             #print line
             start=time.time()
             data=DT.processJSON(line)                          
             print "time (in s) for data transformation required:", time.time() -start
             print data.head(1)
             start=time.time()
             print '\n\n'
             data=MF.transform_data(data)                          
             print "time (in s) for model feature creation required:", time.time() -start
             print data.head(1)
             start=time.time()
             X = data[MF.train_cols].as_matrix()
             #print "time (in s) classification required (1):", time.time() -start
             res=classifier.predict(X)
             print "time (in s) classification required (2):", time.time() -start
             print res