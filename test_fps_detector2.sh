PORT=8092


echo
echo
echo "init_process"
echo
#curl -i -X POST  -d '{"jsonrpc": "2.0", "id": "1", "method": "init_process","params":["fps_data_3.json"]}' -H 'content-type: text/plain;' http://localhost:$PORT 

echo
echo
echo "init_model_features"
echo
#curl -i -X POST  -d '{"jsonrpc": "2.0", "id": "2", "method": "init_model_features","params":[]}' -H 'content-type: text/plain;' http://localhost:$PORT 

echo
echo
echo "add_classifier"
echo
#curl -i -X POST  -d '{"jsonrpc": "2.0", "id": "3", "method": "add_classifier","params":["RandomForest", "RandomForest1"]}' -H 'content-type: text/plain;' http://localhost:$PORT 
#curl -i -X POST  -d '{"jsonrpc": "2.0", "id": "4", "method": "add_classifier","params":["RandomForest", "RandomForest2"]}' -H 'content-type: text/plain;' http://localhost:$PORT 

echo
echo
echo "train_classifiers"
echo
#curl -i -X POST  -d '{"jsonrpc": "2.0", "id": "5", "method": "train_classifiers","params":[]}' -H 'content-type: text/plain;' http://localhost:$PORT 
echo


IFS=$'\n'

lines=($(cat fps_data_4.json))

#lines=($(cat fps_data_3.json))
#lines=($(cat fps_data_5.json))
#lines=($(sed -n '1,3p' fps_data_3.json))
#lines=($(sed -n '/{.*}/p' fps_data_3.json))
#lines=($(sed -n '/{.*}/p' fps_data_3.json))

for line in ${lines[@]}
do

#    echo "line=" "$line"
    echo 
    echo 

    cmd=$(printf "{\"jsonrpc\": \"2.0\", \"id\": \"7\", \"method\": \"predict\",\"params\":[%s]}" "$line" )
    #echo "cmd=" "$cmd"
    echo 
    echo 

    #START=$(date +%s)
    #str="{\"portal\":\"1\"}",
    #curl -i -X POST  -d "{'jsonrpc': '2.0', 'id': '6', 'method': 'predict','params':[$str]}" -H 'content-type: text/plain;' http://localhost:$PORT 
    time curl -i -X POST  -d "$cmd" -H 'content-type: text/plain;' http://localhost:$PORT 
    #curl -i -X POST  -d '{"jsonrpc": "2.0", "id": "6", "method": "predict","params":[{"portal":1}]}' -H 'content-type: text/plain;' http://localhost:$PORT 
#    time curl -i -X POST  -d "{'jsonrpc': '2.0', 'id': '6', 'method': 'predict','params':[${line}]}" -H 'content-type: text/plain;' http://localhost:$PORT 
    #END=$(date +%s)
    #echo $(( $END - $START ))
done
