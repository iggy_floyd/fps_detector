#! /usr/bin/python


import jsonrpclib
SERVER_PORT = 8092

import json
from time import sleep,time


def main():
        ''' a driver doing some work on our JSONRPC service '''

	s = jsonrpclib.Server("http://127.0.0.1:%d"%SERVER_PORT)

        # do ini of the process using the file fps_data_3.json
        
        try:
            print 'Ini of the dataframe'
            print s.init_process('fps_data_3.json')
        except: pass

        print 'Ini of the model parameters'
        while True:
            try:
                # do ini of the model features
                res= s.init_model_features()
                print res
                if  json.loads(res)['status'] == 0: break
            except: pass    
        

	        
        # add classifier  two 'RandomForest'
        print 'add two classifiers'
        try:
            print s.add_classifier(type='RandomForest',name='RandomForest1')
            print s.add_classifier(type='RandomForest',name='RandomForest2')
        except: pass
        
        print 'train two classifiers'
        while True:
            try:
                # train classifiers
                res= s.train_classifiers()
                print res
                if  json.loads(res)['status'] == 0: break
            except: pass
        
        #simulate detection of the frauds        
        print 'simulate FPS requests'
        with open('fps_data_3.json','r') as f:
            for line in f:
              print "status=",json.loads(line)['status']
              while True:
                  try:
                    start = time()
                    res = s.predict(line)
                    print res
                    if  json.loads(res)['status'] == 0: 
                        print time()-start  
                        break
                  except: pass
        
        #simulate detection of the frauds        
        print 'simulate FPS requests2'
        with open('fps_data_4.json','r') as f:
            for line in f:
              print "status=",json.loads(line)['status']
              while True:
                  try:
                    start = time()
                    res = s.predict(line)
                    print time()-start
                    print res
                    if  json.loads(res)['status'] == 0:                       
                        break
                  except: pass


if __name__ == '__main__':
    main()



