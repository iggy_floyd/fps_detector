#! /usr/bin/python



#  File:   TransformWrapper.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on May 3, 2015, 12:20:56 PM




__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$May 3, 2015 12:20:56 PM$"



import sys

sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9



import matplotlib 
import numpy as np
import matplotlib.pyplot as pl


class TransformWrapperLambda(object):
    def __init__(self,func) :
        self.func=func
    
    def fit(self,X,y=None):
        return self

    def fit_transform(self,X,y=None,**kwargs):
        return map(lambda x: self.func(x), X)

    def transform(self,X):
        return map(lambda x: self.func(x), X)
    
class TransformWrapperTransformer(object):
    def __init__(self,obj) :
        self.obj=obj
    
    def fit(self,X,y=None):
        self.obj.fit(X)
        return self

    def fit_transform(self,X,y=None,**kwargs):
        self.obj.fit(X)
        return map(lambda x: self.obj.transform(x), X)

    def transform(self,X):        
        return map(lambda x: self.obj.transform(x), X)








if __name__ == "__main__":
    
    
    
	print "Hi"        
        
