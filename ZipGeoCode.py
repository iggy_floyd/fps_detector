# -*- coding: utf-8 -*-


# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import pandas as pd
import time 
import numpy as np


class ZipGeoCode(object):
    names=[
        "country code", 'postal code','place name','admin name1','admin code1', 'admin name2',
        'admin code2' , 'admin name3','admin code3','latitude','longitude','accuracy']
    start = time.time()
    data = pd.read_csv('allCountries.txt',na_values=[' '],keep_default_na = False,sep='\t',header=None,names=names,
    #encoding='utf-8', 
    dtype=np.str)
    
    print "processed %s : %d eintries has been loaded for %.3f sec."%('allCountries.txt',len(data), time.time()-start)
    
    # lowering
    for name in names:
        #data[name] = data[name].map(unicode.lower)
        data[name] = data[name].map(str.lower)
    
    start = time.time()
    data_array = data[['place name','country code','postal code','latitude','longitude']].as_matrix().astype(np.str)
    print "processed %s : %d eintries has been loaded to array for %.3f sec."%('allCountries.txt',len(data), time.time()-start)
    
    
    
    @staticmethod
    def searchGeoCodesCity_v2(city,country):  
        city=city.lower()
        country = country.lower()        
        
        a= np.where(city == ZipGeoCode.data_array )  
        res=ZipGeoCode.data_array[ [aa for aa in a[0]] ]        
        res=filter(lambda x: x[1] == country, res)
        if (len(res)==0): return (None,None)
        
	lat = float(res[0][3]) if  res[0][3] !='NaN' else None
	long = float(res[0][4]) if  res[0][4] !='NaN' else None 
         
        
        return (lat,long)
    
    
    @staticmethod
    def searchGeoCodesCity(city,country):  
        city=city.lower()
        country = country.lower()        
        result = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['place name'] == city) & (ZipGeoCode.data['country code'] == country)].index,['latitude','longitude']]
        
        
        #print "in ZipGeoCode::searchGeoCodesCity",city
        #print "in ZipGeoCode::searchGeoCodesCity",country
        #print result
        if len(result)==0: return (None,None)
        
        return (float(result['latitude'].as_matrix()[0]),float(result['longitude'].as_matrix()[0]))
        
    @staticmethod
    def searchGeoCodesPostal(postal,country):  
        
        postal = str(postal)
        country = country.lower()     
        result = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['postal code'] == postal) & (ZipGeoCode.data['country code'] == country)].index,['latitude','longitude']]
        
        
        
        if len(result)==0: return (None,None)
        
        return (float(result['latitude'].as_matrix()[0]),float(result['longitude'].as_matrix()[0]))
    
    @staticmethod
    def ValidatePostal(postal,country,city):  
        
        postal = str(postal)
        country = country.lower()    
        city=city.lower()
        result1 = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['postal code'] == postal) & (ZipGeoCode.data['country code'] == country)].index,['country code']]
        result2 = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['postal code'] == postal) & (ZipGeoCode.data['place name'] == city)].index,['place name']]
        
        
        return (int(len(result1)>0),int(len(result2)>0))
    

#### Too SLOW!!!


if __name__ == "__main__":
    

    start = time.time()
    print ZipGeoCode.searchGeoCodesCity(u'München'.encode('utf-8'),'DE')
    print time.time() -start
    
    start = time.time()
    print ZipGeoCode.searchGeoCodesCity_v2(u'München'.encode('utf-8'),'DE')
    print time.time() -start
    
    start = time.time()
    print ZipGeoCode.searchGeoCodesPostal(15732,'DE')
    print time.time() -start
    
    #print ZipGeoCode.ValidatePostal(15732,'DE','Eichwalde')
    #print ZipGeoCode.ValidatePostal(15732,'DE','')
