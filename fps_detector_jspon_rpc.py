from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

from jsonrpc import JSONRPCResponseManager, dispatcher




import sys

SERVER_PORT = 8092

from SocketServer import ThreadingMixIn

from Logger import mylogging


import traceback

import time
import multiprocessing as mp
import copy
import json

from DatasetTransformer import DatasetTransformer    
from ModelFeatures import ModelFeatures
from Classifier import Classifier
from sklearn.ensemble import RandomForestClassifier # random forest

from ProcessPool import ProcessPool,Worker,ProcessManager
from multiprocessing.managers import NamespaceProxy







# to lock common resources
init_dataset_lock =  mp.Lock()
new_process_name_lock = mp.Lock()
init_model_features_lock = mp.Lock()
train_classifiers_lock = mp.Lock()
add_classifier_lock = mp.Lock()
dump_classifiers_lock = mp.Lock()
backup_lock = mp.Lock()
prediction_in_progress_lock = mp.Lock()


# here we define the proxy and manager to communicate with our background processes via shared memory
class DatasetTransformerProxy(NamespaceProxy):
    _exposed_ = ('__getattribute__', '__setattr__', '__delattr__', 
        'initDataFrame','processJSON' # only needed method (which will take a lot of CPU-time) to declare in the proxy
        )
    

    def  initDataFrame(self,file_json='',update=False,doBackup=True):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('initDataFrame',[file_json,update,doBackup])
    
    def processJSON(self,json_data):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('processJSON',[json_data])
    


class ModelFeaturesProxy(NamespaceProxy):
    _exposed_ = ('__getattribute__', '__setattr__', '__delattr__', 
        'initModelFeatures','getTrainSet','transform_data' # only needed method (which will take a lot of CPU-time) to declare in the proxy
        )
    

    
    def  initModelFeatures(self,update=False,doBackup=True):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('initModelFeatures',[update,doBackup])
    
    
    def  getTrainSet(self,target):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('getTrainSet',[target])
    
    def transform_data(self,data):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('transform_data',[data])
    
    

class ClassifierProxy(NamespaceProxy):
    _exposed_ = ('__getattribute__', '__setattr__', '__delattr__', 
        'fit','addClassifier','predict','predict_proba' # only needed methods (which will take a lot of CPU-time) to declare in the proxy
        )
    

    
    def  fit(self,clfs,X,y):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('fit',[clfs,X,y])
    
    def  addClassifier(self,name,clf): 
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('addClassifier',[name,clf])

    def  predict(self,X):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('predict',[X])
    

    def  predict_proba(self,X):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('predict_proba',[X])
    


ProcessManager.register('DatasetTransformer', DatasetTransformer, DatasetTransformerProxy)      
ProcessManager.register('ModelFeatures', ModelFeatures, ModelFeaturesProxy)      
ProcessManager.register('Classifier', Classifier, ClassifierProxy)      




class Process(object):
    '''     
        The 'Process' class:  
                * provides different methods of JSONRPCServer (dispatching)
                * runs data transformation, training, dumping etc
                * call classifiers to make predictions and return result
    
    '''
    
    
    proces_name='fps_fraud_classification'
   

    
    # here add possible common resources
    DatasetTransformer = None
    ModelFeatures = None
    Classifier = None
    Classifier_names = []
    
    # Init a Process pool with the desired number of processes
    pool = ProcessPool(5)
    
    
    PM=ProcessManager()
    PM.start()
    
    #Classifier_status = 0 # 0/1 -- no progress of prediction/prediction in progress
    
    @staticmethod
    @dispatcher.add_method
    def set_process_name(name):
        global new_process_name_lock
        new_process_name_lock.acquire()
        Process.proces_name = name
        new_process_name_lock.release()
        return json.dumps({'proces_name':name,'status':0})
    
    @staticmethod
    @dispatcher.add_method
    def get_process_name(name):        
        return json.dumps({'proces_name':Process.proces_name,'status':0})
    

    @staticmethod
    @dispatcher.add_method
    def init_process(filename):

        try: 
        #if (True):
            global init_dataset_lock
            
            init_dataset_lock.acquire()      
            
            # an old version: a signle process
            #Process.DatasetTransformer = DatasetTransformer(process_name=Process.proces_name)
            #Process.DatasetTransformer.initDataFrame(filename,update=False)
            
#            raise Exception('Test')
            
            # a new version: a mutiprocessing in place
            # use background processes for initialization                                        
            Process.DatasetTransformer = Process.PM.DatasetTransformer(process_name=Process.proces_name)                                                
            Process.pool.add_task(Process.DatasetTransformer.initDataFrame, filename,update=False)            
            
            
            init_dataset_lock.release()
        except ValueError,e:     
            init_dataset_lock.release()
            return json.dumps({'status':1,'error':str(e)}) # 0 if no errors, 1 otherwise
         #   return json.dumps({'status':1})
        
        
        
        return json.dumps({'status':0}) # 0 if no errors, 1 otherwise

    @staticmethod
    @dispatcher.add_method
    def init_model_features():
        
        
        try:
            global init_model_features_lock
            
            init_model_features_lock.acquire()
            if (Process.DatasetTransformer  is None): raise ValueError('DATASETTRANSFORMER_NOT_READY')
            if not(Process.DatasetTransformer.status_ready): raise ValueError('DATASETTRANSFORMER_NOT_READY')
            
            
            # an old version: a signle process 
            #Process.ModelFeatures = ModelFeatures(process_name=Process.proces_name,dataframe=Process.DatasetTransformer.dataframe)            
            #Process.ModelFeatures.initModelFeatures()  
            
            
            # a new version: a mutiprocessing in place
            # use background processes for initialization
            
            Process.ModelFeatures = Process.PM.ModelFeatures(process_name=Process.proces_name,dataframe=Process.DatasetTransformer.dataframe)   
            Process.pool.add_task(Process.ModelFeatures.initModelFeatures)
            
            
            
            init_model_features_lock.release() 
        except ValueError,e:
            init_model_features_lock.release()
            #raise ValueError(str(e))
            return json.dumps({'status':1,'error':str(e)}) # 0 if no errors, 1 otherwise
        
        return json.dumps({'status':0}) # 0 if no errors, 1 otherwise



    @staticmethod
    @dispatcher.add_method
    def add_classifier(type = 'RandomForest',name='RandomForest'):
        
        
        try:
        
        #if (True):

        
            
            global add_classifier_lock
            
            add_classifier_lock.acquire()
            if (Process.Classifier is None):

                #add_classifier_lock.acquire()
                
                # a single-processed version
                #Process.Classifier = Classifier()
                
                # a multiprocessed version
                Process.Classifier = Process.PM.Classifier()
                
                #add_classifier_lock.release()

        
         
            if (name in Process.Classifier_names):                    
                raise ValueError('CLASSIFIER_NAME_IN_PLACE')
                   

            if (type == 'RandomForest' ):
            
                #print "adding..."
                Process.Classifier.addClassifier(
                     name,
                     #RandomForestClassifier(max_features='log2', n_estimators=40, min_samples_split=1,  n_jobs=-1)
                     RandomForestClassifier(n_estimators=100,max_features='log2', min_samples_split=1)
                )            
                #add_classifier_lock.acquire()
                Process.Classifier_names  += [name]
                
            add_classifier_lock.release()

        
        except ValueError,e:
            add_classifier_lock.release()                        
            return json.dumps({'status':1,'error':str(e)}) # 0 if no errors, 1 otherwise
            
            #return json.dumps({'status':0}) # 0 if no errors, 1 otherwise
         
        return json.dumps({'status':0}) # 0 if no errors, 1 otherwise


    @staticmethod
    @dispatcher.add_method
    def train_classifiers():
        
        try:
        
            global train_classifiers_lock
            train_classifiers_lock.acquire()
            
            if (Process.Classifier is None):  raise ValueError('CLASSIFIER_EMPTY')
            if (Process.ModelFeatures  is None):  raise ValueError('MODELFEATURES_NOT_READY')
            if not(Process.ModelFeatures.status_ready): raise ValueError('MODELFEATURES_NOT_READY')
                       
            
            
            
            
            # so far only simple training using the whole datasets            
            _,_,y,X=Process.ModelFeatures.getTrainSet('status')
            
            # a single-processed version
            #Process.Classifier.fit(Process.Classifier_names,X,y)            
            
            
            
            # a multiprocessed version
            # use background processes for initialization                        
            Process.pool.add_task(Process.Classifier.fit,Process.Classifier_names,X,y)            
            
            train_classifiers_lock.release()
  
        except Exception,e:
            train_classifiers_lock.release()
            return json.dumps({'status':1,'error':str(e)}) # 0 if no errors, 1 otherwise
        
        return json.dumps({'status':0}) # 0 if no errors, 1 otherwise


    @staticmethod
    @dispatcher.add_method
    def predict(data):
        
        try:
        #if (True):

            
                    
            
            if (Process.DatasetTransformer  is None): raise ValueError('DATASETTRANSFORMER_NOT_READY')
            if not(Process.DatasetTransformer.status_ready): raise ValueError('DATASETTRANSFORMER_NOT_READY')

            if (Process.ModelFeatures  is None):  raise ValueError('MODELFEATURES_NOT_READY')
            if not(Process.ModelFeatures.status_ready): raise ValueError('MODELFEATURES_NOT_READY')

            if (Process.Classifier is None):  raise ValueError('CLASSIFIER_NOT_READY')
            if not(Process.Classifier.status_ready): raise ValueError('CLASSIFIER_NOT_READY')

            
            #print "(0) data = ",data
            data = Process.DatasetTransformer.processJSON(data)     
            #print "(1) data = ",data.head()
            data=Process.ModelFeatures.transform_data(data)
            #print "(2) data = ",data.head()
            #y = data['status'].as_matrix()
            X = data[Process.ModelFeatures.train_cols].as_matrix()
            res=Process.Classifier.predict(X)
                          
        except ValueError,e:
            return json.dumps({'status':1,'prediction':[],'error':str(e)}) # 0 if no errors, 1 otherwise
        
        
        return json.dumps({'status':0,'prediction':res}) # 0 if no errors, 1 otherwise






@dispatcher.add_method
def predict_validation(data,true_value):
        ''' makes simple validation test '''
    
        mylogging("predict validation  starts ....")
        try:
        
    
            if (Process.DatasetTransformer  is None): raise ValueError('DATASETTRANSFORMER_NOT_READY')
            if not(Process.DatasetTransformer.status_ready): raise ValueError('DATASETTRANSFORMER_NOT_READY')

            if (Process.ModelFeatures  is None):  raise ValueError('MODELFEATURES_NOT_READY')
            if not(Process.ModelFeatures.status_ready): raise ValueError('MODELFEATURES_NOT_READY')

            if (Process.Classifier is None):  raise ValueError('CLASSIFIER_NOT_READY')
            if not(Process.Classifier.status_ready): raise ValueError('CLASSIFIER_NOT_READY')

            
            #print "(0) data = ",data
            data = Process.DatasetTransformer.processJSON(data)     
            #print "(1) data = ",data.head()
            data=Process.ModelFeatures.transform_data(data)
            #print "(2) data = ",data.head()
            #y = data['status'].as_matrix()
            X = data[Process.ModelFeatures.train_cols].as_matrix()
            res=Process.Classifier.predict(X)
                          
        except ValueError,e:
            mylogging("no predictions, something wrong happened ... ")
            return json.dumps({'status':1,'prediction':[],'true_value':true_value,'error':str(e)}) # 0 if no errors, 1 otherwise
        
        
        mylogging('predicted :',repr(res),' | expected: ',true_value)
        return json.dumps({'status':0,'prediction':res,'true_value':true_value}) # 0 if no errors, 1 otherwise






@Request.application
def application(request):
    # Dispatcher is dictionary {<method_name>: callable}
    #dispatcher["echo"] = lambda s: s
    #dispatcher["add"] = lambda a, b: a + b


    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')



if __name__ == '__main__':
#    run_simple('localhost', SERVER_PORT, application,processes=1,threaded=True) # too slow
#    run_simple('localhost', SERVER_PORT, application,processes=1,threaded=False)
    run_simple('localhost', SERVER_PORT, application)
