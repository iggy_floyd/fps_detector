#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 23, 2015 11:33:03 AM$"


import logging
import sys
import inspect 

class Logger(object):
    ''' Logger to log events in FPS detector '''
    
    
    userEventID=115
    
                            
    def __init__(self,LOG_FILENAME = 'history.log',
            format='+'*100 + '\n'+'|%(asctime)s|%(levelname)s|%(message)s \n' +'-'*100,
            datefmt='%d.%m.%Y %H:%M:%S',
            level=logging.DEBUG
            ):
        self.LOG_FILENAME = LOG_FILENAME
        self.format=format
        self.datefmt=datefmt
        #self.level = logging.DEBUG
        self.level = Logger.userEventID
        self.handlers={}
        
        logging.basicConfig(filename= self.LOG_FILENAME,
                            filemode='a',
                            format=self.format,
                            datefmt=self.datefmt,
                            level=115)
        

        
                        
    def addEventHandler(self,EventName='Event',id=None,storeHandler=True):
        ''' add new handler of logging Events of type EventName'''
        
        
        
        if (storeHandler): 
            self.handlers[EventName.lower()]=(EventName,id)
        setattr(logging,EventName.upper(),Logger.userEventID if ((id is None) or (type(id) != int))  else id)

        logging.addLevelName(getattr(logging,EventName.upper()), EventName.upper())
        
        setattr(
                    logging.Logger,
                    EventName.lower(),  
                    lambda inst, msg, *args, **kwargs: inst.log(getattr(logging,EventName.upper()), msg, *args, **kwargs)
        
                )
        setattr(
                logging,
                EventName.lower(),
                lambda msg, *args, **kwargs: logging.log(getattr(logging,EventName.upper()), msg, *args, **kwargs)
                )
                
        
        Logger.userEventID+=1 if ((id is None) or (type(id) != int)) else 0
        return self
        
        
    def write2Log(self,type='BACKUP',msg='Some action was done'):
        ''' make the writting to the log file about some action,
            i.e backuping or updating    '''
        
        
        if not(hasattr(logging,type.lower())) and (type.lower() in self.handlers):
            self.addEventHandler(EventName=self.handlers[type.lower()][0],id=self.handlers[type.lower()][1],storeHandler=False)
        
        
        # writting to log file     
        
        getattr(logging,type.lower())(msg)
        #getattr(logging.Logger,type.lower())(msg)
        
        return self
    
    
    def cleanLog(self):        
        ''' clean the content of the log file '''
        
        open(self.LOG_FILENAME,'w').close()
    




class LogOut(object):
    def __init__(self, filename="logs",category='Unknown'):
        self.terminal = sys.stdout
        self.category = category
        self.log = open(filename, "a")

    def write(self, message):
	if not message: return
	if (len(message)==0): return
        self.terminal.write(self.category+': '+message+'\n')
        
        #self.terminal.flush()
        self.log.write(self.category+': '+message+'\n')
        #self.log.flush()
        


def mylogging(*kargs):
    print ' '.join(kargs)
    print >>open('logs','a'),  inspect.stack()[2][3],': ' ,' '.join(kargs)
    


if __name__ == "__main__":
    
    
    
    # Test
    logger = Logger()
    logger.addEventHandler('Update_Database')
    logger.addEventHandler('Backup_Database')
    logger.addEventHandler('Transform_JSON')
    logger.addEventHandler('Transform_Database')
    logger.addEventHandler('Transform_Variable')
    
    logger.write2Log('Update_Database','Database is updated')
    logger.write2Log('Transform_Database','Database is transformed')
    logger.write2Log('Transform_Variable','Database is transformed using binary transformation')
    
    
    print open(logger.LOG_FILENAME).readlines()
    logger.cleanLog()
    print open(logger.LOG_FILENAME).readlines()
    
