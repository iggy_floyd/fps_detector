function bin2hex(s) {
  //  discuss at: http://phpjs.org/functions/bin2hex/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  // bugfixed by: Linuxworld
  // improved by: ntoniazzi (http://phpjs.org/functions/bin2hex:361#comment_177616)
  //   example 1: bin2hex('Kev');
  //   returns 1: '4b6576'
  //   example 2: bin2hex(String.fromCharCode(0x00));
  //   returns 2: '00'

  var i, l, o = '',
    n;

  s += '';

  for (i = 0, l = s.length; i < l; i++) {
    n = s.charCodeAt(i)
      .toString(16);
    o += n.length < 2 ? '0' + n : n;
  }

  return o;
}


function HEX2DEC(number) {
// Return error if number is not hexadecimal or contains more than ten characters (10 digits)
if (!/^[0-9A-Fa-f]{1,10}$/.test(number)) return '#NUM!';
 
// Convert hexadecimal number to decimal
var decimal = parseInt(number, 16);
 
// Return decimal number
return (decimal >= 549755813888) ? decimal - 1099511627776 : decimal;
}



function getCanvasFingerprint () {
      var canvas = document.createElement('canvas');
      var ctx = canvas.getContext('2d');
      // https://www.browserleaks.com/canvas#how-does-it-work
      var txt = 'http://valve.github.io';
      ctx.textBaseline = "top";
      ctx.font = "14px 'Arial'";
      ctx.textBaseline = "alphabetic";
      ctx.fillStyle = "#f60";
      ctx.fillRect(125,1,62,20);
      ctx.fillStyle = "#069";
      ctx.fillText(txt, 2, 15);
      ctx.fillStyle = "rgba(102, 204, 0, 0.7)";
      ctx.fillText(txt, 4, 17);
      return canvas.toDataURL();
    }


function myfingerprint()
{


if (!window.btoa) window.btoa = base64.encode
if (!window.atob) window.atob = base64.decode

var b64 = getCanvasFingerprint().replace("data:image/png;base64,","");
var bin = atob(b64);


var crc = bin2hex(bin.slice(-16,-12));

return HEX2DEC(crc);

}
