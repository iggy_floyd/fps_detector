<?php

// some trick: Now a model can load  any plugin :-)
class Form_Booking_Model extends TinyMVC_Controller
{

	function get_form() 
	{


		         $this->load->library('JForm_Wrapper','jform');


/// form to create the read request


			 $this->jform->setForm(
           		     array(
			'focus' => "", // focus first element
			    // 'anchor' => "", // submit url will point to anchor at $form.begin
			    // 'enctype' => "multipart/form-data", // for file input
			    'submit_label' => "Process",
			    'inputs' => array(
			        'ID' => array(
			            'validators' => new NumericValidator(),
			            'required' => "true",
			            'maxlength' => 10, // automatically adds a CutOffFilter
			            'style' => "border-color: blue;", // custom options for the <input> tag are added simply by
			            // adding them to this array
					        ),
			        'Depth' => array(
				       'validators' => new NumericValidator(),
				            'required' => "true",
			            'maxlength' => 10, // automatically adds a CutOffFilter
			            'style' => "border-color: blue;", // custom options for the <input> tag are added simply by
			            // adding them to this array
				        ),
		        'Field' => array(

		            'type' => "select",
			            'options' => array(
			                'Unknown' => array(),
			                'BookerName' => array(),
			                'TravelerName' => array(),
			                'BookerEmail' => array(),
			                'TravelerEmail' => array(),
			            ),
		            'style' => "border-color: green;", // custom options for the <input> tag are added simply by
		            // adding them to this array
			        ),

		        'FieldValue' => array(
		            'type' => "text", // default
		            'style' => "border-color: red;", // custom options for the <input> tag are added simply by
		            // adding them to this array
				        ),
			    )

                )
        );


// initial values
		$this->jform->setDefaultValues(array(
   		 'ID' => "-1",
		    'Depth' => "-1",
		    'Field' => "Unknown",
		    'FieldValue' => "",
		));


		$forms["read"] =   clone  $this->jform;





/// form to create the write request

			 $this->jform->setForm(
                array(
			'focus' => "", // focus first element
			 // 'anchor' => "", // submit url will point to anchor at $form.begin
			 // 'enctype' => "multipart/form-data", // for file input
			'submit_label' => "Process",
			'inputs' => array(
			        'ID' => array(
			        	'validators' => new NumericValidator(),
				        'required' => "true",
				        'maxlength' => 10, // automatically adds a CutOffFilter
				        'style' => "border-color: red;", // custom options for the <input> tag are added simply by
				        // adding them to this array
			        	),





// a number of properties of the booking, depends  on the graph model
			       'BookerName' => array(
	        			    'type' => "text", // default
        				    'style' => "border-color: red;", // custom options for the <input> tag are added simply by
				            // adding them to this array
					    ),

			     'TravelerName' => array(
				            'type' => "text", // default
				            'style' => "border-color: red;", // custom options for the <input> tag are added simply by
				            // adding them to this array
					    ),

			        'BookerEmail' => array(
				            'type' => "text", // default
				            'style' => "border-color: blue;", // custom options for the <input> tag are added simply by
				            // adding them to this array
					     ),

			        'TravelerEmail' => array(
				            'type' => "text", // default
				            'style' => "border-color: blue;", // custom options for the <input> tag are added simply by
				            // adding them to this array
					    ),


	    			)
                )
        );


// initial values
		$this->jform->setDefaultValues(array(

				'ID' => "-1",
				'BookerName' => "Booker1~0.80",
				'TravelerName' => "Traveler1~0.80",
				'TravelerEmail' => "Traveler1@gmail.com~0.999",
				'BookerEmail' => "Booker1@gmail.com~0.999"


		));





	 $forms["write"] =   clone  $this->jform;



	 $this->jform->setForm(
                             array(
                        'focus' => "", // focus first element
                            // 'anchor' => "", // submit url will point to anchor at $form.begin
                            // 'enctype' => "multipart/form-data", // for file input
                            'submit_label' => "Dump",
				)
			);

	  $forms["dump"] =   clone  $this->jform;



	$this->jform->setForm(
                             array(
                        'focus' => "", // focus first element
                            // 'anchor' => "", // submit url will point to anchor at $form.begin
                            // 'enctype' => "multipart/form-data", // for file input
                            'submit_label' => "Process",
				'inputs' => array(
				 'GremlinQuery' => array(
                                            'type' => "text", // default
                                            'style' => "border-color: red;", // custom options for the <input> tag are added simply by
                                            // adding them to this array
                                            ),
				 )

                                )
                        );


// initial values
                $this->jform->setDefaultValues(array(

                                'GremlinQuery' => "graph()->Vertex()->All()"


                ));



   $forms["gremlinquery"] =   clone  $this->jform;





		return $forms;

	}


}


?>
 
 
