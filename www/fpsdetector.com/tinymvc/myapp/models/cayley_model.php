<?php

class Cayley_Model extends TinyMVC_Model
{


	//the generic method: returns Cayley PDO object
	function getDb()
	{
		return $this->db->pdo;
	}


	// retuns plotter for Cayley
	function getPlotter()
	{
		return new Cayley_Plotter;
	}

}



