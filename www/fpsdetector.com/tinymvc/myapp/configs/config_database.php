<?php

/**
 * database.php
 *
 * application database configuration
 *
 * @package		TinyMVC
 */

$config['default']['plugin'] = 'TinyMVC_PDO'; // plugin for db access
$config['default']['type'] = 'sql';      // connection type
$config['default']['host'] = 'localhost';  // db hostname
$config['default']['name'] = 'dbname';     // db name
$config['default']['user'] = 'dbuser';     // db username
$config['default']['pass'] = 'dbpass';     // db password
$config['default']['persistent'] = false;  // db connection persistence?


$config['cayley']['plugin'] = 'TinyMVC_CayleyPDO'; // plugin for db access
$config['cayley']['host_r'] = 'localhost';  // db hostname for reading entries 
$config['cayley']['port_r'] = 64210;  // db port for reading entring
$config['cayley']['host_w'] = 'localhost';  // db hostname for writing entries 
$config['cayley']['port_w'] = 8888;  // db port for reading writing
$config['cayley']['type-bk-tree'] = 1;  // 0  -- use http bk-tree, 1 -- use named pipes
$config['cayley']['pipe_read'] = "/tmp/trie_server_w";  //  pipe to read
$config['cayley']['pipe_write'] = "/tmp/trie_server_r";  //  pipe to write
$config['cayley']['pipe_size'] = 4096;  //  size of pipes in bytes
$config['cayley']['key'] = 8;  //  key for semaphores


// FPS detector settings
$config['fpsdetector']['plugin'] = 'TinyMVC_FPSDetector'; // plugin to communicate with FPS ML detector 
$config['fpsdetector']['host'] = 'localhost';  // RPC Service hostname 
$config['fpsdetector']['port'] = 8092;  // RPC Service port



