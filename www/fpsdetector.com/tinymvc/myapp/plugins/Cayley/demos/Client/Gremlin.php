<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */



/**
 * to test reading cayley database
 * first, start the cayley:  cd bk-tree-0.1/cayley/cayley_0.4.0_linux_386; ./cayley http --dbpath=testdata.nq
 */

/**
 * Cayley PDO supports Gremlin structures like Vertex, Path, Morphism etc. See documentation of the cayley for more details.
 */


include_once dirname(__FILE__) . '/../../SplClassLoader.php';


 $classLoader = new SplClassLoader(null, dirname(__FILE__)."/../../library/");
 $classLoader->register();
 
 $config = parse_ini_file( dirname(__FILE__)."/../config/config.ini");
 $adapter_http=Cayley_Client_Adapter_Http::connect($config['host_r'], $config['port_r']);
 $client = new Cayley_Client( $adapter_http, $adapter_http);

 

$query = $client->graph()->Vertex()->All();
$result=$client->read($query);	
var_dump(json_encode($result));