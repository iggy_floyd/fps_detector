<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 *  test of the Client_Adapter_Http
 */

include_once dirname(__FILE__) . '/../../SplClassLoader.php';


 $classLoader = new SplClassLoader(null, dirname(__FILE__)."/../../library/");
 $classLoader->register();
 
 $config = parse_ini_file( dirname(__FILE__)."/../config/config.ini");
 $adapter_http=Cayley_Client_Adapter_Http::connect($config['host_r'], $config['port_r']);
 var_dump($adapter_http);
 