#! /usr/bin/env python

'''
#
#  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
#  All rights reserved.
#
#
#  Usage: %(scriptName)s
#
'''


from subprocess import Popen,PIPE
import inspect
import sys


def test_php(file):

 file=file.replace("_","/");
 return Popen(["php", file], stdout=PIPE).communicate()[0][:-1]
  


def Result():
   """ Result.php

   >>> Result()
   'string(43) "{"ID":"12345","TravelerName":"Igor Marfin"}"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")


def Client_Adapter():
   """  Client/Adapter.php

      to get a hint on the php output, please do
      (php Client/Adapter.php) | sed -e :a -e N -e '$!ba' -e 's/\n/\\\\n/g'

   >>> Client_Adapter()
   'object(Cayley_Client_Adapter_Http)#2 (5) {\\n  ["_protocol":"Cayley_Client_Adapter_Http":private]=>\\n  string(4) "http"\\n  ["_silentMode":"Cayley_Client_Adapter_Http":private]=>\\n  bool(false)\\n  ["_headers":"Cayley_Client_Adapter_Http":private]=>\\n  array(0) {\\n  }\\n  ["_host":"Cayley_Client_Adapter_Http":private]=>\\n  string(9) "localhost"\\n  ["_port":"Cayley_Client_Adapter_Http":private]=>\\n  string(5) "64210"\\n}'
   """
   return test_php(str(inspect.stack()[0][3])+".php")



def Client():
   """  Client.php

   >>> Client()
   'string(151) "{"0":{"id":"alice"},"1":{"id":"loves"},"2":{"id":"bob"},"3":{"id":"charlie"},"4":{"id":"dani"},"5":{"id":"is"},"6":{"id":"cool"},"7":{"id":"not cool"}}"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")




def Client_Gremlin():
   """  Client/Gremlin.php

   >>> Client_Gremlin()
   'string(151) "{"0":{"id":"alice"},"1":{"id":"loves"},"2":{"id":"bob"},"3":{"id":"charlie"},"4":{"id":"dani"},"5":{"id":"is"},"6":{"id":"cool"},"7":{"id":"not cool"}}"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")



def Debugger():
   """  Debugger.php

   >>> Debugger()
   'bool(true)'
   """
   return test_php(str(inspect.stack()[0][3])+".php")



def Client_HttpException():
   """  Client/HttpException.php

   >>> Client_HttpException()
   'string(13) "Error message"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")



def Client_GremlinWithString():
   """  Client/GremlinWithString.php

   >>> Client_GremlinWithString()
   'string(41) "{"0":{"id":"charlie"},"1":{"id":"alice"}}"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")






if __name__ == '__main__':

    print __doc__ % {'scriptName' : sys.argv[0]}


    import doctest
    doctest.testmod(verbose=True)
