<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Description of Abstract
 *
 * @author Igor Marfin
 */
abstract class Cayley_Client_Adapter_Abstract implements Cayley_Client_Adapter_Interface
{

    /**
     *  constructor
     */
    protected function __construct()
    {
        
    }

    /*     * Performs query to a http service or pipe 
     * 
     * @param string $url
     * @param string $params
     */

    public function query($url, $params)
    {
        
    }

    /** Constructs adapter and returns adapter
     *
     * @param  string  $input1 -- might be a host name  or pipe to read
     * @param  string  $input2 -- might be a port or pipe to write
     * @param  string  $input3 -- might be a protocol or an id of the semaphore
     * @return \self
     */
    public static function connect($input1, $input2, $input3)
    {
        
    }

}
