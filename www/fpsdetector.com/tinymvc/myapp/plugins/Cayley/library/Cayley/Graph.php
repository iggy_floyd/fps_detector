<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Cayley_Graph represents a graph in the Gremlin language
 *
 * @author Igor Marfin
 */
class Cayley_Graph extends Cayley_Statement_Abstract
{

  

    public function __construct()
    {
        $this->push('graph');
    }

    /**
     
     * @param array $nodes
     * @return Cayley_Path_Vertex
     */
    public function vertex($nodes = null)
    {
        $this->pushMethodWithListOfStrings('Vertex', (array) $nodes);
        return new Cayley_Path_Vertex($this);
    }

    /**
     * 
     * @param array $nodes
     * @return Cayley_Path_Vertex
     */
    public function v($nodes = null)
    {
        return $this->vertex($nodes);
    }

    /**
     * 
     * @return Cayley_Path_Morphism
     */
    public function morphism()
    {
        $this->push('Morphism()');
        return new Cayley_Path_Morphism($this);
    }

    /**
     * 
     * @return Cayley_Path_Morphism
     */
    public function m()
    {
        return $this->morphism();
    }

    /**
     * Print data response in the gremlin langauge
     * @param mixed $data
     * @return \Cayley_Graph
     */
    public function emit($data)
    {
        $this->push(sprintf('Emit(%s)', json_encode($data)));
        return $this;
    }

}
