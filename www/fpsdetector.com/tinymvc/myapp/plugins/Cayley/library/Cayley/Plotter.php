<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Cayley_Plotter do drawing of the graphs
 *
 * @author Igor Marfin
 */
class Cayley_Plotter
{
    /* @var $_jscode_begin Cayley_Plotter to open script tag */

    private $_jscode_begin = <<< EOT
            
        <script type="text/javascript" src="javascripts/vis.js"></script>
	<script type="text/javascript">
   
EOT;

    /* @var $_jscode_end Cayley_Plotter to close script tag */
    private $_jscode_end = <<< EOT
            
       </script>



	<div id="mynetwork">

	    <script type="text/javascript">

        	draw();

	    </script>

	</div>
   
EOT;

    /**
     * 
     * @param array $nodes
     * @param array $edges
     * @param array $props
     * @return string
     */
    public function getJSCode($nodes, $edges, $props)
    {

        $drawer = file_get_contents(__DIR__ . '/JS/draw.js.tmpl');
        return str_replace(array('%nodes%', '%edges%', '%props%'), array($nodes, $edges, $props),Cayley_Minifier::minify($drawer));
    }
    
    /**
     * to make a nice labels from input data
     * 
     * @param string $data
     * @param string $data2
     * @param string $data3
     * @return string
     */
    private function _makeLabel($data,$data2,$data3)
    {
        
        //$_data = explode($delim,$data);
        $label = "<".$data."> \\n"."<".$data2."> \\n"."<".$data3.">";
        return $label;        
        //return $data;
    }

    /**
     * 
     * @param Cayley_Result $queryresult
     * @return string
     */
    public function getHTMLCode(Cayley_Result $queryresult)
    {



        $mynodes = array();
        $myedges = array();
        $myprops = array();

        $nodes = "";
        $edges = "";
        $props = "";


        // a pattern of the node
        $pattern = '/(?P<value>.*)\|\|\|(?P<id>.*)\|\|\|(?P<prop>.*)/';

        //  a pattern to be used to form a label
        $pattern_id_label = '/_ID/';


        // weights for nodes
        $booking_node_weight = 30;
        $property_node_weight = 15;

        // weights for edges
        $booking_property_edge_weight = 10;
        $property_property_edge_weight = 20;




        // some styling for nodes (up to 10)
        $_colors = array('#8C8CFF', '#01F33E', '#FF0099', '#5EAE9E', '#2FAACE', '#E1CAF9', '#5757FF', '#FF9331', '#F70000', '#FF62B0');
        $_shapes = array('dot', 'dot', 'dot', 'dot', 'dot', 'dot', 'dot', 'dot', 'dot', 'dot');

        $propnum = 1;
        foreach ($queryresult as $item) {

            if (!array_key_exists('id', $item) || !array_key_exists('target', $item) || !array_key_exists('source', $item))
                return null;


            $src = $item["source"];
            $trgt = $item["target"];
            $_nodepattern = "{id: '%id%', value: %weight%, label: '%label%', group: '%group%'},";
            $_edgepattern = "{from: '%src%', to: '%trgt%', value: %weight%},";
            if ($propnum > 10)
                $propnum = 0;
            $_proppattern = "%group% : {color: '%color%', shape:'%shape%'},";



 
            preg_match($pattern, $trgt, $matches_trgt);

            
            if (preg_match($pattern_id_label, $src)) {
                $weight_edge = $booking_property_edge_weight;
                
                if (!array_key_exists("id", $mynodes)) {
                    $mynodes["id"] = str_replace(array('%id%', '%weight%', '%label%', '%group%'), array("id", $booking_node_weight, "Booking ID", "booking"), $_nodepattern);
                }
                   
                
                if (!array_key_exists($src, $mynodes))
                    $mynodes[$src] = str_replace(array('%id%', '%weight%', '%label%', '%group%'), array($src, $booking_node_weight, $src, "booking"), $_nodepattern);
                if (!array_key_exists($trgt, $mynodes)) {
                    $mynodes[$trgt] = str_replace(array('%id%', '%weight%', '%label%', '%group%'), array($trgt, $property_node_weight, $this->_makeLabel($matches_trgt["value"],$matches_trgt["id"],$matches_trgt["prop"]), $matches_trgt["prop"]), $_nodepattern);
                    if (!array_key_exists($matches_trgt["prop"], $myprops)) {
                        $myprops[$matches_trgt["prop"]] = str_replace(array('%group%', '%color%', '%shape%'), array($matches_trgt["prop"], $_colors[$propnum], $_shapes[$propnum]), $_proppattern);
                        $propnum++;
                    }
                }
                
                if (!array_key_exists($matches_trgt["prop"], $mynodes)) {
                    $mynodes[$matches_trgt["prop"]] = str_replace(array('%id%', '%weight%', '%label%', '%group%'), array($matches_trgt["prop"], $property_node_weight, $matches_trgt["prop"], $matches_trgt["prop"]), $_nodepattern);
                   $myedges["id" . $matches_trgt["prop"]] = str_replace(array('%src%', '%trgt%', '%weight%'), array("id", $matches_trgt["prop"], $weight_edge), $_edgepattern);
                }                        
                $myedges[$src . $trgt] = str_replace(array('%src%', '%trgt%', '%weight%'), array($src, $trgt, $weight_edge), $_edgepattern);
            } else {
                $weight_edge = $property_property_edge_weight;
                preg_match($pattern, $src, $matches_src);
                if (!array_key_exists($src, $mynodes))
                    $mynodes[$src] = str_replace(array('%id%', '%weight%', '%label%', '%group%'), array($src, $property_node_weight, $this->_makeLabel($matches_src["value"],$matches_src["id"],$matches_src["prop"]), $matches_src["prop"]), $_nodepattern);
                if (!array_key_exists($trgt, $mynodes)) {
                    $mynodes[$trgt] = str_replace(array('%id%', '%weight%', '%label%', '%group%'), array($trgt, $property_node_weight, $this->_makeLabel($matches_trgt["value"],$matches_trgt["id"],$matches_trgt["prop"]), $matches_trgt["prop"]), $_nodepattern);
                    if (!array_key_exists($matches_trgt["prop"], $myprops)) {
                        $myprops[$matches_trgt["prop"]] = str_replace(array('%group%', '%color%', '%shape%'), array($matches_trgt["prop"], $_colors[$propnum], $_shapes[$propnum]), $_proppattern);
                        $propnum++;
                    }
                }
                $myedges[$src . $trgt] = str_replace(array('%src%', '%trgt%', '%weight%'), array($src, $trgt, $weight_edge), $_edgepattern);
            }
        }


        

        foreach ($mynodes as $node) {
            $nodes .= $node . PHP_EOL;
        }

        foreach ($myedges as $edge) {
            $edges .= $edge . PHP_EOL;
        }
        foreach ($myprops as $prop) {
            $props .= $prop . PHP_EOL;
        }

        $drawer = file_get_contents(__DIR__ . '/JS/draw.js.tmpl');
        return str_replace(array('%nodes%', '%edges%', '%props%'), array($nodes, $edges, $props), Cayley_Minifier::minify($this->_jscode_begin . $drawer . $this->_jscode_end)    );        
    }

}
