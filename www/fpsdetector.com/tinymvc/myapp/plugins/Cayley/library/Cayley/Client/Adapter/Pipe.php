<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * To communicate with piped daemon
 *
 * @author Igor Marfin
 */
class Cayley_Client_Adapter_Pipe extends Cayley_Client_Adapter_Abstract
{
    /* @var $key Cayley_Client_Adapter_Pipe id of the semaphore */
    private $_key = null;

    /* @var $_pipesize Cayley_Client_Adapter_Pipe  size of the pipes in kB */
    private $_pipesize = null;

    /* @var $_piperead   -- named pipe to which bk-tree daemon writes */
    private $_piperead = null;

    /* @var $_pipewrite   -- named pipe from which bk-tree daemon reads */
    private $_pipewrite = null;

    /**
     * 
     * @return string pipe to read
     */
    public function getPiperead()
    {
        return $this->_piperead;
    }

    /**
     * 
     * @return string pipe to write
     */
    public function getPipewrite()
    {
        return $this->_pipewrite;
    }

    /**
     * set pipe to read
     * @param string $piperead
     *  @return Cayley_Client_Adapter_Pipe
     */
    public function setPiperead($piperead)
    {
        $this->_piperead = $piperead;
        return $this;
    }

    /**
     * 
     * @return int id of the semaphore
     */
    public function getKey()
    {
        return $this->_key;
    }

    /**
     * set id of the semaphore
     * @param int $key
     * @return \Cayley_Client_Adapter_Pipe
     */
    public function setKey($key)
    {
        $this->_key = $key;

        return $this;
    }

    /**
     * set pipe to write
     * @param string $pipewrite
     *  @return \Cayley_Client_Adapter_Pipe
     */
    public function setPipewrite($pipewrite)
    {
        $this->_pipewrite = $pipewrite;
        return $this;
    }

    /**
     * constructor
     * 
     * @param string $piperead
     * @param string $pipewrite
     * @param int $key
     */
    protected function __construct($piperead, $pipewrite, $key)
    {
        $this->setPiperead($piperead);
        $this->setPipewrite($pipewrite);
        $this->setKey($key);
        parent::__construct();
    }

    /**
     * Constructs adapter and returns adapter
     * @param  string  $piperead
     * @param  string  $pipewrite
     * @param  string  $key
     * @return \self
     */
    public static function connect($piperead, $pipewrite, $key)
    {
        return new self($piperead, $pipewrite, (int) $key);
    }

    /**
     * set size of the pipes
     * @param  int                 $pipesize
     * @return Cayley_Client_Adapter_Pipe
     */
    public function setPipesize($pipesize)
    {
        $this->_pipesize = $pipesize;

        return $this;
    }

    /**
     * 
     * @return int size of pipes
     */
    public function getPipesize()
    {
        return $this->_pipesize;
    }

    /**
     * Send Query  to bk-tree daemon service
     * @param  string   $url not used here
     * @param  string   $params
     * @return string
     */
    public function query($url, $params)
    {
        if (!(
                is_null($this->getPiperead()) ||
                is_null($this->getPipewrite()) ||
                is_null($this->getKey()) ||
                is_null($this->getPipesize())
                )) {

            $pipe_write = fopen($this->getPipewrite(), 'w');
            if (!$pipe_write) {
                die('Error: Could not open the named pipe to write: \n');
            }
            $pipe_read = fopen($this->getPiperead(), 'r');
            if (!$pipe_read) {
                die('Error: Could not open the named pipe to read: \n');
            }

            $semaphore = sem_get($this->getKey(), 1, 0, 0);

            if (!$semaphore) {
                die('Error: Could not get semaphore: \n' );
            }
            sem_acquire($semaphore);

            fwrite($pipe_write, $params);
            $out = fgets($pipe_read, $this->getPipesize());

            sem_release($semaphore);
            fclose($pipe_read);
            fclose($pipe_write);

            return $out;
        }

        return "";
    }

}
