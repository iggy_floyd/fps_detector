<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Cayley_Result represents a Cayley response
 *
 * @author Igor Marfin
 */
class Cayley_Result extends ArrayIterator
{

    private $_result = null;

    public function __construct($result)
    {

        if (array_key_exists('result', (array) $result)) {
            $this->_result = (array) $result;
            return parent::__construct((Array) $result['result']);
        } else {
            $this->_result = array();
            return parent::__construct(array());
        }
    }

    public function getResponse()
    {
        if (array_key_exists('cayley', $this->_result)) {
            return (Array) $this->_result['cayley'];
        } else {
            return array();
        }
    }

    public function getResult()
    {
        if (array_key_exists('result', $this->_result)) {
            return $this->_result['result'];
        } else {
            return array();
        }
    }

    public function __toString()
    {
        return implode('\n', $this->_result);
    }

}
