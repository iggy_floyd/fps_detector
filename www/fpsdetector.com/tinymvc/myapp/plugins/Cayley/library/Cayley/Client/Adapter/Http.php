<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Description of Http
 *
 * @author Igor Marfin
 */
class Cayley_Client_Adapter_Http extends Cayley_Client_Adapter_Abstract
{
    /* @var $HTTP Cayley_Client_Adapter_Http _protocols */

    const HTTP = 'http';
    const HTTPS = 'https';
    const POST = 'POST';
    const GET = 'GET';
    const DELETE = 'DELETE';
    const PUT = 'PUT';
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACEPTED = 202;

    /* @var $_protocol Cayley_Client_Adapter_Http  defines type of the protocol */

    private $_protocol = null;

    /* @var $_silentMode Cayley_Client_Adapter_Http switches on/off debugging info */
    private $_silentMode = false;

    /* @var $_headers Cayley_Client_Adapter_Http  to store headers */
    private $_headers = array();

    /* @var $_host Cayley_Client_Adapter_Http  to  store  a host name */
    private $_host = null;

    /* @var $_port Cayley_Client_Adapter_Http  to store a port */
    private $_port = null;

    /**
     * 
     * @return string a host name
     */
    public function getHost()
    {
        return $this->_host;
    }

    /**
     * 
     * @return string a port
     */
    public function getPort()
    {
        return $this->_port;
    }

    /**
     * set a host name
     * @param string $host
     */
    public function setHost($host)
    {
        $this->_host = $host;
    }

    /**
     * set a port 
     * @param string $port
     */
    public function setPort($port)
    {
        $this->_port = $port;
    }

    /**
     * constructor
     * 
     * @param string $host
     * @param string $port
     * @param string $protocol
     */
    protected function __construct($host, $port, $protocol)
    {
        $this->setHost($host);
        $this->setPort($port);
        $this->_protocol = $protocol;

        parent::__construct();
    }

    /**
     * create and return Cayley_Client_Adapter_Http
     * @param  string $host
     * @param  string $port
     * @param Cayley_Client_Adapter_Http $protocol
     * @internal param $self ::HTTP  $protocol
     * @return \self
     */
    public static function connect($host, $port, $protocol = self::HTTP)
    {
        return new self($host, $port, $protocol);
    }

    /** set a silent mode
     * 
     * @param boolean $mode
     * @return \Cayley_Client_Adapter_Http
     */
    public function silentMode($mode = true)
    {
        $this->_silentMode = $mode;

        return $this;
    }

    /*     * Send PUT request
     * @param string  $url
     * @param string $params
     * @return string
     */
    public function doPut($url, $params)
    {
        return $this->exec(self::PUT, $this->_url($url), $params);
    }

    /*     * Send POST request
     * 
     * @param string $url
     * @param string $params
     * @return string
     */

    public function doPost($url, $params)
    {
        return $this->exec(self::POST, $this->_url($url), $params);
    }

    /*     * Send GET request
     * 
     * @param string $url
     * @param string $params
     * @return string
     */
    public function doGet($url, $params)
    {
        return $this->exec(self::GET, $this->_url($url), $params);
    }

    /*     * Send DELETE request
     * 
     * @param string $url
     * @param string $params
     * @return string
     */
    public function doDelete($url, $params)
    {
        return $this->exec(self::DELETE, $this->_url($url), $params);
    }

    /*     * Send Query (POST request) to cayley or bk-tree http service 
     * 
     * @param type $url
     * @param type $params
     * @return type
     */

    public function query($url, $params)
    {
        return $this->doPost($url, $params);
    }

    /**
     *    Set headers
     * @param string $headers
     * @return \Cayley_Client_Adapter_Http
     */
    public function setHeaders($headers)
    {
        $this->_headers = $headers;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->_protocol;
    }

    /**
     * set protocol
     * @param string $protocol
     * @return Cayley_Client_Adapter_Http
     */
    public function setProtocol($protocol)
    {
        $this->_protocol = $protocol;
        return $this;
    }

    /**
     * 
     * @param string $url
     * @return string the whole $url
     */
    private function _url($url = null)
    {
        return "{$this->getProtocol()}://{$this->getHost()}:{$this->getPort()}/{$url}";
    }

    /**
     * sends requests via curl
     *
     * @param  string      $type
     * @param  string      $url
     * @param  string      $params
     * @return string
     * @throws Exception
     */
    private function exec($type, $url, $params)
    {
        $headers = $this->_headers;
        $s = curl_init();

        switch ($type) {
            case self::DELETE:
                curl_setopt($s, CURLOPT_URL, $url . '?' . http_build_query($params));
                curl_setopt($s, CURLOPT_CUSTOMREQUEST, self::DELETE);
                break;
            case self::PUT:
                curl_setopt($s, CURLOPT_URL, $url);
                curl_setopt($s, CURLOPT_CUSTOMREQUEST, self::PUT);
                curl_setopt($s, CURLOPT_POSTFIELDS, $params);
                break;
            case self::POST:
                curl_setopt($s, CURLOPT_URL, $url);
                curl_setopt($s, CURLOPT_POST, true);
                curl_setopt($s, CURLOPT_POSTFIELDS, $params);
                break;
            case self::GET:
                curl_setopt($s, CURLOPT_URL, $url . '?' . http_build_query($params));
                break;
        }

        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($s, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($s, CURLOPT_TIMEOUT, 400000); //timeout in seconds
        if (!$_out = curl_exec($s)) {
            trigger_error(curl_error($s));
        }
        $status = curl_getinfo($s, CURLINFO_HTTP_CODE);
        curl_close($s);

        switch ($status) {
            case self::HTTP_OK:
            case self::HTTP_CREATED:
            case self::HTTP_ACEPTED:
                $out = $_out;
                break;
            case Cayley_Client_Exception_HttpException::BAD_REQUEST:
            case Cayley_Client_Exception_HttpException::INTERNAL_ERROR:
                if (!$this->_silentMode) {
                    throw new Exception($_out, $status);
                }
                $out = $_out;
                break;
            default:
                $out = '';
                break;
        }

        return $out;
    }

}
