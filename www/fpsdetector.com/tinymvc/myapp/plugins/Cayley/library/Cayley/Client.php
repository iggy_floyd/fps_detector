<?php

class Cayley_Client
{
    /* @var URL_READ_GREMLIN  Cayley_Client */
    const URL_READ_GREMLIN = 'api/v1/query/gremlin';

    /* @var URL_WRITE Cayley_Client */
    const URL_WRITE = 'write';

    /* @var $_adapter_r Cayley_Client */

    private $_adapter_r = null;

    /* @var $_adapter_w Cayley_Client */
    private $_adapter_w = null;

 
    /**
     * 
     * @param Cayley_Client_Adapter_Interface $adapter_r
     * @param Cayley_Client_Adapter_Interface $adapter_w
     */
    public function __construct(Cayley_Client_Adapter_Interface $adapter_r, Cayley_Client_Adapter_Interface $adapter_w)
    {
        $this->_adapter_r = $adapter_r;
        $this->_adapter_w = $adapter_w;
        set_time_limit(0); // to infinity
    }

    /**
     *
     * @return Cayley_Graph
     */
    public function graph()
    {
        return new Cayley_Graph();
    }

    /**
     *  this and other methods depends on the "JS/api.js" code. 
     * The change of the content of the  'JS/api.js' member  will probably require to rewrite
     *  the methods.
     */

    /**
     *
     * @return Cayley_Result
     */
    public function getsizeID()
    {
        return $this->readWithApi("g.Emit", "getsizeID_v5()");
    }

    /**
     *
     * @param  int $from
     * @param  int $to
     * @return Cayley_Result
     */
    public function getIDFromTo($from, $to)
    {
        return $this->readWithApi("getAllID_v5", (int) $from, (int) $to);
    }

    /**
     *
     * @return Cayley_Result
     */
    public function getAllID()
    {
        return $this->readWithApi("getAllID_v3");
    }

    /**
     *
     * @param  string $booking should be numerical value in the string ,like '10000'
     * @param  int $depth
     * @return Cayley_Result
     */
    public function getConnectedPropertiesWithBooking($booking, $depth)
    {
        $_booking = "\"" . (string) $booking . "_ID\"";

        return $this->readWithApi("getConnectedPropertiesWithBooking_v5", $_booking, (string) $depth, (string) 3 * (int) $depth, "false");
    }

    /**
     *
     * @param  int $depth
     * @param  int  $category
     * @return Cayley_Result
     */
    public function viewAllConnectedOnlyIf($depth, $category)
    {
        $_category = "\"" . $category . "\"";

        return $this->readWithApi("viewAllConnectedOnlyIf_v3", (string) 4 * (int) $depth, (string) 4 * (int) $depth, $_category);
    }

    /**
     *
     * @param  int $depth
     * @return Cayley_Result
     */
    public function viewAllConnected($depth)
    {
        return $this->readWithApi("viewAllConnected_v3", (string) 4 * (int) $depth, (string) 4 * (int) $depth);
    }

    /**
     *
     * @param  int $limitedges
     * @return Cayley_Result
     */
    public function viewAll($limitedges)
    {
        return $this->readWithApi("viewAll_v3", (string) 4 * (int) $limitedges);
    }

    /**
     *
     * @param  int $target
     * @param  int $depth
     * @return Cayley_Result
     */
    public function getConnectedPropertiesFromTarget($target, $depth)
    {
        $_target = "\"" . $target . "\"";

        return $this->readWithApi("getConnectedPropertiesFromTarget_v5", $_target, (string) $depth);
    }

    /**
     *
     * @param  string $booking
     * @param  int $depth
     * @param  int $category
     * @return Cayley_Result
     */
    public function getConnectedProperty($booking, $depth, $category)
    {
        $_booking = "\"" . (string) $booking . "_ID\"";
        $_category = "\"" . $category . "\"";

        return $this->readWithApi("getConnectedProperty_v5", $_booking, (string) $depth, $_category);
    }

    /**
     *
     * @return Cayley_Result
     */
    public function readWithApi()
    {
        $numarg = func_num_args();
        $arg = func_get_args();
        if ($numarg < 1) {
            return new Cayley_Result(array());
        }
        $fun = $arg[0];

        if ($numarg < 2) {
            $fun.="();" . PHP_EOL;
        } else {

            $args = array_slice($arg, 1);
            $numarg = count($args);
            $callback = function ($i) {
                $str = "arg";
                $str = "%" . $str . "$i" . "%";

                return (string) $str;
            };
            $arg_i = range(0, $numarg - 1);
            $arg_pattern = array_map($callback, $arg_i);
            $fun_pattern = $fun . "(" . implode(",", $arg_pattern) . ");";
            $fun = str_replace($arg_pattern, $args, $fun_pattern) . PHP_EOL;
        }

        $api = file_get_contents(__DIR__ . '/JS/api.js.tmpl');

        $request = Cayley_Minifier::minify($api . PHP_EOL . $fun);
       
        return $this->readWithGremlin($request);
    }

    /**
     *
     * @param  string         $js
     * @return Cayley_Result
     */
    public function readWithGremlin($js)
    {
        $response = $this->_doRequest($this->_adapter_r, self::URL_READ_GREMLIN, $js);

        return new Cayley_Result(json_decode($response, true));
    }

    /**
     * read data from cayley
     * @param  Cayley_Statement_Interface $statement
     * @return string
     */
    public function read(Cayley_Statement_Interface $statement)
    {
        return $this->readWithGremlin((string) $statement);
    }

    /**
     *
     * @param  string  $statement
     * @return string
     */
    public function readWithGremlinString($statement)
    {
        $chain = explode("->", $statement);

        // a pattern of the chain element
        $pattern = '/(?P<method>.*)\((?P<args>.*)\)/';
        $obj = $this;

        $callback = function ($obj, $method, $args) {

            if (strlen($args) == 0) {
                $args = null;
            }



            if (strlen($args) == 0) {
                $_args = array();
            } else {
                $_args = explode(',', $args);
            }




            return call_user_func_array(array($obj, $method), $_args);
        };

        foreach ($chain as $item) {

            preg_match($pattern, $item, $matches);




            $obj = $callback($obj, $matches["method"], $matches["args"]);
        }



        return $this->read($obj);
    }

    /**
     * write data to bk-tree
     * @param  array         $data
     * @param  boolean         $debug
     * @param  array         $rules contains regex expressions and 
     * corresponding distances
     * @return Cayley_Result
     */
    public function write($data, $debug = true, $rules=array(".*" => 0.9999 ),$exclusions = array())
    {
           $_rules = $rules;
           //to test regex expression
//        $_rules = array_merge($rules,array(".*(name|Name).*"  => 0.20));

        $arg = array(
            "debug" => $debug,
            "entry" => $data,
            "rules" => $_rules,
             "exclusion" =>$exclusions
        );

        $cmd = array(
            "cmd" => "save2DB",
            "arg" => $arg
        );

        $response = $this->_doRequest($this->_adapter_w, self::URL_WRITE, json_encode($cmd));

        return new Cayley_Result(json_decode($response, true));
    }

    /**
     * dump trees of bk-tree to a disk
     * @param  boolean         $debug
     * @return Cayley_Result
     */
    public function dumpTrees($debug = true)
    {
        $arg = array(
            "debug" => $debug
        );

        $cmd = array(
            "cmd" => "dump",
            "arg" => $arg
        );

        $response = $this->_doRequest($this->_adapter_w, self::URL_WRITE, json_encode($cmd));

        return new Cayley_Result(json_decode($response, true));
    }

    /**
     * send query to the service
     * @param  Cayley_Client_Adapter_Interface                           $client
     * @param  string                           $url
     * @param  string                           $body
     * @return string
     * @throws Cayley_Client_Exception_HttpException
     */
    private function _doRequest(Cayley_Client_Adapter_Interface $client, $url, $body)
    {
        try {
            return $client->query($url, $body);
        } catch (Exception $e) {
            throw new Cayley_Client_Exception_HttpException($e);
        }
    }

}
