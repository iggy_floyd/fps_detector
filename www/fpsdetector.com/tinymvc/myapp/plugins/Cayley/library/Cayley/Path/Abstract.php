<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Description of Abstract
 *
 * @author Igor Marfin
 */
abstract class Cayley_Path_Abstract  extends Cayley_Statement_Abstract implements Cayley_Path_Interface  {

    /**
     * Construct a path statement from a graph
     * @param Cayley_Graph $graph
     */
    public function __construct(Cayley_Graph $graph) {
        $this->push((string) $graph);
    }

    
    /**
     * forms the 'Out' method in the statement
     * @param string $predicatePath
     * @param string $tags
     * @return string
     */
    public function out($predicatePath = null, $tags = null) {
        return $this->bounds('Out', $predicatePath, $tags);
    }

    /**
     * forms the 'In' method in the statement
     * @param string $predicatePath
     * @param string $tags
     * @return string
     */
    public function in($predicatePath = null, $tags = null) {
        return $this->bounds('In', $predicatePath, $tags);
    }

    /**
     * forms the 'Both' method in the statement
     * @param string $predicatePath
     * @param string $tags
     * @return string
     */
    public function both($predicatePath = null, $tags = null) {
        return $this->bounds('Both', $predicatePath, $tags);
    }

    /**
     * makes formating of the statements
     * @param string $method
     * @param string $predicatePath
     * @param string $tags
     * @return \Cayley_Path_Abstract
     */
    public function bounds($method, $predicatePath = null, $tags = null) {
        if (!$predicatePath && !$tags) {
            $this->push(sprintf('%s()', $method));
        } elseif (!$tags) {
            $this->push(sprintf('%s(%s)', $method, $this->formatInputBounds($predicatePath)));
        } else {
            $this->push(sprintf('%s(%s, %s)', $method, $this->formatInputBounds($predicatePath), $this->formatInputBounds($tags)));
        }
        return $this;
    }

    /**
     * makes formating input arguments of the statemtent
     * @param string $value
     * @return string
     */
    public function formatInputBounds($value) {
        if (is_array($value)) {
            return json_encode($value);
        }
        if (is_string($value)) {
            return '"' . $value . '"';
        }
        if (!$value) {
            return 'null';
        }
        return $value;
    }

    /**
     * forms the 'Is' method in the statement
     * @param string $nodes
     * @return \Cayley_Path_Abstract
     */
    public function is($nodes) {
        $this->pushMethodWithListOfStrings('Is', $nodes);
        return $this;
    }

    
    /**
     * forms the 'Has' method in the statement
     * @param string $predicate
     * @param string $object
     * @return \Cayley_Path_Abstract
     */
    public function has($predicate, $object) {
        $this->push(sprintf('Has("%s", "%s")', $predicate, $object));
        return $this;
    }

    /**
     * forms the 'Tag' method in the statement
     * @param string $tags
     * @return \Cayley_Path_Abstract
     */
    public function tag($tags) {
        $this->pushMethodWithListOfStrings('Tag', $tags);
        return $this;
    }

    
    /**
     * forms the 'Back' method in the statement
     * @param string $tag
     * @return \Cayley_Path_Abstract
     */
    public function back($tag) {
        $this->push(sprintf('Back("%s")', $tag));
        return $this;
    }

    
    /**
     * forms the 'Save' method in the statement
     * @param string $predicate
     * @param string $object
     * @return \Cayley_Path_Abstract
     */
    public function save($predicate, $object) {
        $this->push(sprintf('Save("%s", "%s")', $predicate, $object));
        return $this;
    }

    
    /**
     * forms the 'Intersect' method in the statement
     * @param Cayley_Path_Vertex $query
     * @return \Cayley_Path_Abstract
     */
    public function intersect(Cayley_Path_Vertex $query) {
        $this->push(sprintf('Intersect(%s)', $query));
        return $this;
    }

    
    /**
     * forms the 'Union' method in the statement
     * @param Cayley_Path_Vertex $query
     * @return \Cayley_Path_Abstract
     */
    public function union(Cayley_Path_Vertex $query) {
        $this->push(sprintf('Union(%s)', $query));
        return $this;
    }

    
    /**
     * forms the 'Follow' method in the statement
     * @param Cayley_Path_Morphism $morphism
     * @return \Cayley_Path_Abstract
     */
    public function follow(Cayley_Path_Morphism $morphism) {
        $this->push(sprintf('Follow(%s)', $morphism));
        return $this;
    }

    
 /**
  *    forms the 'FollowR' method in the statement
  * @param Cayley_Path_Morphism $morphism
  * @return \Cayley_Path_Abstract
  */
    public function followR(Cayley_Path_Morphism $morphism) {
        $this->push(sprintf('FollowR(%s)', $morphism));
        return $this;
    }

}
