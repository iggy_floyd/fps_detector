<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */



/**
 *Cayley_Statement_Interface defines an interface of the Cayley_Statement
 *
 * @author Igor Marfin
 */
interface Cayley_Statement_Interface {
    //put your code here

    /**
     * to push a statement to the internal container
     * @param string $statement
     */
     function push($statement);
     
     /**
      * to push a statement with arguments to the internal container
      * @param string $method
      * @param array $strings
      */
     function pushMethodWithListOfStrings($method, array $strings =  array());
     
     /**
      * to represent a statement as a string
      */
     function __toString();
    
}
