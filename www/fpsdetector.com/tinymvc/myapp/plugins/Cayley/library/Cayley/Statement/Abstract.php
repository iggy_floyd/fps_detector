<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 *  Cayley_Statement_Abstract  defines a statement in the cayley gremlin language
 *
 * @author Igor Marfin
 */
abstract class Cayley_Statement_Abstract implements Cayley_Statement_Interface
{
    /* @var $statements  Cayley_Statement_Abstract to store statements */

    protected $statements = array();

    /**
     * to add a new statement to the query
     * @param string $statement
     */
    public function push($statement)
    {
        $this->statements[] = $statement;
    }

    /**
     * to add a new statement with arguments
     * @param string $method
     * @param array $strings
     */
    public function pushMethodWithListOfStrings($method, array $strings = array())
    {
        $formated = '';
        if ($strings) {
            $formated = sprintf('"%s"', implode('", "', $strings));
        }
        $this->push(sprintf('%s(%s)', $method, $formated));
    }

    /**
     * string representation of the Cayley_Statement
     * @return string
     */
    public function __toString()
    {
        return implode('.', (array) $this->statements);
    }

}
