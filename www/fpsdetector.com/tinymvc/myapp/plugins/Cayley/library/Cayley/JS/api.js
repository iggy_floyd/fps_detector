
// version: v.0.0.2
// new: changed the logic of getConnectedFromBooking()

// version: v.0.0.3
// new: 'v2' functions for getAllBookingsID(), getAllID() and findBookingFromTarget()
/*
 solved the problems with memory leakage. But it's temporary fix! Should be replaced  later !!
 */
// version: v.0.0.4
// new: schema   subject "is" object is added
// new: 'v3' and 'v5' functions: getAllID_v3, viewAll_v3, viewAllConnected_v3, viewAllConnectedOnlyIf_v3, getConnectedProperty_v5 , EmitBooking, getConnectedPropertyWithBooking_v5, getConnectedPropertiesWithBooking_v5, getDefs
// new: 'v3' and 'v5' functions: findPropertyValues_v3, getConnectedPropertyFromTarget_v5, getConnectedPropertiesFromTarget_v5


// version: v.0.0.5
// new getAllID_v5(from,to) and  getsizeID_v5()
// update on _v5 functions: now we don't depend on the structure of the graph

// Utility functions


/**
 * returns size of the object
 * @param {type} obj
 * @returns {Number}
 */
Object.size = function (obj) {
    var s = 0, key;
    for (k in obj) {
        if (obj.hasOwnProperty(k))
            s++;
    }
    return s;
};

/**
 * push element to the array
 * @param {type} arr
 * @param {type} obj
 * @returns {Boolean}
 */
function include(arr, obj) {
    return (arr.indexOf(obj) !== -1);
}

/**
 * find properties of a booking
 * set needed members of the the property object
 * @param {type} x
 * @param {type} path
 * @returns {unresolved}
 */
function getBookingFromProperty(x, path)
{

    var arr = g.V(x).FollowR(path).TagArray();

    var props = g.V(x).FollowR(path).As("source").Follow(path).As("target").TagArray();

    for (a in props) {
        props[a]["source_color"] = "#880000";
        props[a]["target_color"] = "#66FF33";
        props[a].id = props[a].source;
    }
    

    return props;
}

/**
 * return properties of the booking
 * @param {type} x
 * @param {type} path
 * @param {type} toArray
 * @returns {getPropertiesFromBooking._res|Array}
 */
function getPropertiesFromBooking(x, path, toArray)
{
    toArray = typeof toArray !== 'undefined' ? toArray : true;


    var _res = [];
    var props = g.V(x).As("source").Follow(path).As("target").TagArray();
    

    if (toArray) {
        for (a in props) {
            _res.push(props[a].target);
        }
        
        return _res;
    }
    else {
        return props;
    }
}

/**
 * return property of the booking if property_name_pattern is found in the value
 * of the property
 * @param {type} booking
 * @param {type} property_name_pattern
 * @returns {getPropertiesFromBooking._res|Array|getTargetFromBooking.props}
 */
function getTargetFromBooking(booking, property_name_pattern)
{
    var props = getPropertiesFromBooking(booking, property_path);

    for (a in props) {
        if (props[a].indexOf(property_name_pattern) > 0) {
            return  props[a];
        }


    }
}

/**
 * return the number of all triplets stored by cayley
 * @returns {unresolved}
 */
function getSizeOfTriplets()
{
    return g.V().Out("has").TagArray().length + g.V().Out("follows").TagArray().length;
}

/**
 * return the follower of the property for  one-directed graph
 * @param {type} x
 * @param {type} path
 * @param {type} toArray
 * @returns {unresolved}
 */
function getFollower(x, path, toArray) {
 

    toArray = toArray || false;

    if (toArray) {
        return g.V(x).As("source").Follow(path).As("target").ToArray();
    }
    else {
        return g.V(x).As("source").Follow(path).As("target").TagArray();
    }
}

/**
 * return the follower of the property for  bi-directed graph
 * @param {type} x
 * @param {type} path
 * @param {type} toArray
 * @returns {_res|Array}
 */
function getFollowerBiDirectional(x, path, toArray) {
 

    toArray = toArray || false;

    _res = [];


    var forward = g.V(x).As("source").Follow(path).As("target");
    var backward = g.V(x).As("target").FollowR(path).As("source");




    if (toArray) {
        forward = forward.ToArray();
        backward = backward.ToArray();
    }
    else {
        forward = forward.TagArray();
        backward = backward.TagArray();
    }


    for (a in forward) {
        _res.push(forward[a]);
    }
    for (a in backward) { /*if ( !toArray) {backward[a].id=backward[a].target;}; */
        _res.push(backward[a]);
    }

    return _res;

}


/**
 * to print content of the array in the Gremlin environment
 * @param {type} obj
 * @returns {undefined}
 */
function EmitArray(obj) {
    for (a in obj) {
        g.Emit(obj[a]);
    }
}

/**
 * to print  propertis of the objects stored in the array in the Gremlin environment
 * @param {type} obj
 * @param {type} prop
 * @returns {undefined}
 */
function EmitArrayProp(obj, prop) {
    for (a in obj) {
        g.Emit(obj[a][prop]);
    }
}

/**
 * to dump array
 * @param {type} obj
 * @param {type} type
 * @returns {undefined}
 */
function PrintArray(obj, type) {
    for (a in obj) {
        console.log(obj[a]);
    }
}

/**
 * to dump properties of objects stored in an array
 * @param {type} obj
 * @param {type} prop
 * @returns {undefined}
 */
function PrintArrayProperty(obj, prop) {
    for (a in obj) {
        if (obj[a].hasOwnProperty(prop)) {
            console.log(obj[a][prop]);
        }
    }
}

/**
 * to dump x
 * @param {type} x
 * @returns {undefined}
 */
function Print(x) {
    console.log(x);
}

/**
 * to emmit array if it is not a part of the another array
 * @param {type} obj
 * @param {type} arr1
 * @param {type} prop
 * @returns {undefined}
 */
function EmitArrayInNotArray1(obj, arr1, prop) {
    for (a in obj)
        if (!include(arr1, obj[a][prop])) {
            g.Emit(obj[a]);
        }
}


// Putters
/**
 * put object to map
 * @param {type} map
 * @param {type} obj
 * @returns {undefined}
 */
function PutObjToMap(map, obj) {
    map[obj.id] = obj;
}
;

/**
 * put array to map
 * @param {type} map
 * @param {type} arr
 * @returns {undefined}
 */
function PutArrayToMap(map, arr) {
    for (a in arr) {

        map[changeId(arr[a])] = arr[a];
    }
}


/**
 * put objects to map if it doesn't have them
 * @param {type} map
 * @param {type} arr
 * @returns {undefined}
 */
function PutArrayToMapIfNotExisted(map, arr) {
    keys = Object.keys(map);
    for (a in arr) {
        if (!include(keys, changeId(arr[a]))) {
            map[changeId(arr[a])] = arr[a];
        }
        
    }
}




function PutObjToArrayProperty(arr, obj, prop) {
    if (obj.hasOwnProperty(prop)) { 
        arr.unshift(obj[prop]);
    }
}

function PutArrayToArrayProperty(arr1, arr2, prop) {
    for (a in arr1) {
        PutObjToArrayProperty(arr2, arr1[a], prop);
    }
}

function PutArray1ToArray2PropertyIfNotArrays3_4(arr1, arr2, arr3, arr4, prop)
{
    for (a in arr1)
    {
        if (!include(arr3, arr1[a][prop]) && !include(arr4, arr1[a][prop]))
            PutObjToArrayProperty(arr2, arr1[a], prop);
    }

}


// Popers
/**
 * return last element of the array
 * @param {type} arr
 * @returns {unresolved}
 */
function PopObjToArrayProperty(arr) {
    return arr.pop();
}



// Changer id
/**
 * return id of any triplet as "target_src"
 * @param {type} obj
 * @returns {String}
 */
function changeId(obj) {
    var src = obj.source.split("_")[1];
    src = src + obj.source.split("_")[2];
    var trgt = obj.target.split("_")[1];
    trgt = trgt + obj.target.split("_")[2];
    return trgt + "_" + src;
}


/**
 * the block determines all  types of edges in the graph
 * 
 * 
 */
var property_path = g.M().Out("has");
var property_source_color = g.M().Out("source_color");
var property_target_color = g.M().Out("target_color");
var property_follows = g.M().Out("follows");
var definition = g.M().Out("is");

/**
 * returns definition of the property
 * @param {type} what
 * @returns {unresolved}
 */
function getDefs(what) {
    return g.V().Has("is", what).TagArray();
}


/**
 * returns bookings  connected via one property
 * @param {type} node
 * @param {type} addBooking
 * @param {type} printdebug
 * @param {type} howmany
 * @param {type} recursive
 * @param {type} visited
 * @param {type} result
 * @returns {unresolved}
 */
function getConnectedFromProperty(node, addBooking, printdebug, howmany, recursive, visited, result)
{

    var addBooking = typeof addBooking !== 'undefined' ? addBooking : true;
    var printdebug = typeof printdebug !== 'undefined' ? printdebug : true;
    var howmany = typeof howmany !== 'undefined' ? howmany : -1;
    var recursive = typeof recursive !== 'undefined' ? recursive : false;
    var visited = typeof visited !== 'undefined' ? visited : [];
    var result = typeof result !== 'undefined' ? result : {};

    /*
     Print(["lenght of visited",visited.length]);
     Print(["result of result",Object.size(result)]);
     */
    //Print(howmany);

// temporary variables to store results
    var for_visit = [];



    /*
     Print("start");
     Print(["node:",node]);
     */


    var followers = getFollowerBiDirectional(node, property_follows);
    if (printdebug)
//EmitArray(followers); // to debug print
        EmitArrayInNotArray1(followers, visited, "id");
//else Print("Emitting of the followers is forbidden");

//PutArrayToMap(result,followers);
//PutArrayToMapIfNotExisted(result,followers);
    /*
     Print("id");
     PrintArrayProperty(followers,"id");
     Print("target");
     PrintArrayProperty(followers,"target");
     Print("source");
     PrintArrayProperty(followers,"source");
     */



//      PutArrayToArrayProperty(followers,for_visit,"id");
    PutArray1ToArray2PropertyIfNotArrays3_4(followers, for_visit, visited, for_visit, "id");
    visited.push(node);

    if (addBooking) {
        var booking = getBookingFromProperty(node, property_path);
        if (!include(visited, booking[0]["id"])) {
            if (printdebug)
                EmitArray(booking); // to debug print
//        else Print("Emitting of booking is forbidden");

//           PutArrayToMap(result,booking);
            PutArrayToMapIfNotExisted(result, booking);

        }
//         else Print(["booking will not be printed:",booking[0]["id"]]);

        if (recursive) {
//        Print("recursion");
            visited.push(booking[0]["id"]);
            var props = getPropertiesFromBooking(booking[0]["id"], property_path, false);
//        Print("props of the booking;")
//         PrintArrayProperty(props,"id");
            for (p in props)
                if (props[p]["id"].indexOf(node) === -1 && visited.indexOf(props[p]["id"]) === -1 && for_visit.indexOf(props[p]["id"]) === -1)
                { /*Print(["props to be visited: ",props[p]["id"]]); */
                    for_visit.unshift(props[p]["id"]);
                }

        }
    }

//PutArrayToMap(result,followers);
    PutArrayToMapIfNotExisted(result, followers);



    var _howmany = 0;
//Print("main loop starts:");
    while (for_visit.length > 0)
    {
        // Print(["_howmany =",_howmany]);
        // Print(["visited =",visited]);
        //    Print(["for visit =",for_visit]);
        //Print(["howmany =",visited.length]);
        //if ((_howmany > howmany)  &&  (howmany>0)) break;



        if ((visited.length > howmany) && (howmany > 0))
            break;

        node = for_visit.pop();

        //Print(["current in the loop node =",node]);

        if (include(visited, node)) {
            continue;
        }

        //Print(["current node is not in  visited"]);

        followers = getFollowerBiDirectional(node, property_follows);
        /*
         Print("id of followers in the loop:");
         PrintArrayProperty(followers,"id");
         Print("target of the followers:");
         PrintArrayProperty(followers,"target");
         Print("source of the followers:");
         PrintArrayProperty(followers,"source");
         */
        if (printdebug)
            //EmitArray(followers); // to debug print
            EmitArrayInNotArray1(followers, visited, "id");
        //else Print("Emitting of followers in loop is forbidden");



        //PutArrayToMap(result,followers);
        // PutArrayToMapIfNotExisted(result,followers);



//      PutArrayToArrayProperty(followers,for_visit,"id");
        PutArray1ToArray2PropertyIfNotArrays3_4(followers, for_visit, visited, for_visit, "id");
        visited.push(node);




        if (addBooking) {
            var booking = getBookingFromProperty(node, property_path);
            if (!include(visited, booking[0]["id"])) {
                if (printdebug)
                    EmitArray(booking); // to debug print
                //else Print("Emitting of booking in loop is forbidden");
                //PutArrayToMap(result,booking);
                PutArrayToMapIfNotExisted(result, booking);
            }
            //else Print(["booking will not be printed:",booking[0]["id"]]);

            if (recursive) {
//        Print("recursion");
                visited.push(booking[0]["id"]);
                var props = getPropertiesFromBooking(booking[0]["id"], property_path, false);
//         PrintArrayProperty(props,"id");
                for (p in props)
                    if (props[p]["id"].indexOf(node) === -1 && !include(visited, props[p]["id"]) && !include(for_visit, props[p]["id"]))
                    { /*Print(["props to be visited: ",props[p]["id"]]);*/
                        for_visit.unshift(props[p]["id"]);
                    }

            }
        }


//  PutArrayToMap(result,followers);
        PutArrayToMapIfNotExisted(result, followers);
        _howmany = _howmany + 1;

        //Print(for_visit); // to debug print
    }

    return result;
}


/**
 * returns bookings  connected via all found properties
 * @param {type} booking
 * @param {type} addBooking
 * @param {type} printdebug
 * @param {type} howmany
 * @returns {undefined}
 */
function getConnectedFromBooking(booking, addBooking, printdebug, howmany)
{
    var visited = [];
    var result = {};

    var props = getPropertiesFromBooking(booking, property_path);
    for (a in props) {
        result = getConnectedFromProperty(props[a], addBooking, printdebug, howmany, true, visited, result);
    }


    var printdebug = typeof printdebug !== 'undefined' ? printdebug : true;


    if (!(printdebug)) {

        EmitArray(result);


    }



}

/**
 * returns the list of all ids of the bookings
 * @param {type} path
 * @returns {Array}
 */
function getAllBookingsID(path)
{

    var bookings = g.V().FollowR(path).TagArray();
    var set = {};
    for (var i = 0; i < bookings.length; i++)
        set[bookings[i].id] = 1;

    return Object.keys(set);
}

/**
 * returns all bookings connected
 * @returns {undefined}
 */
function viewAllConnected()
{

    var ids = getAllBookingsID(property_path);
    for (a in ids)
        getConnectedFromBooking(ids[a]);
}


/**
 * returns all bookings unconnected
 * @returns {undefined}
 */
function viewAll()
{

    var ids = getAllBookingsID(property_path);
    for (a in ids) {
        var props = getPropertiesFromBooking(ids[a], property_path, false);
        EmitArray(props); // to debug print
    }


}

/**
 * finds bookings containing in their properties targetName
 * @param {type} targetName
 * @param {type} oneProperty
 * @returns {getPropertiesFromBooking._res|Array|findBookingFromTarget.props|findBookingFromTarget.res}
 */
function findBookingFromTarget(targetName, oneProperty)
{
    var oneProperty2 = typeof oneProperty !== 'undefined' ? oneProperty : false;


    var ids = getAllBookingsID(property_path);

    var res = [];

    for (a in ids) {
        var props = getPropertiesFromBooking(ids[a], property_path, false);

//    Print("id");
//    Print(ids[a]);

        for (b in props) {

            var str = props[b]["target"].split("_")[0];

            if (str.indexOf(targetName) >= 0) {
                if (oneProperty2) {

                    return props[b];
                }
                else
                {


                    res.push(props[b].target);
                }

            }
        }
    }

    return res;
}

/**
 * returns the id of the last booking
 * @returns {Object}
 */
function getLastID()
{
    var ids = getAllBookingsID(property_path);
    Print(ids);
    return ids.pop();
}

/**
 * returns the number of bookings
 * @returns {Number|Object@call;keys.length}
 */
function getSizeOfBookings()
{
    return getAllBookingsID(property_path).length;
}


/**
 * emmits the list of all ids 
 * @returns {undefined}
 */
function getAllID()
{

    var ids = getAllBookingsID(property_path);
    EmitArray(ids);
}


/**
 * 
 * returns the list of all ids
 */
var allbookingsid = [];
function getAllBookingsID_v2(path)
{
    Print("getAllBookingsID started");
    allbookingsid.length = 0;
    var bookings = g.V().FollowR(path).TagArray();
    var _set = {};
    var res = [];
    Print("getAllBookingsID bookings obtained ");
    for (var i = 0; i < bookings.length; i++) {
//    Print(bookings[i].id);
//    Print(["Current entry:", i]);
        if (typeof _set[bookings[i].id] === "undefined") {
//     Print(["Current entry to be set:", i]);
            _set[bookings[i].id] = 1;
            allbookingsid.push(bookings[i].id);
            //g.Emit(bookings[i].id);
        }  //else Print(["Current entry is not set:", i]);
//    Print(["total length is ",bookings.length]);
    }


//  Print("getAllBookingsID finished");
    //var kes= Object.keys(set);
    //Print("keys obtained");
//          Print(["size of keys:",allbookingsid.lenght]);
    //return res;
    delete bookings;
    delete _set;
}


/**
 * emmit the list of all ids
 * @returns {undefined}
 */
function getAllID_v2()
{
    //Print("getAllID started");
    //var ids = getAllBookingsID(property_path);
    getAllBookingsID_v2(property_path);
    //Print("getAllID before EmitArray");
    //PrintArray(allbookingsid);
    EmitArray(allbookingsid);
    //Print("getAllID finished");
    delete allbookingsid;
}

/**
 * * finds bookings containing in their properties targetName
 * @param {type} targetName
 * @param {type} oneProperty
 * @returns {Array|findBookingFromTarget_v2.res}
 */
function findBookingFromTarget_v2(targetName, oneProperty)
{


    var oneProperty2 = typeof oneProperty !== 'undefined' ? oneProperty : false;
    getAllBookingsID_v2(property_path);
    var res = [];


    for (var i = 0; i < allbookingsid.length; i++) {

        var book = allbookingsid[i];
        //Print(i);

        var props = getPropertiesFromBooking_v2(book, property_path);
        for (var j = 0; j < props.length; j++) {


            var prop = props[j];
            var _str = prop["target"].split("_")[0];

            if (_str.indexOf(targetName) >= 0) {
                if (oneProperty2) {

                    return prop["target"];
                }
                else
                {


                    res.push(prop["target"]);
                }

            }

        }
        delete props;

    }


    delete allbookingsid;
    return res;
}




function getPropertiesFromBooking_v2(x, path)
{

    var _tmp = g.V(x).As("source").Follow(path).As("target").TagArray();
    return _tmp;

}



/**
 * 
 * returns the list of all ids
 */
var allbookings_v2 = [];
function getAllID_v3()
{
    allbookings_v2.length = 0;
    allbookings_v2 = getDefs("ID");
    EmitArrayProp(allbookings_v2, "id");
    delete allbookings_v2;
}

/**
 * to emmit the number ( limit_edges) of unconnected bookings
 * @param {int} limit_edges
 */
function viewAll_v3(limit_edges)
{

    var limit_edges2 = typeof limit_edges !== 'undefined' ? limit_edges : 100;
    if (limit_edges2 < 0)
        g.V().As("source").Follow(property_path).As("target").All();
    
    else
        g.V().Has("is", "ID").ForEach(limit_edges2, function (d) {
            EmitBooking(d["id"]);
        });


}

/**
 *  to emmit the number of bookings connected each other by the the limit_edges number  of edges
 * @param {int} limit_nodes
 * @param {int} limit_edges
 */
function viewAllConnected_v3(limit_nodes, limit_edges)
{
    var limit_nodes2 = typeof limit_edges !== 'undefined' ? limit_nodes : 100;
    var limit_edges2 = typeof limit_edges !== 'undefined' ? limit_edges : 100;

    if (limit_nodes2 < 0)
        g.V().As("source").Follow(property_path).As("target").All();
//  else  g.V().As("source").Follow(property_path).As("target").GetLimit(limit_nodes2);
    else
        g.V().Has("is", "ID").ForEach(limit_nodes2, function (d) {
            EmitBooking(d["id"]);
        });


//   Print("done vtx2");
    if (limit_edges2 < 0) {
        g.V().As("source").Follow(property_follows).As("target").All();
    } else {
        g.V().As("source").Follow(property_follows).As("target").GetLimit(limit_edges2);
    }

}


/**
 * to emmit the number of bookings connected  by a number of (up to  limit_edges) the 'category' properties
 * @param {int} limit_nodes
 * @param {int} limit_edges
 * @param {string} category
 */
function viewAllConnectedOnlyIf_v3(limit_nodes, limit_edges, category)
{

    var limit_nodes2 = typeof limit_nodes !== 'undefined' ? limit_nodes : 100;
    var limit_edges2 = typeof limit_edges !== 'undefined' ? limit_edges : 100;
    var category2 = typeof category !== 'undefined' ? category : "Booker";


//    Print("done1");

    if (limit_nodes2 < 0)
        g.V().As("source").Follow(property_path).As("target").All();
    //else g.V().As("source").Follow(property_path).As("target").GetLimit(limit_nodes2);
    else
        g.V().Has("is", "ID").ForEach(limit_nodes2, function (d) {
            EmitBooking(d["id"]);
        });

//Print("done2");
    if (limit_edges2 < 0) {
        //Print("done3");
        g.V().Has("is", category2).As("source").Follow(property_follows).As("target").All();
    } else {

        g.V().Has("is", category2).As("source").Follow(property_follows).As("target").GetLimit(limit_edges2);
//    Print("done4");
    }


}


/**
 * to emmit the booking with booking_id
 * @param {string} booking_id
 */
function EmitBooking(booking_id)
{
    if (typeof booking_id === 'undefined')
        return;
    g.V(booking_id).As("source").Follow(property_path).As("target").All();
}


/**
 * to emmit connected properties of the type 'prop'
 * @param {type} booking_id
 * @param {type} depth
 * @param {type} prop
 * @returns {undefined}
 */
function getConnectedProperty_v5(booking_id, depth, prop)
{
    if ((typeof booking_id === 'undefined') || (typeof depth === 'undefined') || (typeof prop === 'undefined'))
        return;

    var booker = g.V(booking_id).As("source").Follow(property_path).Has("is", prop).As("target").TagArray();

    var _res = [];
    var _resR = [];
    var _forward = [];
    var _backward = [];
    var _visited = [];
    var _found;
    var _foundR;
    var _visited = [];
    var _forvisit = [];

    var _curdepth = 0;
    for (d in booker ) {
        _forvisit.push(booker[d].target);
    }
//    _forvisit.push(booker[0].target);

    while (_curdepth < depth) {
        if (_forvisit.length === 0)
            break;
        var _node2vist = _forvisit.pop();
        //Print(["curdepth",_curdepth]);
        //Print(["_node2vist",_node2vist])

        _res = g.V(_node2vist).As("source").Follow(property_follows).As("target").TagArray();
        _resR = g.V(_node2vist).As("source").FollowR(property_follows).As("target").TagArray();

        var i = 0;
        for (d in _res)
        {

            if (i > depth)
                break;
            if (!(include(_visited, _res[d].target) || include(_forvisit, _res[d].target)))
            {
                //Print(["Adding" ,_res[d].target]);
                _forvisit.unshift(_res[d].target);
            }

            if (!(include(_visited, _res[d].target)))
            {
                if (_curdepth < depth)
                    g.Emit(_res[d]);
                _curdepth++;
            }
            i++;
        }

        var j = 0;
        for (d in _resR)
        {
            if (j > depth)
                break;
            if (!(include(_visited, _resR[d].target) || include(_forvisit, _resR[d].target)))
            {
                //Print(["AddingR" ,_resR[d].target]);
                _forvisit.unshift(_resR[d].target);
            }

            if (!(include(_visited, _resR[d].target)))
            {
                if (_curdepth < depth)
                    g.Emit(_resR[d]);
                _curdepth++;
            }
            j++;
        }

        _visited.push(_node2vist);


    }

}

/**
 * to emmit the bookings connected by the property 'prop'
 * @param {string} booking_id
 * @param {int} depth
 * @param {string} prop
 * @param {array} visited
 * @param {int} curdepth
 * @param {int} maxconnections
 * @param {bool} mode
 * @param {int} maxloop
 */
function getConnectedPropertyWithBooking_v5(booking_id, depth, prop, visited, curdepth, maxconnections, mode, maxloop)
{
    if ((typeof booking_id === 'undefined') || (typeof depth === 'undefined') || (typeof prop === 'undefined'))
        return;

    var _visited = typeof visited !== 'undefined' ? visited : [];
    var _curdepth = typeof curdepth !== 'undefined' ? curdepth[0] : 0;
    var _maxconnections = typeof maxconnections !== 'undefined' ? maxconnections : 20;
    var _mode = typeof mode !== 'undefined' ? mode : false;
    var _maxloop = typeof maxloop !== 'undefined' ? maxloop : 10;



    if (_curdepth > depth)
        return;

    var booking = g.V(booking_id).As("source").Follow(property_path).As("target").TagArray();
//    Print(mydump(booking[0],0));
    EmitArray(booking);
    var booker = g.V(booking_id).As("source").Follow(property_path).Has("is", prop).As("target").TagArray();
//    Print(mydump(booker,0));
    var _res = [];
    var _resR = [];

    var _forvisit = [];

    var __maxconnections = 0;



    _visited.push(booking[0].source);
    for (d in booker ) {
            _forvisit.push(booker[d].target);
    }
//    _forvisit.push(booker[0].target);
    while ((_curdepth < depth) && (__maxconnections < _maxconnections))
    {



        if (_forvisit.length === 0) {
            break;
        }
        var _node2vist = _forvisit.pop();



        _res = g.V(_node2vist).As("source").Follow(property_follows).As("target").TagArray();
        _resR = g.V(_node2vist).As("source").FollowR(property_follows).As("target").TagArray();


        if (_mode) {

            var i = 0;
            for (d in _res) {

                if (_curdepth > depth)
                {
                    curdepth[0] = _curdepth;
                    return;
                }

                if (i > _maxloop)
                    break;
                i++;

                if (!(include(_visited, _res[d].target) || include(_forvisit, _res[d].target)))
                {

                    _forvisit.unshift(_res[d].target);
                }

                if (!(include(_visited, _res[d].target)))
                {
                    if (_curdepth < depth)
                        if (__maxconnections < _maxconnections)
                            g.Emit(_res[d]);
                    __maxconnections++;

                    var bookid = g.V(_res[d].target).FollowR(property_path).As("target").TagArray();
                    if (!(include(_visited, bookid[0].target)))
                    {

                        _visited.push(bookid[0].target);
                        if (_curdepth < depth)
                            EmitBooking(bookid[0].target);
                        _curdepth++;
                    }
                }
            }

            var j = 0;

            for (d in _resR) {

                if (_curdepth > depth) {
                    curdepth[0] = _curdepth;
                    return;
                }
                if (j > _maxloop)
                    break;
                j++;


                if (!(include(_visited, _resR[d].target) || include(_forvisit, _resR[d].target)))
                {

                    _forvisit.unshift(_resR[d].target);
                }

                if (!(include(_visited, _resR[d].target)))
                {
                    if (_curdepth < depth)
                        if (__maxconnections < _maxconnections)
                            g.Emit(_resR[d]);
                    __maxconnections++;

                    var bookid = g.V(_resR[d].target).FollowR(property_path).As("target").TagArray();
                    if (!(include(_visited, bookid[0].target)))
                    {

                        _visited.push(bookid[0].target);
                        if (_curdepth < depth)
                            EmitBooking(bookid[0].target);
                        _curdepth++;
                    }

                }
            }
        }

        else {

            var i = 0;
            for (d in _res)
            {
                if (i > _maxloop)
                    break;

                if (_curdepth > depth)
                    return;



                if (!(include(_visited, _res[d].target) || include(_forvisit, _res[d].target)))
                {

                    _forvisit.unshift(_res[d].target);
                }

                if (!(include(_visited, _res[d].target)))
                {
//               if (_curdepth<depth)
                    if (__maxconnections < _maxconnections) {
                        g.Emit(_res[d]);
                        var bookid = g.V(_res[d].target).FollowR(property_path).As("target").TagArray();
                        if (!(include(_visited, bookid[0].target)))
                        {

                            _visited.push(bookid[0].target);
                            if (_curdepth < depth) {
                                EmitBooking(bookid[0].target);
                                _curdepth++;
                            }
                        }
                        __maxconnections++;
                    }

                }


                i++;
            }

            var j = 0;
            for (d in _resR)
            {
                if (j > _maxloop)
                    break;

                if (_curdepth > depth)
                    return;

                if (!(include(_visited, _resR[d].target) || include(_forvisit, _resR[d].target)))
                {

                    _forvisit.unshift(_resR[d].target);
                }

                if (!(include(_visited, _resR[d].target)))
                {

                    //               if (_curdepth<depth)
                    if (__maxconnections < _maxconnections) {
                        g.Emit(_resR[d]);
                        var bookid = g.V(_resR[d].target).FollowR(property_path).As("target").TagArray();
                        if (!(include(_visited, bookid[0].target)))
                        {

                            _visited.push(bookid[0].target);
                            if (_curdepth < depth) {
                                EmitBooking(bookid[0].target);
                                _curdepth++;
                            }
                        }
                        __maxconnections++;
                    }

                }


                j++;
            }

        }

        _visited.push(_node2vist);


    }



    if (_mode)
        curdepth[0] = _curdepth;


}

/**
 * to dump objects
 * @param {array} arr
 * @param {int} level
 * @returns {string}
 */

function mydump(arr,level) {
    var dumped_text = "";
    if(!level) level = 0;

    var level_padding = "";
    for(var j=0;j<level+1;j++) level_padding += "    ";

    if(typeof(arr) == 'object') {
        for(var item in arr) {
           var value = arr[item];
            if(typeof(value) == 'object') {
                dumped_text += level_padding + "'" + item + "' ...\n";
                dumped_text += mydump(value,level+1);
            } else {
                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
            }
        }
    } else {
        dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
    }
    return dumped_text;
}



/**
 * emmits all connected properties  and bookings
 * @param {string} booking_id
 * @param {int} depth
 * @param {int} maxconnection
 * @param {bool} mode
 */
function getConnectedPropertiesWithBooking_v5(booking_id, depth, maxconnection, mode)
{
    if ((typeof booking_id === 'undefined') || (typeof depth === 'undefined') || (typeof maxconnection === 'undefined') || (typeof mode === 'undefined'))
        return;
    var visited = [];
    var curdepth = [0];

    //Print(booking_id);
    //var propsdefs2 = g.V("6345789-20_ID").As("source").Follow(property_path).TagArray();
    //Print(mydump(propsdefs2,0));
     var propsdefs = g.V(booking_id).As("source").Follow(property_path).Follow(definition).As("target").TagArray();
    // Print(mydump(propsdefs,0));

     for (a in propsdefs) {
            getConnectedPropertyWithBooking_v5(booking_id, depth, propsdefs[a]["target"], visited, curdepth, maxconnection, mode);
        }
  
}

/**
 * to find unconnected properties of type 'property' containing 'targetName' in their values
 * @param {string} targetName
 * @param {string} property
 * @param {int} limit
 * @returns {Array|findPropertyValues_v3.res}
 */
function findPropertyValues_v3(targetName, property, limit)
{

    var res = [];
    if (typeof targetName === 'undefined' || typeof property === 'undefined' || typeof limit === 'undefined')
        return res;

    var _cur = 0;
    g.V().As("source").Has("is", property).ForEach(
            function (d) {
                if (_cur > limit)
                    return res;

                var _str = d.source.split("_")[0];
                if (_str.indexOf(targetName) >= 0) {
                    res.push(d.source);
                    _cur++;
                }
            }

    );

    return res;
}

/**
 * to emmit properties of type 'property' containing 'targetName' in their values. All returned properties are connected
 * to each other.
 * @param {string} targetName
 * @param {int} depth
 * @param {string} prop
 */
function getConnectedPropertyFromTarget_v5(targetName, depth, prop)
{
    if ((typeof targetName === 'undefined') || (typeof depth === 'undefined') || (typeof prop === 'undefined'))
        return;


    var booker = findPropertyValues_v3(targetName, prop, 0);

    var _res = [];
    var _resR = [];
    var _forward = [];
    var _backward = [];
    var _visited = [];

    var _visited = [];
    var _forvisit = [];

    var _curdepth = 0;

      for (a in booker) {
    if (typeof booker[a] !== 'undefined')
        _forvisit.push(booker[a]);

      }
//    if (typeof booker[0] !== 'undefined')
//        _forvisit.push(booker[0]);

    while (_curdepth < depth) {
        if (_forvisit.length === 0)
            break;
        var _node2vist = _forvisit.pop();
        //Print(["curdepth",_curdepth]);
        //Print(["_node2vist",_node2vist])

        _res = g.V(_node2vist).As("source").Follow(property_follows).As("target").TagArray();
        _resR = g.V(_node2vist).As("source").FollowR(property_follows).As("target").TagArray();

        var i = 0;
        for (d in _res)
        {

            if (i > depth)
                break;
            if (!(include(_visited, _res[d].target) || include(_forvisit, _res[d].target)))
            {
                // Print(["Adding" ,_res[d].target]);
                _forvisit.unshift(_res[d].target);
            }

            if (!(include(_visited, _res[d].target)))
            {
                if (_curdepth < depth)
                    g.Emit(_res[d]);
                _curdepth++;
            }
            i++;
        }

        var j = 0;
        for (d in _resR)
        {
            if (j > depth)
                break;
            if (!(include(_visited, _resR[d].target) || include(_forvisit, _resR[d].target)))
            {
                //Print(["AddingR" ,_resR[d].target]);
                _forvisit.unshift(_resR[d].target);
            }

            if (!(include(_visited, _resR[d].target)))
            {
                if (_curdepth < depth)
                    g.Emit(_resR[d]);
                _curdepth++;
            }
            j++;
        }

        _visited.push(_node2vist);


    }

}

/**
 *  to emmit all properties  containing 'targetName' in the values and to connect them
 * @param {string} targetName
 * @param {int} depth
 */
function getConnectedPropertiesFromTarget_v5(targetName, depth)
{

  // get first id
  var first_id = g.V().As("source").Has("is", "ID").Follow(definition).ForEach(1,
  function (d) {
        return d["source"];
  } );
    
    
    
     var propsdefs = g.V(first_id).As("source").Follow(property_path).Follow(definition).As("target").TagArray();
     for (a in propsdefs) {
         getConnectedPropertyFromTarget_v5(targetName, depth,propsdefs[a]["target"]);
        }
    
}


/**
 * to emmit all IDs from the booking 'from' to the booking 'to'
 * @param {int} from
 * @param {int} to
 */

function getAllID_v5(from, to)
{
    var counter = 0;
    g.V().Has("is", "ID").ForEach(
            function (d) {
                if (counter > to - 1)
                    return;
                if (counter >= from && counter < to)
                    g.Emit(d);

                counter++;
            }
    );

return;
}

/**
 * returns the  number of bookings 
 * @returns a JSON of {id:x,source:y,target:y}
 */
function getsizeID_v5()
{
    var counter = 0;
    g.V().Has("is", "ID").ForEach(
            function (d) {
                counter++;
            }
    );
    return {"id": "1", "source": counter, "target": counter};

}

/**
 * to emmit all bookings
 */
function getAllID_v3()
{
    g.V().Has("is", "ID").ForEach(
            function (d) {
                g.Emit(d);
            }
    );
}
