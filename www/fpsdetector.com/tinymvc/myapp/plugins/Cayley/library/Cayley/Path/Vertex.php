<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Cayley_Path_Vertex represents a Vertex in a Graph
 *
 * @author Igor Marfin
 */
class Cayley_Path_Vertex extends Cayley_Path_Abstract
{

    /**
     * executes 'All' method
     * @return \Cayley_Path_Vertex
     */
    public function all()
    {
        $this->push('All()');
        return $this;
    }

    /**
     * executes 'GetLimit' method
     * @param int $limit
     * @return \Cayley_Path_Vertex
     */
    public function getLimit($limit)
    {
        $this->push(sprintf('GetLimit(%d)', $limit));
        return $this;
    }

}
