<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Description of HttpException
 *
 * @author Igor Marfin
 */




class FPSDetector_Client_Exception_HttpException extends RuntimeException {
    
    /**
     * Error codes of the Http protocol
     */
    const NOT_MODIFIED = 304;
	const BAD_REQUEST = 400;
	const NOT_FOUND = 404;
	const NOT_ALOWED = 405;
	const CONFLICT = 409;
	const PRECONDITION_FAILED = 412;
	const INTERNAL_ERROR = 500; 

	public function __construct(Exception $exception)
	{

		$data = json_decode($exception->getMessage(),true); 
                
                if (!isset($data)) {
                    parent::__construct($exception->getMessage(),$exception->getCode());
                    return;
                }
                
                if (array_key_exists('result', $data)) {

                        $data = $data['result'];
                    if (is_string($data)) {
                        $data = json_decode($data,true); 
                    }    
                        
                    if (array_key_exists('error', $data)) {

                        parent::__construct($data['error'],$exception->getCode());
                        return;
                    }
                }

                parent::__construct(json_encode($data),$exception->getCode());
                return;

                
	}
    
}
