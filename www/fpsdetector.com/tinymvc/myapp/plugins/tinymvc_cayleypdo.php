
<?php



/* * *
 * Name:       TinyMVC_CayleyPDO
 * * */

// ------------------------------------------------------------------------


include_once dirname(__FILE__) . '/Cayley/SplClassLoader.php';



/**
 * 
 * PDO database access
 *
 * @author Igor Marfin
 */
class TinyMVC_CayleyPDO {

    /**
     * $pdo
     *
     * the PDO object handle
     *
     * @access	public
     */
    public $pdo = null;
    public $classLoader = null;

    /**
     * class constructor
     *
     * @access	public
     */
    function __construct($config) {




        $classLoader = new SplClassLoader(null, dirname(__FILE__) . "/Cayley/library/");
        $classLoader->register();

        if (!class_exists('Cayley_Client_Adapter_Http', true)) {
            throw new Exception("PHP Cayley package is required.");
        }
        if (!class_exists('Cayley_Client_Exception_HttpException', true)) {
            throw new Exception("PHP Cayley package is required.");
        }
        if (!class_exists('Cayley_Statement_Abstract', true)) {
            throw new Exception("PHP Cayley package is required.");
        }
        if (!class_exists('Cayley_Debugger', true)) {
            throw new Exception("PHP Cayley package is required.");
        }
       
     



        if (empty($config))
            throw new Exception("database definitions required.");


        // attempt to instantiate PDO object and database connection 
        try {

            if ($config['type-bk-tree'] === 0) {
                $this->pdo = new Cayley_Client(
                        Cayley_Client_Adapter_Http::connect($config['host_r'], $config['port_r']), Cayley_Client_Adapter_Http::connect($config['host_w'], $config['port_w'])
                );
            } else {

                $this->pdo = new Cayley_Client(
                        Cayley_Client_Adapter_Http::connect($config['host_r'], $config['port_r']), Cayley_Client_Adapter_Pipe::connect($config['pipe_read'], $config['pipe_write'], $config['key'])
                                ->setPipesize($config['pipe_size'])
                );
            }

           
        } catch (Exception $e) {
            throw new Exception(sprintf("Can't connect to the Cayley database.   Error: %s", $e->getMessage()));
        }
    }

}

?>
