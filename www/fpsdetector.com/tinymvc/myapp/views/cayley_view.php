<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/strict.dtd">


<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Test CayleyDB</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="javascripts/jquery.cookies.2.2.0.min.js"></script>

  <script>


$(function() {
    //  jQueryUI 1.10 and HTML5 ready
    //      http://jqueryui.com/upgrade-guide/1.10/#removed-cookie-option
    //  Documentation
    //      http://api.jqueryui.com/tabs/#option-active
    //      http://api.jqueryui.com/tabs/#event-activate
    //      http://balaarjunan.wordpress.com/2010/11/10/html5-session-storage-key-things-to-consider/
    //
    //  Define friendly index name
    var index = 'key';
    //  Define friendly data store name
    var dataStore = window.sessionStorage;
    //  Start magic!
    try {
        // getter: Fetch previous value
        var oldIndex = dataStore.getItem(index);
    } catch(e) {
        // getter: Always default to first tab in error state
        var oldIndex = 0;
    }
    $('#tabs').tabs({
        // The zero-based index of the panel that is active (open)
        active : oldIndex,
        // Triggered after a tab has been activated
        activate : function( event, ui ){
            //  Get future value
            var newIndex = ui.newTab.parent().children().index(ui.newTab);
            //  Set future value
            dataStore.setItem( index, newIndex);
        }
    });
});



  </script>

    <style type="text/css">
        * { margin: 0; padding: 0; }
        #page-wrap { width: 960px; margin: 100px auto; }
        h1 { font: 36px Georgia, Serif; margin: 20px 0; }
        .group:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
        p { margin: 0 0 10px 0; }



    #mynetwork
    {
    float: left;
    width: 1600px;
    height: 1200px;

    }
 
    </style>


</head>


<body>


<h1>Proof of the Concept: "FPS   && Cayley DB"</h1> <br> <br> <br>


 
<div id="tabs">
  <ul>
	<li><a href="#tabs-1">Read Request Test</a></li>
	<li><a href="#tabs-2">Write Request Test</a></li>
	<li><a href="#tabs-3">Service (Dump) Requests Test</a></li>
	<li><a href="#tabs-4">GremlinQuery (Extension of the CayleyPDO) Requests Test</a></li>
  </ul>

  <div id="tabs-1">
      <div>
      [onload;file=cayley_view_sub1.php;getbody]
      </div>
  </div>

  <div id="tabs-2">
      <div>
      [onload;file=cayley_view_sub2.php;getbody]
      </div>
  </div>

  <div id="tabs-3">
      <div>
      [onload;file=cayley_view_sub3.php;getbody]
      </div>
  </div>

  <div id="tabs-4">
      <div>
      [onload;file=cayley_view_sub4.php;getbody]
      </div>
  </div>

</div>
 
</body>
</html>



