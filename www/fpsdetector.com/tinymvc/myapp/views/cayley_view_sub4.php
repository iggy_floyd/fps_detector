

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title> Cayley Test </title>

    <style type="text/css">
        body {
            font: 10pt arial;
        }



    </style>



<body>


<h1>Test of the GremlinRequests </h1> <br>  <br> 

<p>
This extension of the CayleyPDO is inspired by the project
<br>
<br>
<pre>

	https://github.com/mcuadros/php-cayley

</pre>
<br>
You can make the test after starting the cayley database:
<pre> 
	./cayley http --dbpath=testdata.nq
</pre>
<br>
where testdata.nq  contains
</p>

<pre>
	alice loves bob .
	bob loves alice .
	charlie loves bob .
	dani loves charlie .
	dani loves alice .
	alice is "cool" .
	bob is "cool" .
	charlie is "not cool" .
	dani is "not cool" .
</pre>

<br>
<br>


<!--
         the "gremlinquery"  form
-->  
      <p>       
     [onshow.form_gremlinquery.begin;strconv=no]  
     <p> <div>[form_gremlinquery.label;strconv=no;block=p] </div>  <div> [form_gremlinquery.input;strconv=no]   </div>  </p>
     <p>   [onshow.form_gremlinquery.submit.input;strconv=no]  </p> 
     [onshow.form_gremlinquery.end;strconv=no] 

     </p>

<br>
<br>

<p>
Following the examples given at 
</p>
<br>
<br>

<pre>
https://github.com/google/cayley
https://github.com/google/cayley/blob/master/docs/GremlinAPI.md
</pre>
<br>
<br>

you can try to test the  request like these
<br>
<br>

<pre>
graph()->Vertex()->Has(is,cool)->All()
graph()->Vertex()->Out(loves)->All()
graph()->Vertex(dani)->Out(loves)->All()
graph()->Vertex(dani)->Out(is)->All()
</pre>

<br>
<br>



<h1> Returned  result : </h1>  <p>  [onshow.gremlinqueryresult; strconv=no;noerr]  </p> <br> <br>


</body>
</html>









