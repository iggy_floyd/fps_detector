<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Description of FPSDetector_Controller:
 * 
 * The main controller for the Proof of Concept of the FPS detector Library
 *
 * @author Igor Marfin
 */
class FPSDetector_Controller extends TinyMVC_Controller
{

      function index()
     {
          
          /*
           *  Model initialization
           * 
           */
          
          //load FPSDetector_Model to the controller.
          
          $this->load->model('FPSDetector_Model','fpsdetector',null,'fpsdetector');
          
          

          /*
           * Some needed variables to be rendered at the pages
           * 
           */
            
           // fps_detector_path          
           $this->tbs->VarRef['fps_detector_path'] =  preg_replace('/www(.*)/','',dirname(__FILE__));

           
                
           /**
            * 
            * clean logs
            * 
            */

            $handle = fopen ($this->tbs->VarRef['fps_detector_path'].'logs' , "w+");
            fclose($handle);
           
          
           /*
            * 
            * Forms initialization
            * 
            * 
            */
           $this->load->model('FPSDetector_Forms_Model','forms_model');           
           $forms = $this->forms_model->get_forms();
           
           foreach($this->forms_model->forms as $name) {
               $this->tbs->VarRef[$name] =  $forms[$name]->getHtml();
           }
           
           
           /**
            * 
            * Here controller decides what to do depending on the user input
            * 
            */
           
           $response_rpc=null; // response from RPC server
           $exceptions  = array();
           
           // Case of the 'Prepare Detector Test'
           $name_form = 'prepare_detector_form';                      
           if( $forms[$name_form]->isSubmittedAndValid() ) {                              
               $form_values = array();
               $_vals =  $forms[$name_form]->getValues();

               foreach ($this->tbs->VarRef[$name_form]['inputs'] as $item) {
                   $label = preg_replace('/ /','_',$item["label"]);                  
                    $form_values[$label] = $_vals[$label];                    
		}
                $form_values['Hidden'] = $_vals['Hidden'];
                // this illustrates how the Client to communicate with RPC can be used                
                if ( isset($form_values['Name_Of_The_Classifier']) && 
                        trim($form_values['Name_Of_The_Classifier']) !== '' && 
                        is_numeric($form_values['Train_Sample_Size']) &&
                        trim($form_values['Dataset_File']) !== ''  && 
                        trim($form_values['Dataset_File']) !== 'Unknown' 
                        ){                     
                    
                        // construct the right name of the file to be used for training
                        $form_values['Dataset_File'] = $this->tbs->VarRef['fps_detector_path'].$form_values['Dataset_File'];                      

                        // first, do ini of the whole system 
                        $isHostAvailable=true;
                        try {
                                $response_rpc = $this->fpsdetector->getClient()->initProcess($form_values['Dataset_File']);
                            } catch (FPSDetector_Client_Exception_HttpException $e) {
                                 FPSDetector_Debugger::log($e->getMessage());
                                 $exceptions[]=array('type'=>$e->getMessage());
                                 $isHostAvailable = false;
                            }
                        
                        // second, wait until the first step is ready
                        while (true && $isHostAvailable) {    
                        
                            try {
                                $response_rpc = $this->fpsdetector->getClient()->initModelFeatures();
                                if ($response_rpc->getStatus() == 0 ) {
                                    break;
                                }
                                
                            } catch (FPSDetector_Client_Exception_HttpException $e) {
                                 FPSDetector_Debugger::log($e->getMessage());
                                 $exceptions[]=array('type'=>$e->getMessage());
                            }
                        }
                        
                        // third, add a classifier
                         try {
                                $response_rpc = $this->fpsdetector->getClient()->addClassifier(
                                $form_values['Classifier'],
                                trim($form_values['Name_Of_The_Classifier'])
                                );
                            } catch (FPSDetector_Client_Exception_HttpException $e) {
                                 FPSDetector_Debugger::log($e->getMessage());
                                 $exceptions[]=array('type'=>$e->getMessage());
                                 $isHostAvailable = false;
                            }
                            
                             // fourth, train classifiers
                        while (true && $isHostAvailable) {    
                        
                            try {
                                $response_rpc = $this->fpsdetector->getClient()->trainClassifiers();
                                if ($response_rpc->getStatus() == 0 ) {
                                    break;
                                }
                                
                            } catch (FPSDetector_Client_Exception_HttpException $e) {
                                 FPSDetector_Debugger::log($e->getMessage());
                                 $exceptions[]=array('type'=>$e->getMessage());
                            }
                        }
        
                            // That's it. We are ready for simulations!
                }
                
           }
           
           
           // Case of the 'Simulate Detector Test'
           $name_form = 'simulate_detector_form';                      
           if( $forms[$name_form]->isSubmittedAndValid() ) {                              
               $form_values = array();
               $_vals =  $forms[$name_form]->getValues();

               foreach ($this->tbs->VarRef[$name_form]['inputs'] as $item) {
                   $label = preg_replace('/ /','_',$item["label"]);                  
                    $form_values[$label] = $_vals[$label];                    
		}
                
                // this illustrates how the Client to communicate with RPC can be used                
                if (    trim($form_values['Dataset_File']) !== ''  && 
                        trim($form_values['Dataset_File']) !== 'Unknown' 
                   ){
                    
                        // construct the right name of the file to be used for training
                        $form_values['Dataset_File'] = $this->tbs->VarRef['fps_detector_path'].$form_values['Dataset_File'];                      
                        $handle = fopen($form_values['Dataset_File'], "r");
                        
                        while(!feof($handle)){
                            $line = fgets($handle);
                            # do all stuff with the $line
                            
                            $data = json_decode($line,true);
                            FPSDetector_Debugger::log($data['status']);
                            
                            
                            try {
                                $response_rpc = $this->fpsdetector->getClient()->predictValidation($line,$data['status']);
                                
                            } catch (FPSDetector_Client_Exception_HttpException $e) {
                                 FPSDetector_Debugger::log($e->getMessage());
                                 $exceptions[]=array('type'=>$e->getMessage());
                            }
                            
                        }
                        fclose($handle);

                    
                }
                
           }
           
          
          
          /*
           * 
           *  View Layer definitions
           * 
           */
          

           // display the view layer with substitution templates 
	   $this->tbs->LoadTemplate('fpsdetector_view'); # load a template
           
           // render forms defined previously
           foreach($this->forms_model->forms as $name) {
               $this->tbs->MergeBlock($name, 'array', $name.'[inputs]'); // to produce the form from a template
           }
           
           
           // add a table with exceptions caught
           $this->tbs->MergeBlock('excpt', $exceptions); // to fill the template of the  table in the view layer
           $this->tbs->MergeBlock('excpt2', $exceptions); // to fill the template of the  table in the view layer
           $this->tbs->Show();
      }
    //put your code here
}
