<?php

class Cayley_Controller extends TinyMVC_Controller
{

      function index()
     {


//  to load Cayley PDO to the controller.
   	 $this->load->model('Cayley_Model','cayley',null,'cayley');



// to load the 'form' generator to the controller.
// That's not a real model object. It can't work with PDO!
 	 $this->load->model('Form_Booking_Model','formbooking');


	 $form_read_obj = $this->formbooking->get_form();
	 $form_read_obj =$form_read_obj["read"];
         $form_read = $form_read_obj->getHtml();
         $this->tbs->VarRef['form_read'] =  $form_read;

	 $form_write_obj = $this->formbooking->get_form();
	 $form_write_obj =$form_write_obj["write"];
         $form_write = $form_write_obj->getHtml();
         $this->tbs->VarRef['form_write'] =  $form_write;

	 $form_dump_obj = $this->formbooking->get_form();
	 $form_dump_obj = $form_dump_obj["dump"];
         $form_dump = $form_dump_obj->getHtml();
         $this->tbs->VarRef['form_dump'] =  $form_dump;
     

	 $form_gremlinquery_obj = $this->formbooking->get_form();
	 $form_gremlinquery_obj=$form_gremlinquery_obj["gremlinquery"];
         $form_gremlinquery = $form_gremlinquery_obj->getHtml();
         $this->tbs->VarRef['form_gremlinquery'] =  $form_gremlinquery;




	// a read request to the cayley
        //      first, start the cayley:  cd bk-tree-0.1/cayley/cayley_0.4.0_linux_386; ./cayley http --dbpath=testdata.nq
        //      second, start bk-tree:    cd bk-tree-0.1/bin/; ./bktree -p 8888 --prefixDB test --writeTriplets 
	//	third,  send a few write requests to fill the database
	// 	and finally, submit the form  $form_read_obj of the specific read request (see, bellow the logic of input fields of the form)


	$result_graph=array(); // to be read by TBS 
        if( $form_read_obj->isSubmittedAndValid() ) {
		$result = null;
		foreach ($form_read["inputs"] as $item) {
			 $_vals =  $form_read_obj->getValues();
			 $form_values[$item["label"]] = $_vals[$item["label"]];
		}


		// this illustrates how the Cayley PDO can be used
		if ($form_values["ID"]>0 and $form_values["Field"] === "Unknown" and $form_values["FieldValue"] === "" and $form_values["Depth"] > 0) {
			$result = $this->cayley->getDB()->getConnectedPropertiesWithBooking($form_values["ID"], $form_values["Depth"]);
		} elseif ($form_values["ID"]>0 and $form_values["Field"] !== "Unknown" and $form_values["FieldValue"] === "" and $form_values["Depth"] > 0) {
			$result = $this->cayley->getDB()->getConnectedProperty($form_values["ID"],$form_values["Depth"], $form_values["Field"]);
		} elseif ($form_values["ID"] == -1 and  $form_values["FieldValue"] !== "" and  $form_values["Depth"] > 0) {
			$result = $this->cayley->getDB()->getConnectedPropertiesFromTarget( $form_values["FieldValue"], $form_values["Depth"]);
		} elseif  ($form_values["ID"] == -2 and $form_values["FieldValue"] === "" and $form_values["Field"] === "Unknown"  and  $form_values["Depth"] > 0) {
			$result = $this->cayley->getDB()->viewAll($form_values["Depth"]);
		} elseif ($form_values["ID"] == -3 and $form_values["FieldValue"] === "" and $form_values["Field"] === "Unknown"  and  $form_values["Depth"] > 0) {
			$result = $this->cayley->getDB()->viewAllConnected( $form_values["Depth"]);
		} elseif ($form_values["ID"] == -3 and $form_values["FieldValue"] === "" and $form_values["Field"] !== "Unknown"   and  $form_values["Depth"] > 0) {
			 $result = $this->cayley->getDB()->viewAllConnectedOnlyIf( $form_values["Depth"], $form_values["Field"]);
		} elseif ($form_values["ID"] == -4 and $form_values["FieldValue"] !== "" and $form_values["Field"] === "Unknown" and  $form_values["Depth"] > 0) {
			 $result = $this->cayley->getDB()->getIDFromTo( $form_values["Depth"], $form_values["FieldValue"]);
		} elseif  ( $form_values["ID"] == -5 ) {
			 $result = $this->cayley->getDB()->getsizeID();
		} else  {
			  $result = $this->cayley->getDB()->getAllID();
		}



		if (count($result)>0)	{

			$result_graph=$result->getResult();

			 $this->tbs->VarRef['plotter'] =  $this->cayley->getPlotter()->getHTMLCode($result);

			}


	} 


	// write to the cayley
	//      first, start the cayley:  cd bk-tree-0.1/cayley/cayley_0.4.0_linux_386; ./cayley http --dbpath=testdata.nq
        //      second, start bk-tree:    cd bk-tree-0.1/bin/; ./bktree -p 8888 --prefixDB test --writeTriplets 
	// 	and finally, submit the form  $form_write_obj


	// the  $form_write_obj represents the 'entry' part of the json request  "save2DB" of the bk-tree
	if( $form_write_obj->isSubmittedAndValid() ) {

		// this block  transforms properties and its values, of the format of "BookerName" => "Booker1~0.80",  to the new format of "BookerName~0.80" => "Booker1" suggested by bk-tree
		$data=array();
		foreach ($form_write["inputs"] as $item) {
			$key = $item["label"];
			$_vals = $form_write_obj->getValues();
			$val =  $_vals[$item["label"]];
			$pattern1 = '/~.*$/';
			$pattern2 = '/.*~/';
			preg_match($pattern1,$val ,$matches);
			if (count($matches)>0) {
				$end = $matches[0];
				preg_match($pattern2,$val ,$matches);
				$val = rtrim($matches[0],"~");
				$key .= $end;
			}
			$data[$key] =   $val;
	        }


		// to write $data to cayley via bk-tree
		$result = $this->cayley->getDB()->write($data,true);


		$this->tbs->VarRef['writeresult'] =	  json_encode($result); // Cayley_Result is JSON
		$this->tbs->VarRef['writeresponse'] =	  json_encode($result->getResponse()); // Cayley response is JSON


	}



	// dump of the trees
        //      first, start the cayley:  cd bk-tree-0.1/cayley/cayley_0.4.0_linux_386; ./cayley http --dbpath=testdata.nq
        //      second, start bk-tree:    cd bk-tree-0.1/bin/; ./bktree -p 8888 --prefixDB test --writeTriplets 
        //      and finally, submit the form  $form_dump_obj

	if( $form_dump_obj->isSubmittedAndValid() ) {
		$result = $this->cayley->getDB()->dumptrees(true);
                $this->tbs->VarRef['dumpresult'] =       json_encode($result); // Cayley_Result is JSON
		// hence dump is an internal deal of the bk-tree, there is no cayley response
                $this->tbs->VarRef['dumpresponse'] =     json_encode($result->getResponse()); // Cayley response is JSON
	}



	




	// test of the Gremlin Language supoort in the CayleyPDO
        //      first, start the cayley:  cd bk-tree-0.1/cayley/cayley_0.4.0_linux_386; ./cayley http --dbpath=testdata.nq
        //      second, start bk-tree:    cd bk-tree-0.1/bin/; ./bktree -p 8888 --prefixDB test --writeTriplets 
        //      and finally, submit the form   $form_gremlinquery_obj


	if( $form_gremlinquery_obj->isSubmittedAndValid() ) {

		foreach ($form_gremlinquery["inputs"] as $item) {
                         $_vals =  $form_gremlinquery_obj->getValues();
                         $form_values[$item["label"]] = $_vals[$item["label"]];
                }

		if ($form_values["GremlinQuery"] !== "") {
			$result=$this->cayley->getDB()->readWithGremlinString($form_values["GremlinQuery"]);
			$this->tbs->VarRef['gremlinqueryresult'] =       json_encode($result); // Cayley_Result is JSON
		}

	}


/// display the view layer with substitution templates 
	 $this->tbs->LoadTemplate('cayley_view');
	 $this->tbs->MergeBlock('form_read', 'array', 'form_read[inputs]'); // to produce the read form from a template
	 $this->tbs->MergeBlock('form_write', 'array', 'form_write[inputs]'); // to produce the write form from a template
	 $this->tbs->MergeBlock('form_dump', 'array', 'form_dump[inputs]'); 
	 $this->tbs->MergeBlock('form_gremlinquery', 'array', 'form_gremlinquery[inputs]'); 
 	 $this->tbs->MergeBlock('blk', $result_graph); // to fill the template of the  table in the view layer
	 $this->tbs->Show();

   }

}




