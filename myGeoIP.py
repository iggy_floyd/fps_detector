'''
	This is a myGeoIP Class designed 
            to get Geo information from IP.
            to validate Geographic places, address etc
        
'''

from freegeoip import get_geodata # to work with freegeoip
from geopy import geocoders # to work with Google Maps, Yandex Maps, OpenMaps etc
import GeoIP  # to use MaxMind GeoIp Lite
from datetime import datetime

class myGeoIP(object):
 '''
   
 '''

 
 
 @staticmethod
 def get_geodata_ip(ip):
    ''' get geodata from ip '''

    # first try to get data from freegeoip    
    try:     
        res=freegeoip.get_geodata(ip)
        print res
        if (res['status'] ==1 ): return res
    except: pass

    # then try to get data from GeoIP Lite
    gi = GeoIP.open("GeoLiteCity.dat", GeoIP.GEOIP_INDEX_CACHE | GeoIP.GEOIP_CHECK_CACHE)
    res=gi.record_by_name(ip)
    res['type'] = 'maxmindgeoip'
    return res

if __name__ == '__main__':

 # Test 1: get geo data using IP
 print GeoIP.get_geodata_ip('8.8.8.8') # google DNS
 print GeoIP.get_geodata_ip('87.118.83.186') # Unister IPs
