'''
	This is a Dataset Class designed to read data from json files and provide a DataFrame
'''


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

from read_json import *
import pandas as pd
import numpy as np
import pickle




class Dataset(object):
 '''
	Dataset -- to process data from json files
 '''

 def __init__(self,dataset=None,dataframe=None):
	''' nothing to do '''
	self.dataset = dataset
        self.dataframe = dataframe
        self.storename = None
        self.store = None

	pass

 def loadJson(self,filename):
	''' read json data from the filename '''

	self.dataset = read_fps_data(filename)

	return self

 def listStatuses(self):
	''' get list of possible statuses in the bookings '''
	if (not self.dataset): return list()

	else: return list(set([item['status'] for item in self.dataset]))




 def filterStatus(self,status): 
	''' performs initial filtering '''

	if (not self.dataset): return []
	a = [item for item in self.dataset if item['status'] == status]
	return a

 def createDataFrame(self,dataset=None):
        ''' create a pandas DataFrame from  an array-like dataset of dictionaries obtained from json file '''
        
        if (dataset is None):
            if (self.dataset): self.dataframe = pd.DataFrame.from_dict(self.dataset)
        else: self.dataframe = pd.DataFrame.from_dict(dataset)

        
        # some transformation of the data
        if (self.dataframe is not None):
            
            # 1) join 'params' of the bookings in json as a new columns in DataFrame
            df=self.dataframe['params'].to_dict()
            df = [ value for key, value in df.iteritems() ]            
            df = pd.DataFrame.from_dict(df)
            params_opt = list(df.columns.values)
            self.dataframe = pd.merge(self.dataframe,df,left_index=True, right_index=True, how='outer')
            # 2) remove unneeded 'params'
            self.dataframe = self.dataframe.drop('params',1)
            
            # 3) repeat 1) --2) for any category found in 'params': 'booker', 'payer' etc            
            for categ in params_opt:
                df=self.dataframe[categ].to_dict()
                df = [ value for key, value in df.iteritems() ]
                df = pd.DataFrame.from_dict(df)
                column_names = list(df.columns.values)                
                column_names = [ categ +'_'+ str(i) for i in column_names]
                df.columns =  column_names
                self.dataframe = pd.merge(self.dataframe,df,left_index=True, right_index=True, how='outer')
                self.dataframe = self.dataframe.drop(categ,1)

        return self.dataframe

 def storeDataFrame(self,storename,dataframe=None):
        ''' store datataframe '''
        
        if (dataframe is None): dataframe = self.dataframe 
        if (dataframe is None): return

        if (self.store is None):
            self.store =  pd.HDFStore(storename+'.h5')
            #self.store ={storename:self.dataframe} 
        
        self.store.put('df',self.dataframe)
        #with open(storename+'.pkl', "wb") as f:
        #    s= pickle.dump(self.store,f)
        return self.store #s

 
 def storeAttribute(self,storename,attributeName, attribute):
        ''' save attribute in the store '''
        
        if (dataframe is None): dataframe = self.dataframe 
        if (dataframe is None): return

        if (self.store is None):
            #s=self.storeDataFrame(storename)
            #self.store[attributeName]=attribute
            #with open(storename+'.pkl', "wb") as f:
            #    s= pickle.dump(self.store,f)
             
            self.store =  pd.HDFStore(storename+'.h5')
            self.store.put('df',self.dataframe)
            

        
        
        setattr(self.store.get_storer('df').attrs,attributeName,attribute)
        self.store.close()
        return #s


 def loadDataFrame(self,storename):
        ''' load datataframe '''
        
        
        if (self.store is None): return 
        
        return self.store.get('df')
        


def loadAttribute(self,attributeName):
        ''' load attribute '''
        
        
        if (self.store is None): return 
        if hasttr(self.store.get_storer('df').attrs,attributeName): return getattr(self.store.get_storer('df').attrs,attributeName)
        
        return 
        

    


