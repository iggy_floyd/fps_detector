#! /usr/bin/python

from SocketServer import ThreadingMixIn
from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer

class SimpleThreadedJSONRPCServer(ThreadingMixIn, SimpleJSONRPCServer):
    pass


"""
Multithreaded JSONRPCServer example

"""

import sys

SERVER_PORT = 8090

from SocketServer import ThreadingMixIn
from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer

class SimpleThreadedJSONRPCServer(ThreadingMixIn, SimpleJSONRPCServer):
    pass


import time
import multiprocessing as mp
import copy

# some common variable among threads
storage=' '
nCalls=0
method = 2
lock=mp.Lock()

def hello(x):
#    print 'Hello', x
    global storage,nCalls,method,lock
# Method 1. No protection from asynchroneos access
    if method == 1:
	    storage=x
	    nCalls+=1
	    time.sleep(0.1)
	    if storage != x: print "storage %s was changed from %s during time.sleep"%(storage,x) 
	    return 'Hello '+ storage + ' '  + str(nCalls) + ' times!'

# Method 2. Theere is a synchronization 
    elif method == 2:
	    lock.acquire()
            storage=x
            nCalls+=1
            time.sleep(0.1)
            if storage != x: print "storage %s was changed from %s during time.sleep"%(storage,x)
	    copy_storage = copy.copy(storage)
	    lock.release()
	    return 'Hello '+ copy_storage + ' '  + str(nCalls) + ' times!'


def main():
    server = SimpleThreadedJSONRPCServer(('localhost', SERVER_PORT))
    server.register_function(hello)
    server.serve_forever()

if __name__ == '__main__':
    main()



