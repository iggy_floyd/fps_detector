'''
	This is a VariableAnalysis Class designed to store some columns from DataFrame
'''
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl
import seaborn as  sns
import Normalizer


class VariableAnalysis(object):
 '''
	VariableAnalysis 
 '''

 def __init__(self,name,dataframe,type=None):

        self.name = name
        self.dataframe = dataframe        
        if (type is not None and not self.dataframe[name].isnull().any()):  #.astype(type)
            self.dataframe[name] = pd.DataFrame(self.dataframe[name],dtype=type)
        self.type = type
        self.bins=None
        self.hist=None
	pass


 # taken from  https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
 def kde(self,x, x_grid, bandwidth=0.5, **kwargs):

    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)


 # taken from http://toyoizumilab.brain.riken.jp/hideaki/res/code/sskernel.py
 def kde_adaptive(self, x, x_grid):
    import sskernel
    ret = sskernel.sskernel(x,tin=x_grid, bootstrap=True)
    return ret['y']


 def likelihood(self,target,key,type='adaptive',nsteps=100,X_grid=None):
    ''' possible types: 'adaptive', 'small', 'big', 'standard', 'boolean', 'categoricalnum', 'categoricaltext' '''    

    # extract values for kde creation
    if (target is not None) and (key is not None):
        vals =self.dataframe.loc[self.dataframe[self.dataframe[target] == key].index,self.name].as_matrix()  
    else:
        vals =self.dataframe.loc[:,self.name].as_matrix()     

    fullvals = self.dataframe.loc[:,self.name].as_matrix()  if type in 'categoricaltext' else None

    kdes={
             'adaptive': None,
             'small': None,
             'big': None,
             'standard': None
         }


    if type in kdes:
        if X_grid is None:
            minx=min(vals)
            maxx=max(vals)
            if (minx == maxx): minx = float(minx)/nsteps
            X_grid= np.linspace(float(minx),float(maxx), nsteps)        

        
        try:
            kdes = {
                'adaptive': self.kde_adaptive(vals,X_grid),
                'small':    self.kde(vals,X_grid, bandwidth=0.05),  # a small bandwith
                'big':      self.kde(vals,X_grid, bandwidth=1.5),   #  a big bandwith,to decrease fluctuations
                'standard': self.kde(vals,X_grid, bandwidth=0.5)  # a standard  bandwith
            }               
            pdf = kdes[type] 
            import detect_peaks as dp
            import operator
            peakind=dp.detect_peaks(pdf, mph=0, edge='both' ,show=True)
            #peakind=dp.detect_peaks(pdf, mph=0, edge='both')
            return(X_grid.tolist(),pdf.tolist(),vals.tolist(),peakind.tolist())
        except: 
            print "something wrong with kde"           
            return(None,None,None,None)
    elif type in 'boolean':
        
        f1 = lambda x: x>0
        f2 = lambda x: x==0
            
        return([0,1],{0:float(len(filter(f2,vals.tolist())))/len(vals.tolist()),1:float(len(filter(f1,vals.tolist())))/len(vals.tolist())},vals.tolist(),[0,1])

    elif type in 'categoricalnum':
        
        D = {}
        for i in vals.tolist(): 
            if i not in D: D[i] = 1
            else: D[i] += 1
       
        import operator
        D=sorted([(key,float(val)/len(vals.tolist())) for key,val in D.items() ],key=operator.itemgetter(0),reverse=False)
            
        return (map(lambda x: x[0],D),map(lambda x: x[1],D),vals.tolist(),map(lambda x: x[0],D)) 

    elif type in 'categoricaltext':
        
        D = {} 
        for i in vals.tolist(): 
            if i not in D: D[i] = 1
            else: D[i] += 1

        Dfullvals ={}
        for i in fullvals.tolist(): 
            if i not in Dfullvals: Dfullvals[i] = 1
            else: Dfullvals[i] += 1



        if X_grid is None:
            import operator
            Dfullvals=sorted([(key,float(val)/len(vals.tolist())) for key,val in Dfullvals.items() ],key=operator.itemgetter(1),reverse=False)
        else:
            Dfullvals = [(key,key) for key in  X_grid]
        
  
        
        return (map(lambda x: x[0],Dfullvals),map(lambda x: float(D[x[0]])/len(vals.tolist()) if x[0] in D.keys() else 0,Dfullvals),vals.tolist(),map(lambda x: x[0],Dfullvals)) 
     

 def takeClosest(self,myList, myNumber):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    from bisect import bisect_left
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       return after
    else:
       return before  

 def likelihood_at_point(self,x,x_grid,pdf):
    x=self.takeClosest(x_grid,x)
    return pdf[x_grid.index(x)]
    
 #### Numerical integration (http://rosettacode.org/wiki/Numerical_integration)
 @staticmethod
 def simpson(f,x,h):
  return (f(x) + 4*f(x + h/2) + f(x+h))/6.0
 
 @staticmethod
 def left_rect(f,x,h):
  return f(x)
 @staticmethod
 def integrate( f, a, b, steps, meth):
   import __builtin__
   h = float(b-a)/steps
   ival = h * __builtin__.sum([meth(f, a+i*h, h) for i in range(steps)])
   return ival  

 @staticmethod
 def integrate_scipy(f, a, b):
   from scipy import integrate
   return integrate.romberg(f,a,b)


 def normalize(self,minx,maxx,x_grid,pdf):
   f = lambda x: self.likelihood_at_point(x,x_grid,pdf)
   intgrl= VariableAnalysis.integrate(f,minx,maxx,len(x_grid),VariableAnalysis.simpson)
   return map(lambda x: float(x)/intgrl,pdf) if intgrl !=0 else pdf 


 def scatter_plot(self,X,Y,bins=100):
    ''' www.bi.wisc.edu/~fox/2013/06/05/visualizing-the-correlation-of-two-volumes/'''

    from scipy.stats import spearmanr
    from matplotlib.ticker import NullFormatter

    MapX = X[0]
    MapY = Y[0]
    x = np.array(X[1])
    y = np.array(Y[1])

     # start with a rectangular Figure
    mainFig = pl.figure(1, figsize=(8,8), facecolor='white')

    # define some gridding.
    axHist2d = pl.subplot2grid( (9,9), (1,0), colspan=8, rowspan=8 )
    axHistx  = pl.subplot2grid( (9,9), (0,0), colspan=8 )
    axHisty  = pl.subplot2grid( (9,9), (1,8), rowspan=8 )

    # the 2D Histogram, which represents the 'scatter' plot:
    H, xedges, yedges = np.histogram2d( x, y, bins=(bins,bins) )
    cax=axHist2d.imshow(H.T, interpolation='nearest', aspect='auto' )
    cbar = pl.colorbar(cax, pad = 0.5)
    

    # make histograms for x and y seperately.
    axHistx.hist(x, bins=xedges, facecolor='blue', alpha=0.5, edgecolor='None' )
    axHisty.hist(y, bins=yedges, facecolor='blue', alpha=0.5, orientation='horizontal', edgecolor='None')

    # print some correlation coefficients at the top of the image.
    mainFig.text(0.05,.95,'r='+str(round(np.corrcoef( x, y )[1][0],2))+'; rho='+str(round(spearmanr( x, y )[0],2)), style='italic', fontsize=10 )

    # set axes
    axHistx.set_xlim( [xedges.min(), xedges.max()] )
    axHisty.set_ylim( [yedges.min(), yedges.max()] )
    axHist2d.set_ylim( [ axHist2d.get_ylim()[1], axHist2d.get_ylim()[0] ] )

    # remove some labels
    nullfmt   = NullFormatter()
    axHistx.xaxis.set_major_formatter(nullfmt)
    axHistx.yaxis.set_major_formatter(nullfmt)
    axHisty.xaxis.set_major_formatter(nullfmt)
    axHisty.yaxis.set_major_formatter(nullfmt)

    # remove some axes lines
    axHistx.spines['top'].set_visible(False)
    axHistx.spines['right'].set_visible(False)
    axHistx.spines['left'].set_visible(False)
    axHisty.spines['top'].set_visible(False)
    axHisty.spines['bottom'].set_visible(False)
    axHisty.spines['right'].set_visible(False)

    # remove some ticks
    axHistx.set_xticks([])
    axHistx.set_yticks([])
    axHisty.set_xticks([])
    axHisty.set_yticks([])

    # label 2d hist axes
    myTicks = np.arange(0,bins,10);
    axHist2d.set_xticks(myTicks)
    axHist2d.set_yticks(myTicks)
    axHist2d.set_xticklabels(np.round(xedges[myTicks],2))
    axHist2d.set_yticklabels(np.round(yedges[myTicks],2))

    # set titles
    axHist2d.set_xlabel(MapX, fontsize=16)
    axHist2d.set_ylabel(MapY, fontsize=16)
    axHistx.set_title(MapX, fontsize=10)
    axHisty.yaxis.set_label_position("right")
    axHisty.set_ylabel(MapY, fontsize=10, rotation=-90, verticalalignment='top', horizontalalignment='center' )

    # set the window title
    mainFig.canvas.set_window_title( (MapX + ' vs. ' + MapY) )

    
    return
    
 




 

 
 
 
 