#! /usr/bin/python


'''
    The class is part of the dataset support in the FPS detector. The dataset is created from exctracted data of  the FPS database.
    The dataset (usually called as a dataframe) is used to train classifiers.
    
'''

__author__ = "debian"
__date__ = "$Apr 23, 2015 12:25:02 PM$"




# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import numpy as np
import os.path
import sys
import pandas as pd
import json
import datetime
import time
import shutil




# User definded modules: Common Tools and Types of the FPS detector
from Dataset import Dataset
from Features import Features
from Logger import Logger, LogOut,mylogging
import DataFrameImputer
from FPSGeoIP import FPSGeoIP
#from numpy_database import GeoCodeCity
#from get_latlong import get_lat_long

#from bigcsv import ZipGeoCode
import phonenumbers
from phonenumbers import geocoder
from phonenumbers import carrier


# User definded modules: for some specific transormation
import  TimeConvert 
from  Levenshtein import StringMatcher
from  Levenshtein._levenshtein import *







class DatasetTransformer(Features,Logger):

    '''  DatasetTransformer is responsible for:
        
                * data reading from json file or from request
                * adding a new information related to booking 
                * storing transformed data in memory (for providing to some customer objects later)
                * storing the whole dataframe to csv file
    
    '''
    
    # bookings which are not considered for training
    train_statuses_refuse = [
     
        'DENIED_PAYMENTERROR',
        'DENIED_DOUBLE',       
        'DENIED_NOULA',
        'OPEN',
        'DENIED_OTHER'
    ]
    
    def __init__(self,process_name='fps_test'):
        
        super(Features, self).__init__()
        super(Logger, self).__init__()
        
        self.dataframe = None        	
        self.process_name = process_name if (type(process_name) is str) and (len(process_name)>0) else 'fps_fraud_classification2'
        self.imputer = None
        #self.features=self.getFeatures()
        self.addEventHandler('DataFrameTransform')
        self.addEventHandler('DataFrameCreate')
        self.addEventHandler('DataFrameBackup')
        self.status_ready=False
        
        
        

    
    def _createDataframe(self,file_json=''):
        ''' create a dataframe for training '''
        
        #sys.stdout = LogOut(filename="logs",category=repr(self.__class__))
        #print "Creation of the dataframe starts...."
        mylogging("Creation of the dataframe starts....")
        
        
        
        if self.dataframe is None:
            if ( (type(file_json) is str) or (type(file_json) is unicode)) and (len(file_json)>0): 
                ds = Dataset() # create dataset
                ds.loadJson(file_json)  # load data 
                self.dataframe =  ds.createDataFrame() # create dataframe
                if (self.dataframe is None): raise ValueError("DATASET_DATAFRAME_FAILED_CREATION")            
            df=self.dataframe 
            
            for status in DatasetTransformer.train_statuses_refuse:
                df =  df.drop(df[df.status == status].index) # drop not needed rows
        
        self.dataframe.to_csv(self.process_name+'.csv', index=False,encoding='utf-8')
        return self
    
    def imputtingColumns(self,selected,dataframe=None):
        
        self.selected = selected
        dataframe = self.dataframe[selected] if dataframe is None else dataframe[selected]
        if (self.imputer is None):
            self.imputer = DataFrameImputer.DataFrameImputer().fit(dataframe)
        dataframe_new = self.imputer.transform(dataframe)
            
        for x in dataframe_new: dataframe[x] = dataframe_new[x]
        return self
    
    def _transformDataframe(self):
        ''' add new features to the dataframe '''
        
        
        if self.dataframe is None:  raise ValueError('DATASET_DATAFRAME_NOT_READY')     
        
        #sys.stdout = LogOut(filename="logs",category=repr(self.__class__))
        #print "Transformation starts...."
        mylogging("Transformation starts....")
        
        
        # ini of Transformation
        execfile('Transformations.py')  # upload transformations
        
        
        # get a list of transformation functions (sorted)
        transf_func_names=sorted([ key  for key in  locals().keys() if '_add' in key ])
        
        for func in transf_func_names:
            setattr(self,func,locals()[func]) # add as an attribute
            getattr(self,func)(self) # call the function
            
        
        # fill n/A fields    
        self.imputtingColumns(self.features)     
        self.dataframe =  pd.DataFrame(self.dataframe)[self.features]
        
        # save to file        
        self.dataframe.to_csv(self.process_name+'_transformed.csv', index=False,encoding='utf-8')
        return self


    def _backup(self,msg,fileprefix,filesuffix):
        
        ''' do a backup of the dataframe '''
        
        
        if not(self.status_ready): raise ValueError('DATASETTRANSFORMER_NOT_READY') 
        if not(os.path.exists(fileprefix+filesuffix+'.csv')): raise ValueError('DATASETTRANSFORMER_NOT_READY')
            
        # get a suffix for the backup file name
        suffix =  datetime.datetime.now().strftime("_%H_%M_%d_%m_%Y")
        # logging
        self.write2Log(type='DataFrameBackup',msg=msg)
        # backup
        try:
          shutil.copy(fileprefix+filesuffix+'.csv', fileprefix + suffix + filesuffix+'.csv.bak')          
        except shutil.Error as e:
            print('Error: %s' % e)
        
        return self



    def initDataFrame(self,file_json='',update=False,doBackup=True):
        '''Create or Update DataFrame.'''
        
        #sys.stdout = LogOut(filename="logs",category=repr(self.__class__))
        #print "Init of the dataframe starts...."
        mylogging("Init of the dataframe starts....")
        
        if not(os.path.exists(self.process_name+'.csv')) or update:
            if doBackup and update: self._backup('Re-create and backup the dataframe...',self.process_name, '')
            elif not(update): self.write2Log(type='DataFrameCreate',msg='Create a new dataframe')
            
            self._createDataframe(file_json)            
            
            #print self.dataframe.head(100) # for testing purpose
        else:    
            #sys.stdout = LogOut(filename="logs",category=repr(self.__class__))
            #print "original dataframe is reading from ",self.process_name+'.csv',"...."
            mylogging("original dataframe is reading from ",self.process_name+'.csv',"....")
            
        
            
            self.dataframe = pd.read_csv(self.process_name+'.csv',na_values=[' '],keep_default_na = False)
            #print self.dataframe.head() # for testing purpose
        
        if not(os.path.exists(self.process_name+'_transformed.csv')) or update:
            if doBackup and update: self._backup('Re-create and backup the transformation in dataframe...',self.process_name, '_transformed')
            elif not(update): self.write2Log(type='DataFrameTransform',msg='Create a new transformed dataframe')
            
            self._transformDataframe()            
            #print self.dataframe.head(100) # for testing purpose
            
        else: 
            #sys.stdout = LogOut(filename="logs",category=repr(self.__class__))
            #print "a transformed dataframe is reading from ",self.process_name+'_transformed.csv',"...."
            mylogging("a transformed dataframe is reading from ",self.process_name+'_transformed.csv',"....")
            
            self.dataframe = pd.read_csv(self.process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
            
            #print self.dataframe.head() # for testing purpose
        
            
        self.status_ready=True
        #return self # not needed any more as we use proxy for background process
        
        
    def processJSON(self,json_data):
        ''' the file read json with bookings parameters and translated to the single-row dataframe'''

        #sys.stdout = LogOut(filename="logs",category=repr(self.__class__))
        #print "processJSON starts ..."
        mylogging("processJSON starts ...")
        
        # extracting data from booking parameters         
             
        if not(self.status_ready): raise ValueError('DATASETTRANSFORMER_NOT_READY')  
        if not((type(json_data) is str) or (type(json_data) is  unicode) or (type(json_data) is dict)): raise ValueError('DATASETTRANSFORMER_processJSON_WRONG_INPUT')   
        
        if ( (type(json_data) is str) or (type(json_data) is  unicode)):
            data=pd.DataFrame.from_dict([json.loads(json_data)])
        else:
            data=pd.DataFrame.from_dict([json_data])
        # 2) remove unneeded 'rulebook_id','rule_options','portal', 'rule_id'
        data = data.drop('rule_options',1) if 'rule_options' in data else data
        data = data.drop('rulebook_id',1)  if 'rulebook_id' in data else data
        data = data.drop('portal',1) if 'portal' in data else data
        data = data.drop('rule_id',1) if 'rule_id' in data else data
                
        # 1) join 'params' of the bookings in json as a new columns in DataFrame
        df=data['params'].to_dict()
        
        #print "1-df",df
        
        df = [ value for key, value in df.iteritems() ]            
        #print "2-df",df
        
        
        
        df = pd.DataFrame.from_dict(df)
        
        #print "3-df",df
        
        
        params_opt = list(df.columns.values)
        
        #print "params",params_opt
        
        data = pd.merge(data,df,left_index=True, right_index=True, how='outer')
        
        #print "new data",data
        #print data.columns.values
        
        
       # 2) remove unneeded 'params'
        data = data.drop('params',1)
       
     

        #print "2 new data",data
        #print data.columns.values
        

            
        # 3) repeat 1) --2) for any category found in 'params': 'booker', 'payer' etc            
        for categ in params_opt:
            
            #print "categ=",categ
            df=data[categ].to_dict()
            df = [ value for key, value in df.iteritems() ]
            df = pd.DataFrame.from_dict(df)
            column_names = list(df.columns.values)                
            column_names = [ categ +'_'+ str(i) for i in column_names]
            df.columns =  column_names
            data = pd.merge(data,df,left_index=True, right_index=True, how='outer')
            data = data.drop(categ,1)


        #print "3 new data",data
        #print data.columns.values
        
        #print data.to_dict()
        
        # remove unneeded arrivalAirportGeo and departureAirportGeo
        data = data.drop('product_arrivalAirportGeo',1)
        data = data.drop('product_departureAirportGeo',1)

        
        # ini of Transformation
        execfile('Transformations.py')  # upload transformations
    
        # exec Transformations
        
        # get a list of transformation functions (sorted)
        transf_func_names=sorted([ key  for key in  locals().keys() if '_add' in key ])
        
        
        for func in transf_func_names:
            setattr(self,func,locals()[func]) # add as an attribute
            getattr(self,func)(self,data) # call the function
            
        
        # fill n/A fields    
        #self.imputtingColumns(self.features,data)     
        
        #print data.to_dict()        
        data =  pd.DataFrame(data)[self.features]        
        return data
        


if __name__ == "__main__":
    
        
    filename = sys.argv[1]  if len(sys.argv)>1 else "fps_data_3.json"
    
    # Test 0: create DatasetTransformer
    manager=DatasetTransformer()
    print "Process '%s' is started"%manager.process_name

    
    # Test 1: create a dataframe
    manager.initDataFrame(filename,update=False)

    
    
    # Test 2:   JSON proceeding 
    with open(filename,'r') as f:
        for line in f:
             print line
             start=time.time()
             manager.processJSON(line)             
             print "time (in s) required:", time.time() -start
             