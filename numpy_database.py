# -*- coding: utf-8 -*-

# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import numpy as np
import time 
import codecs
import requests
import json




class database(object):
	
	def __ini__(self): return

	def read(self,filename):
		start = time.time()
		#self.data = np.char.lower(np.loadtxt(open(filename,"rb"),delimiter="\t",skiprows=0,dtype=np.str,usecols=(0,1,2,4,5), converters={4:lambda s: s if s is not None else 'nan'}))
		self.data = np.char.lower(np.genfromtxt(open(filename,"rb"),delimiter="\t",skiprows=0,dtype=np.str,usecols=(0,1,2,4,5), filling_values='NaN'))
		print "%d eintries has been loaded for %.3f sec."%(self.data.shape[0], time.time()-start)
		return self

	def search(self,txt,field):
		a= np.where(txt == self.data)
		b= filter(lambda x: x[1]==field,  zip(*a))
		return np.array([ self.data[c[0]].tolist()  for c in b])


class GeoCodeCity(object):


	filename = 'cities1000.txt'

	start = time.time()	
	data = np.char.lower(np.genfromtxt(open(filename,"rb"),delimiter="\t",skiprows=0,dtype=np.str,usecols=(0,1,2,4,5,8), filling_values='NaN'))
#	data = np.char.lower(np.genfromtxt(open(filename,"rb"),delimiter="\t",skiprows=0,dtype=np.str,usecols=(0,1,3,4,5), filling_values='NaN'))

	#data = np.genfromtxt(open(filename,"rb"),delimiter="\t",skiprows=0,dtype='S8',usecols=(0,1,2,4,5), filling_values='NaN')
	#data=data.view(np.chararray).decode('utf-8')
	#data=np.char.lower(data)
	#s = codecs.open(filename, encoding="utf-8").read()
	#data =  np.char.lower(np.frombuffer(s, dtype="<U3"))
	

        print "%d eintries has been loaded for %.3f sec."%(data.shape[0], time.time()-start)

	@staticmethod
	def searchGeoCodes(city,field):
		city = city.lower()
		#print unicode(city).decode()
		a= np.where(city == GeoCodeCity.data)
#		a= np.where(np.core.defchararray.find(GeoCodeCity.data,city)>=0)
		print a
                b= filter(lambda x: x[1]==field,  zip(*a))
		res=np.array([ GeoCodeCity.data[c[0]].tolist()  for c in b])
		print res
		if (len(res)==0): return (None,None)
		lat = float(res[0][3]) if  res[0][3] !='NaN' else None
		long = float(res[0][4]) if  res[0][4] !='NaN' else None
                
	@staticmethod
	def searchGeoCodes_v2(city,country):
		city = city.lower()
                country = country.lower()       
                #print country
		#print unicode(city).decode()
                #a= np.where(np.core.defchararray.find(GeoCodeCity.data,city)>=0)
		a= np.where(city == GeoCodeCity.data )                
                #print "a=",a
                #b= np.where(country == GeoCodeCity.data)                                                 
                res=GeoCodeCity.data[ [aa for aa in a[0]] ]        
                #print "res=",res
                res=filter(lambda x: x[5] == country, res)

                
                
                #c = np.intersect1d(a[0],b[0])                		
                #res=GeoCodeCity.data[c]
                
                
                
                #print "res=",res
                
                
		if (len(res)==0): return (None,None)
		lat = float(res[0][3]) if  res[0][3] !='NaN' else None
		long = float(res[0][4]) if  res[0][4] !='NaN' else None
                
                #print lat,long
		return (lat,long)
	


	


if __name__ == "__main__":

	
#	db = database().read('allCountries.txt')
#	sys.exit()
#	db = database().read('cities1000.txt')

#	print "eichwalde at pos 1", db.search('eichwalde',1)
#	print "eichwalde at pos 2", db.search('eichwalde',2)

#	print "eichwalde at pos 1", GeoCodeCity.searchGeoCodes('Eichwalde',1)
#	print "eichwalde at pos 2", GeoCodeCity.searchGeoCodes('Eichwalde',2)
	
#	print "Schönau at pos 1",GeoCodeCity.searchGeoCodes(u'Schönau',1)
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'Schönau',2)

#'\u00f6'

        print "Jo"
#	print  GeoCodeCity.data
#	print GeoCodeCity.data[31610]
#	print "Schönau at pos 1",GeoCodeCity.searchGeoCodes('Sch\u00f6nau',1)
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes('Schönau',1)
	#print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'Sch\u00f6nau'.encode('utf-8'),1)
        #print "Schönau at pos 2",GeoCodeCity.searchGeoCodes_v2(u'Sch\u00f6nau'.encode('utf-8'),1,'AT')
        #print "Schönau at pos 2",GeoCodeCity.searchGeoCodes_v2(u'Sch\u00f6nau'.encode('utf-8'),'DE')
        #print "Dallas at pos 2",GeoCodeCity.searchGeoCodes_v2(u'Dallas'.encode('utf-8'),'US')
        
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'G\u00f6tzis'.encode('utf-8'),1)
#	print "Schönau at pos 2",GeoCodeCity.searchGeoCodes(u'M\u00fcnchen'.encode('utf-8'),2)
#	print "Schönau at pos 2",GeoCodeCity_v2.searchGeoCodes(u'M\u00fcnchen'.encode('utf-8'),2)


        
        
        