# -*- coding: utf-8 -*-
'''
	    This is a FPSGeoIP Class designed 
            to get Geo information from IP.
            to validate Geographic places, address etc
        
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

from freegeoip import get_geodata # to work with freegeoip
from geopy import geocoders # to work with Google Maps, Yandex Maps, OpenMaps etc
#import GeoIP  # to use MaxMind GeoIp Lite
import myGeoIP  # to use MaxMind GeoIp Lite

from geopy.distance import great_circle

import pycountry
import json
import sys
import traceback
import csv
import itertools
from numpy_database import GeoCodeCity
from get_latlong import get_lat_long
#import phonenumbers
#from phonenumbers import geocoder
#from bigcsv import ZipGeoCode

class FPSGeoIP(object):
 '''
   
 '''

 # a list of free geocoders (no api keys, no licence keys are needed)
 # the oder of list is priority of the geolocators
 osm_geocoder = geocoders.Nominatim()
 yandex_geocoder = geocoders.Yandex(lang='en_US')
 openmap_geocoder = geocoders.OpenMapQuest()
 
 table = {
                0xe4: u'ae', # a diaeresis
                ord(u'ö'): u'oe',
                ord(u'ü'): u'ue',
                ord(u'ß'): u'ss',
                0xe1: u'a',  	# a acute 'á' => 'a'
                0xe0: u'a',     # a grave 'à' => 'a'
                0xe2: u'a',     # a circumflex  'â' => 'a'
                0xe3: u'a',     # a tilde 'ã' => 'a'
                0xe7: u'c',     # c cedilla 'ç' => 'c'
                0xe9: u'e',     # e acute 'é' => 'e'
                0xe8: u'e',     # e grave 'è' => 'e'
                0xea: u'e',     # e circumflex 'ê' => 'e'
                0xed: u'i',     # i acute 'í' => 'i'
                0xec: u'i',     # i grave 'ì' => 'i'
                0xee: u'i',     # i circumflex 'î' => 'i'
                0xef: u'i',     # i diaeresis 'ï' => 'i'
                0xf1: u'n',     # n tilde 'ñ' => 'n'
                0xf2: u'o',     # o grave 'ò' => 'o'
                0xf3: u'o',     # o acute 'ó' => 'o'
                0xf5: u'o',     # o tilde   'õ' => 'o'
                0xf4: u'o',     # o circumflex 'ô' => 'o'
                0xf6: u'o',     # o diaeresis 'ï' => 'o'
                0xf9: u'u',     # u grave 'ù' => 'u'
                0xfa: u'u',     # u acute 'ú' => 'u'
                0xfb: u'u',     # u circumflex 'û' => 'u'
                0xfd: u'y',      # y acute 'ý' => 'y',
                0xdf: u'ss'      
                
            }
  
 country_codes= dict((country.alpha2.lower(),country.name.lower()) for country in list(pycountry.countries))
 airports_filecsv='airports.csv'
    
 with open(airports_filecsv,'r') as file:
    reader=csv.reader(file)
    airports_lat_long=dict((row[0].lower(),(float(row[3]),float(row[4]))) for row in reader)
 
 proxy_list_files = ['proxy.list' ,'proxy.list.2','proxy.list.3' ]  
 proxies = list(set(itertools.chain(*[ map(lambda x: x.replace('\n','').split(':')[0],open(f).readlines())   for f in proxy_list_files ])))
  
    

 @staticmethod
 def get_geodata_ip(ip,dofreegeoip=True):
    ''' get geodata from ip '''

    # first try to get data from freegeoip    
    if (dofreegeoip):
        try:     
            res=get_geodata(ip)        
            if (res['status'] ==1 ): 
                res = dict((key,unicode(value).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()) for key,value in res.items())
                return res

        except: pass

    # then try to get data from GeoIP Lite
    gi = GeoIP.open("GeoLiteCity.dat", GeoIP.GEOIP_INDEX_CACHE | GeoIP.GEOIP_CHECK_CACHE)
    res=gi.record_by_name(ip)
    res['geotype'] = 'maxmindgeoip'
    #for key,value in res.items():
    #    print unicode(value).translate(FPSGeoIP.table).encode('ascii', 'ignore')
    res = dict((key,unicode(value).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()) for key,value in res.items())
    return res

 @staticmethod
 def get_geodata_lat(latitude,longitude):
   ''' get geodata from latitude,longitude '''
  
   try:  # OSM geocoder
     
     location = FPSGeoIP.osm_geocoder.reverse(str(latitude)+', '+str(longitude),language='en_US')
     if type(location) == type([]): res = map(lambda x: x.address,location )
     else : res = [location.address]
     res =filter(lambda x: x, res )     
     map_res={}
     for r in res:        
        r2=r.split(',')        
        for r3 in r2:         
            map_res[r3]=1     
     if (len( map_res)>0): 
        #for key,value in map_res.items():
        #   print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
        #print "2>>>>", map_res
        return  map_res
  
   except Exception, err: 
     print traceback.format_exc()

   try:  # Yandex Map geocoder
     
     location = FPSGeoIP.yandex_geocoder.reverse(str(latitude)+', '+str(longitude))     
     if type(location) == type([]): res = map(lambda x: x.address,location )
     else : res = [location.address]
     res =filter(lambda x: x, res )     
     map_res={}
     for r in res:        
        r2=r.split(',')        
        for r3 in r2:         
            map_res[r3]=1     
     if (len( map_res)>0): 
        #for key,value in map_res.items():
        #   print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')

        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
        #print "2>>>", map_res
        return  map_res
  
   except Exception, err: 
     print traceback.format_exc()


   try:  # openmap  geocoder
     location = FPSGeoIP.openmap_geocoder.reverse(str(latitude)+', '+str(longitude))
     if type(location) == type([]): res = map(lambda x: x.address,location )
     else : res = [location.address]
     res =filter(lambda x: x, res )     
     map_res={}
     for r in res:        
        r2=r.split(',')        
        for r3 in r2:         
            map_res[r3]=1     
     if (len( map_res)>0): 
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
        return  map_res

   except Exception, err: 
     print traceback.format_exc()

   return  



 @staticmethod
 def get_geocode(street='',house='',zipcode='',city='',country=''):
   ''' get geodata from geocode '''
   
   # normalization of data
   if (not(street is None) and (len(street)>0)): street=unicode(street).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(house is None) and (len(house)>0)): house=unicode(house).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(zipcode is None) and (len(zipcode)>0)): zipcode=unicode(zipcode).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(city is None) and (len(city)>0)): city=unicode(city).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(country is None) and (len(country)>0)): country=unicode(country).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()

   #print 'request:',  "%s %s %s %s %s "%(street,house,zipcode,city,country)

   ## Let's try to use Geocoder 
   map_res,_ = FPSGeoIP.get_geocode_with_geocoder(street='',house='',zipcode='',city='',country='',search_cmd="%s %s %s %s %s "%(street,house,zipcode,city,country))
   if (len(map_res)>0): 
    #print 'Geocoder: ', map_res
    return map_res
     
   ## If nothing was found so far, use another approach: GeoPy

   try:  # OSM geocoder     
     location = FPSGeoIP.osm_geocoder.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country),language='en_US', exactly_one=False)
     #print "1 OSM location:", type(location)
     if type(location) == type([]): res = map(lambda x: x.address,location )
     else : res = [location.address]
     res =filter(lambda x: x, res )     
     map_res={}
     
     for r in res:        
        r2=r.split(',')        
        for r3 in r2:         
            map_res[r3]=1     
     if (len( map_res)>0): 
        
        #for key,value in map_res.items():
        #   print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
        map_res['provider']="osm"
      #  print "1 OSM res:", map_res
        return  map_res
   except Exception, err: 
     print traceback.format_exc()

   try:  # OpenMapQuest geocoder    
     #print 'openmap request:',  "%s %s %s %s %s "%(street,house,zipcode,city,country)
     location = FPSGeoIP.openmap_geocoder.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country), exactly_one=False)
     #print "openmap geolocator location:",type(location)
     if type(location) == type([]): res = map(lambda x: x.address,location )
     else : res = [location.address]
     res =filter(lambda x: x, res )     
     
     map_res={}
     for r in res:        
        r2=r.split(',')        
        for r3 in r2:         
            map_res[r3]=1     
     if (len( map_res)>0): 
        
   #     for key,value in map_res.items():
   #        print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
      #  print "2 openmap res:", map_res
        map_res['provider']="openmap"
        return  map_res
   except Exception, err: 
     print traceback.format_exc()

   

   try:  # Yandex  geocoder    
     #print 'yandex request:',  "%s %s %s %s %s "%(street,house,zipcode,city,country)
     location = FPSGeoIP.yandex_geocoder.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country),exactly_one=False )
     #print "yandex geolocator location:",type(location)
     if type(location) == type([]): res = map(lambda x: x.address,location )
     else : res = [location.address]
     res =filter(lambda x: x, res )     
     
     map_res={}
     for r in res:        
        r2=r.split(',')        
        for r3 in r2:         
            map_res[r3]=1     
     if (len( map_res)>0): 
        
   #     for key,value in map_res.items():
   #        print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
      #  print "2 yandex res:", map_res
        map_res['provider']="yandex"
        return  map_res
   except Exception, err: 
     print traceback.format_exc()

  
   
   return 

 @staticmethod
 def get_lat_long(street='',house='',zipcode='',city='',country=''):
   ''' get geodata from geocode '''
   
   # normalization of data
   if (not(street is None) and (len(street)>0)): street=unicode(street).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(house is None) and (len(house)>0)): house=unicode(house).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(zipcode is None) and (len(zipcode)>0)): zipcode=unicode(zipcode).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(city is None) and (len(city)>0)): city=unicode(city).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   if (not(country is None) and (len(country)>0)): country=unicode(country).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
   
   try:  # OSM geocoder     
     location = FPSGeoIP.osm_geocoder.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country),language='en_US', exactly_one=True)
     #print "1 OSM location:", type(location)
     if type(location) == type([]): res = map(lambda x: str(x.latitude)+","+str(x.longitude),location )
     else : res = [str(location.latitude)+","+str(location.longitude)]
     res =filter(lambda x: x, res )     
     map_res={}
     
     for r in res:        
        r2=r.split(',')        
        #for r3 in r2:         
        #    map_res[r3]=1     
        if len(r2)>1: 
            map_res['lat'] = r2[0]
            map_res['long'] = r2[1]
     if (len( map_res)>0): 
        
        #for key,value in map_res.items():
        #   print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
        map_res['provider']="osm"
#        print "1 OSM res:", map_res
        return  map_res
   except Exception, err: 
     print traceback.format_exc()

   try:  # OpenMapQuest geocoder    
     #print 'openmap request:',  "%s %s %s %s %s "%(street,house,zipcode,city,country)
     location = FPSGeoIP.openmap_geocoder.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country), exactly_one=True)
     #print "openmap geolocator location:",type(location)
     if type(location) == type([]): res = map(lambda x: str(x.latitude)+","+str(x.longitude),location )
     else : res = [str(location.latitude)+","+str(location.longitude)]
     res =filter(lambda x: x, res )     
     
     map_res={}
     for r in res:        
        r2=r.split(',')        
        #for r3 in r2:         
        #    map_res[r3]=1     
        if len(r2)>1: 
            map_res['lat'] = r2[0]
            map_res['long'] = r2[1]
     if (len( map_res)>0): 
        
   #     for key,value in map_res.items():
   #        print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
#        print "2 openmap res:", map_res
        map_res['provider']="openmap"
        return  map_res
   except Exception, err: 
     print traceback.format_exc()

   try:  # Yandex  geocoder    
     #print 'yandex request:',  "%s %s %s %s %s "%(street,house,zipcode,city,country)
     location = FPSGeoIP.yandex_geocoder.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country),exactly_one=True )
     #print "yandex geolocator location:",type(location)
     if type(location) == type([]): res = map(lambda x:  str(x.latitude)+","+str(x.longitude),location )
     else : res = [str(location.latitude)+","+str(location.longitude)]
     res =filter(lambda x: x, res )     
     
     map_res={}
     for r in res:        
        r2=r.split(',')        
        #for r3 in r2:         
        #    map_res[r3]=1     
        if len(r2)>1: 
            map_res['lat'] = r2[0]
            map_res['long'] = r2[1]
     if (len( map_res)>0): 
        
   #     for key,value in map_res.items():
   #        print"1>>>",unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore')
        map_res = dict((unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower(),value) for key,value in map_res.items())
#        print "2 yandex res:", map_res
        map_res['provider']="yandex"
        return  map_res
   except Exception, err: 
     print traceback.format_exc()

   
   return 


 @staticmethod
 def validate_zipcode(zipcode,country_code):

    try:
        import geonameszip
        return geonameszip.lookup_postal_code(zipcode, country_code.upper())
        
    except Exception, err: 
     print traceback.format_exc()

    return None

 @staticmethod
 def validate_proxy(ip='some_proxy_ip'):

    return ip in FPSGeoIP.proxies


 @staticmethod
 def get_geocode_with_geocoder(street='',house='',zipcode='',city='',country='',search_cmd=''):

    # assume that all inputdata  translated from utf-8 to ascii
    list_of_geolocators = [
        ('google',{}),
        ('bing',{}),
        ('osm',{}),
        ('tomtom',{}),
        ('yahoo',{}),
        ('yandex',{'lang':'en-US'})
    ]

    attrs = [
                 'street',
                 'housenumber',
                 'postal',
                 'city_long',
                 'country_long',
                 'city',
                 'country'
            ]

    try:
        import geocoder
        addr = street+' '+house+' '+zipcode+' '+city+' '+country
        if (len(addr)==4): addr = search_cmd
        if (len(addr)==0): return ({},{})
        for geolocator in list_of_geolocators:
            location = eval('geocoder.'+geolocator[0])(addr,**geolocator[1])
            if location.ok: break


        if (not location.ok): return ({},{})
        map_res_adr={}
        map_res_geo={}
        for attr in attrs:
            try:
                key = getattr(location,attr)
            except:
                key = None
            if key is None: continue
            #print attr,key
            map_res_adr[unicode(key).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()] = 1
            
        
        map_res_adr['provider']=location.provider
        map_res_geo['lat'] = location.lat
        map_res_geo['long'] = location.lng
        return (map_res_adr,map_res_geo)
    except Exception, err: 
     print traceback.format_exc()
     return ({},{})

 @staticmethod
 def validate_ip(ip='',street='',house='',zipcode='',city='',country='',country_code=''):
    ''' validate ip and address '''
    
    # normalization of data
    if (not(street is None) and (len(street)>0)): street=unicode(street).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
    if (not(house is None) and (len(house)>0)): house=unicode(house).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
    if (not(zipcode is None) and (len(zipcode)>0)): zipcode=unicode(zipcode).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
    if (not(city is None) and (len(city)>0)): city=unicode(city).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
    if (not(country is None) and (len(country)>0)): country=unicode(country).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()

    ipdata={}
    if (len(ip)>0 and not(ip is None) ): 
        _ipdata = FPSGeoIP.get_geodata_ip(ip)
        del _ipdata['time_zone'] ## not needed, but it can confuse the result
#        print
#        print "ip data: ",_ipdata 
        
        if (_ipdata['status']>0):
            _dataprovider = _ipdata['geotype']
            _lat = _ipdata['latitude']
            _long = _ipdata['longitude']
            _zipcode = 'zip_code' if _dataprovider == 'freegeoip' else 'postal_code'
            _zipcode = _ipdata[_zipcode]
            _city=_ipdata['city']
            
            _ipdata = [key for key in _ipdata.values()]            
            
            # no city or zipcode from FreeIP: take it from geolocators
            if (len(_city)*len(_zipcode) == 0 and len(_lat)*len(_long)>0): 
                _ipdata2 = FPSGeoIP.get_geodata_lat(_lat,_long)
                _ipdata += [key for key in _ipdata2.keys()]
 #               print "new ip data....", _ipdata

            street = street.replace('.','')            
            #print "street=",filter(lambda z: len(z)>1,street.split())            
            #print [ filter(lambda y: x in y,_ipdata) for x in filter(lambda z: len(z)>1 and z not in ['street'],street.split())  ]
            street = max([ len(filter(lambda y: x in y,_ipdata)) for x in filter(lambda z: len(z)>1 and  z not in ['street'],street.split()) ]) if (not(street is None) and (len(street)>0)) else 0            
            zipcode = max([ len(filter(lambda y: x in y,_ipdata)) for x in filter(lambda z: len(z)>1,zipcode.split())  ]) if (not(zipcode is None) and (len(zipcode)>0)) else 0
            city = max([ len(filter(lambda y: x in y,_ipdata)) for x in filter(lambda z: len(z)>1,city.split()) ]) if (not(city is None) and (len(city)>0)) else 0
            country = max([ len(filter(lambda y: x in y,_ipdata)) for x in filter(lambda z: len(z)>1,country.split())  ])  if (not(country is None) and (len(country)>0)) else 0
 
            return  [ 
                    ('city',1 if city>0 else 0),
                    ('country',1 if country>0 else 0),
                    ('zipcode',1 if zipcode>0 else 0),
                    ('street',1 if street>0 else 0),
                    ('provider',_dataprovider ),
                    ('FoundNone',0)
                  ]
            
            '''
            if (_ipdata['geotype'] == 'freegeoip'):
                ipdata['city'] = _ipdata['city']
                ipdata['country'] = _ipdata['country_name']
                ipdata['zipcode'] = _ipdata['zip_code']
                ipdata['country_code'] = _ipdata['country_code']
            else:   # 'type' -> 'maxmindgeoip'
                ipdata['city'] = _ipdata['city']
                ipdata['country'] = _ipdata['country_name']
                ipdata['zipcode'] = _ipdata['postal_code']
                ipdata['country_code'] = _ipdata['country_code']
            '''

    '''
    return [    
                ('city',city == ipdata['city']),
                ('country',country == ipdata['country']),
                ('country_code',country_code == ipdata['country_code']),
                ('zipcode',zipcode == ipdata['zipcode'])
           ]
    '''

    return  [ 
                    ('city',0),
                    ('country',0),
                    ('zipcode',0),                     
                    ('street',0),
                    ('provider',''),
                    ('FoundNone',1)                
            ] 
   
 @staticmethod
 def validate_address(street='',house='',zipcode='',city='',country='',geocoder=False,do_print=False):
        ''' validate ip and address '''
        
        # normalization of data
        if (not(street is None) and (len(street)>0)): street=unicode(street).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
        if (not(house is None) and (len(house)>0)): house=unicode(house).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
        if (not(zipcode is None) and (len(zipcode)>0)): zipcode=unicode(zipcode).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
        if (not(city is None) and (len(city)>0)): city=unicode(city).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
        if (not(country is None) and (len(country)>0)): country=unicode(country).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()

        if (geocoder):
            _data,_ = FPSGeoIP.get_geocode_with_geocoder(street=street,house=house,zipcode=zipcode,city=city,country=country)
        else:
            _data = FPSGeoIP.get_geocode(street=street,house=house,zipcode=zipcode,city=city,country=country)
        print
        if (do_print): print "found place: ",_data
        if not(_data is None) and ( len(_data)>0):
            _dataprovider = _data['provider']
            _data = [key for key in _data.keys()]
            street = street.replace('.','')        
            street = max([ len(filter(lambda y: x in y,_data)) for x in street.split()  ]) if (not(street is None) and (len(street)>0)) else 0
            zipcode = max([ len(filter(lambda y: x in y,_data)) for x in zipcode.split()  ]) if (not(zipcode is None) and (len(zipcode)>0)) else 0
            city = max([ len(filter(lambda y: x in y,_data)) for x in city.split()  ]) if (not(city is None) and (len(city)>0)) else 0
            country = max([ len(filter(lambda y: x in y,_data)) for x in country.split()  ])  if (not(country is None) and (len(country)>0)) else 0
            
            
            return  [ 
                    ('city',1 if city>0 else 0),
                    ('country',1 if country>0 else 0),
                    ('zipcode',1 if zipcode>0 else 0),
                    ('street',1 if street>0 else 0),
                    ('provider',_dataprovider ),
                    ('FoundNone',0)
                  ]
      
        return  [ 
                    ('city',0),
                    ('country',0),
                    ('zipcode',0),                     
                    ('street',0),
                    ('provider',''),
                    ('FoundNone',1)                ] 



 @staticmethod
 def calculate_distance(lat,long,airport):
    ''' calculates the distance in km between the place (lat,long) and 'airport' '''
    
    lat = float(lat) if not(lat is None) else None
    long = float(long) if not(long is None) else None
    airport = airport.lower()   if not(airport is None) else ''
    start_point = (lat,long) if not(lat is None) and not(long is None)  else None
    end_point   = FPSGeoIP.airports_lat_long[airport] if airport in FPSGeoIP.airports_lat_long else None

    return great_circle(start_point, end_point).kilometers if not(start_point is None) and not(end_point is None) else -1

 @staticmethod
 def calculate_distance_airports(airport1,airport2):
    ''' calculates the distance in km between two airports '''
    
    airport1 = airport1.lower()   if not(airport1 is None) else ''
    airport2 = airport2.lower()   if not(airport2 is None) else ''
    
    start_point   = FPSGeoIP.airports_lat_long[airport1] if airport1 in FPSGeoIP.airports_lat_long else None
    end_point   = FPSGeoIP.airports_lat_long[airport2] if airport2 in FPSGeoIP.airports_lat_long else None

    return great_circle(start_point, end_point).kilometers if not(start_point is None) and not(end_point is None) else -1






 @staticmethod
 def get_geocode_with_geolocator(locator,street='',house='',zipcode='',city='',country=''):
   ''' get geodata from geocode '''
   try:  
     
     if (type(locator)) == type(FPSGeoIP.osm_geocoder):
        location =locator.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country),language='en_US', exactly_one=False) # OSM geocoder     
     else:
        location =locator.geocode(" %s %s %s %s %s "%(street,house,zipcode,city,country), exactly_one=False)
     
     if type(location) == type([]): 
        res_adr = map(lambda x: x.address,location )
        res_geo = map(lambda x: str(x.latitude)+","+str(x.longitude),location )
     else : 
        res_adr = [location.address]
        res_geo = [str(location.latitude)+","+str(location.longitude)]
     res_adr =filter(lambda x: x, res_adr )     
     res_geo =filter(lambda x: x, res_geo )     
     map_res_adr={}
     map_res_geo={}

     for r in res_adr:        
        r2=r.split(',')        
        for r3 in r2:         
            map_res_adr[unicode(r3).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()]=1   
        
     for r in res_geo:        
        r2=r.split(',')        
        if len(r2)>1: 
            map_res_geo['lat'] = r2[0]
            map_res_geo['long'] = r2[1]

     
     return (map_res_adr,map_res_geo)
   except Exception, err: 
     #print traceback.format_exc()
     pass   

   return ({},{})




 
 
 
 @staticmethod
 def airportdistance(airport1='',airport2=''):
    airport1 = airport1.lower()   if not(airport1 is None) else ''
    airport2 = airport2.lower()   if not(airport2 is None) else ''
    
    start_point   = FPSGeoIP.airports_lat_long[airport1] if airport1 in FPSGeoIP.airports_lat_long else None
    end_point  = FPSGeoIP.airports_lat_long[airport2] if airport2 in FPSGeoIP.airports_lat_long else None
    
    return great_circle(start_point, end_point).kilometers if not(start_point is None) and not(end_point is None) else -1


 @staticmethod
 def booker_airport_distance(street='',house='',zipcode='',city='',country='',airport1='',airport2=''):
    airport1 = airport1.lower()   if not(airport1 is None) else ''
    airport2 = airport2.lower()   if not(airport2 is None) else ''
    
    start_point   = FPSGeoIP.airports_lat_long[airport1] if airport1 in FPSGeoIP.airports_lat_long else None
    end_point  = FPSGeoIP.airports_lat_long[airport2] if airport2 in FPSGeoIP.airports_lat_long else None
    
    map_res_adr,map_res_geo = FPSGeoIP.get_geocode_with_geocoder(street=street,house=house,zipcode=zipcode,city=city,country=country)
    booker_point  = (map_res_geo['lat'],map_res_geo['long']) if len(map_res_geo)>0 else None
    
    #calculate distance between booker and airports
    dist_booker_airport1 = great_circle(booker_point, start_point).kilometers if not(booker_point is None) and not(start_point is None) else -1
    dist_booker_airport2 = great_circle(booker_point, end_point).kilometers if not(booker_point is None) and not(end_point is None) else -1
    
    return dist_booker_airport1

 @staticmethod
 def booker_airport_distance_v2(city='',country='',airport1='',airport2=''):
    airport1 = airport1.lower()   if not(airport1 is None) else ''
    airport2 = airport2.lower()   if not(airport2 is None) else ''
    
    start_point   = FPSGeoIP.airports_lat_long[airport1] if airport1 in FPSGeoIP.airports_lat_long else None
    end_point  = FPSGeoIP.airports_lat_long[airport2] if airport2 in FPSGeoIP.airports_lat_long else None
    
    #map_res_adr,map_res_geo = FPSGeoIP.get_geocode_with_geocoder(street=street,house=house,zipcode=zipcode,city=city,country=country)
    #booker_point  = (map_res_geo['lat'],map_res_geo['long']) if len(map_res_geo)>0 else None
    booker_point  = GeoCodeCity.searchGeoCodes(city.encode('utf-8'),1)
    if (booker_point[0] is None or booker_point[1] is None): booker_point=None
    
    #calculate distance between booker and airports
    dist_booker_airport1 = great_circle(booker_point, start_point).kilometers if not(booker_point is None) and not(start_point is None) else -1
    dist_booker_airport2 = great_circle(booker_point, end_point).kilometers if not(booker_point is None) and not(end_point is None) else -1
    
    return dist_booker_airport1

 @staticmethod
 def booker_airport_distance_v3(city='',country='',airport1='',airport2=''):
    airport1 = airport1.lower()   if not(airport1 is None) else ''
    airport2 = airport2.lower()   if not(airport2 is None) else ''
    
    start_point   = FPSGeoIP.airports_lat_long[airport1] if airport1 in FPSGeoIP.airports_lat_long else None
    end_point  = FPSGeoIP.airports_lat_long[airport2] if airport2 in FPSGeoIP.airports_lat_long else None
    
    
    
    airport_dist=great_circle(start_point, end_point).kilometers if not(start_point is None) and not(end_point is None) else -1

    
    # using geonames database
    # attempt Nr 1
    #map_res_adr,map_res_geo = FPSGeoIP.get_geocode_with_geocoder(street=street,house=house,zipcode=zipcode,city=city,country=country)
    #booker_point  = (map_res_geo['lat'],map_res_geo['long']) if len(map_res_geo)>0 else None
    booker_point  = GeoCodeCity.searchGeoCodes_v2(city.encode('utf-8'),country)
    
    #print "(1) booker_point=",booker_point
    # using geonames database
    # attempt Nr 2
    #if ( (booker_point[0] is None) or (booker_point[1] is None)): 
    #    print 'allCountries.txt'
    #    booker_point=ZipGeoCode.searchGeoCodesCity(city,country)
    #    print "(2) booker_point=",booker_point
    
    
    # using Rest API: Google, Yandex, Geonames
    if ((booker_point[0] is None) or (booker_point[1] is None)): 
        #print 'use get_lat_long'
        #booker_point=get_lat_long(urlend=city+','+country)
        booker_point=get_lat_long(city+','+country)
        #print "(3) booker_point=",booker_point
    
    #calculate distance between booker and airports
    dist_booker_airport1 = great_circle(booker_point, start_point).kilometers if not(booker_point is None) and not(start_point is None) else -1
    dist_booker_airport2 = great_circle(booker_point, end_point).kilometers if not(booker_point is None) and not(end_point is None) else -1
    
    return airport_dist,dist_booker_airport1,dist_booker_airport2



 @staticmethod
 def calculate_and_validate(ip='',street='',house='',zipcode='',city='',country='',airport1='',airport2='',geocoder=False):
    
    airport1 = airport1.lower()   if not(airport1 is None) else ''
    airport2 = airport2.lower()   if not(airport2 is None) else ''
    
    start_point   = FPSGeoIP.airports_lat_long[airport1] if airport1 in FPSGeoIP.airports_lat_long else None
    end_point  = FPSGeoIP.airports_lat_long[airport2] if airport2 in FPSGeoIP.airports_lat_long else None
    
    
    # calculate distance between airports
    dist_airport1_airport2 = great_circle(start_point, end_point).kilometers if not(start_point is None) and not(end_point is None) else -1
    
    res_ip = (
                    ('city',0),
                    ('country',0),
                    ('zipcode',0),                     
                    ('street',0)
             )       

    res_addr = (
                    ('city',0),
                    ('country',0),
                    ('zipcode',0),                     
                    ('street',0)
             ) 
    dist_booker_airport1 = -1
    dist_booker_airport2 = -1

    try:
     # normalization of data
     if (not(street is None) and (len(street)>0)): street1=unicode(street).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(house is None) and (len(house)>0)): house1=unicode(house).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(zipcode is None) and (len(zipcode)>0)): zipcode1=unicode(zipcode).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(city is None) and (len(city)>0)): city1=unicode(city).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(country is None) and (len(country)>0)): country1=unicode(country).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()

     if (not(street is None) and (len(street)>0)): street2=unicode(street).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(house is None) and (len(house)>0)): house2=unicode(house).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(zipcode is None) and (len(zipcode)>0)): zipcode2=unicode(zipcode).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(city is None) and (len(city)>0)): city2=unicode(city).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()
     if (not(country is None) and (len(country)>0)): country2=unicode(country).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower()

     # validation of ip
    
     if (len(ip)>0 and not(ip is None) ): 
        _ipdata = FPSGeoIP.get_geodata_ip(ip)
        if (_ipdata['status']>0):

            _ipdata = [key for key in _ipdata.values()] 
            street1 = street1.replace('.','')            
            street1 = max([ len(filter(lambda y: x in y,_ipdata)) for x in street1.split()  ]) if (not(street1 is None) and (len(street1)>0)) else 0
            zipcode1 = max([ len(filter(lambda y: x in y,_ipdata)) for x in zipcode1.split()  ]) if (not(zipcode1 is None) and (len(zipcode1)>0)) else 0
            city1 = max([ len(filter(lambda y: x in y,_ipdata)) for x in city1.split()  ]) if (not(city1 is None) and (len(city1)>0)) else 0
            country1 = max([ len(filter(lambda y: x in y,_ipdata)) for x in country1.split()  ])  if (not(country1 is None) and (len(country1)>0)) else 0
 
            res_ip = ( 
                        ('city',1 if city1>0 else 0),
                        ('country',1 if country1>0 else 0),
                        ('zipcode',1 if zipcode1>0 else 0),
                        ('street',1 if street1>0 else 0)
                    )

     # validation of address
 
     map_res_adr,map_res_geo = {},{}
     if (geocoder):
        map_res_adr,map_res_geo = FPSGeoIP.get_geocode_with_geocoder(street=street,house=house,zipcode=zipcode,city=city,country=country)
        if (len(map_res_adr)*len(map_res_geo)>0):
                _data = [key for key in map_res_adr.keys()]
                street2 = street2.replace('.','')        
                street2 = max([ len(filter(lambda y: x in y,_data)) for x in street2.split()  ]) if (not(street2 is None) and (len(street2)>0)) else 0
                zipcode2 = max([ len(filter(lambda y: x in y,_data)) for x in zipcode2.split()  ]) if (not(zipcode2 is None) and (len(zipcode2)>0)) else 0
                city2 = max([ len(filter(lambda y: x in y,_data)) for x in city2.split()  ]) if (not(city2 is None) and (len(city2)>0)) else 0
                country2 = max([ len(filter(lambda y: x in y,_data)) for x in country2.split()  ])  if (not(country2 is None) and (len(country2)>0)) else 0

                res_addr = ( 
                        ('city',1 if city2>0 else 0),
                        ('country',1 if country2>0 else 0),
                        ('zipcode',1 if zipcode2>0 else 0),
                        ('street',1 if street2>0 else 0)
                    )
     else:
        locators = [FPSGeoIP.osm_geocoder, FPSGeoIP.openmap_geocoder  ,FPSGeoIP.yandex_geocoder ]
        for locator in locators:
            map_res_adr,map_res_geo = FPSGeoIP.get_geocode_with_geolocator(locator,street=street,house=house,zipcode=zipcode,city=city,country=country)
            if (len(map_res_adr)*len(map_res_geo)==0): continue
            else:

                _data = [key for key in map_res_adr.keys()]
                street2 = street2.replace('.','')        
                street2 = max([ len(filter(lambda y: x in y,_data)) for x in street2.split()  ]) if (not(street2 is None) and (len(street2)>0)) else 0
                zipcode2 = max([ len(filter(lambda y: x in y,_data)) for x in zipcode2.split()  ]) if (not(zipcode2 is None) and (len(zipcode2)>0)) else 0
                city2 = max([ len(filter(lambda y: x in y,_data)) for x in city2.split()  ]) if (not(city2 is None) and (len(city2)>0)) else 0
                country2 = max([ len(filter(lambda y: x in y,_data)) for x in country2.split()  ])  if (not(country2 is None) and (len(country2)>0)) else 0

                res_addr = ( 
                        ('city',1 if city2>0 else 0),
                        ('country',1 if country2>0 else 0),
                        ('zipcode',1 if zipcode2>0 else 0),
                        ('street',1 if street2>0 else 0)
                    )
                break

     booker_point  = (map_res_geo['lat'],map_res_geo['long']) if len(map_res_geo)>0 else None
    
     # calculate distance between booker and airports
     dist_booker_airport2 = great_circle(booker_point, end_point).kilometers if not(booker_point is None) and not(end_point is None) else -1
     dist_booker_airport1 = great_circle(booker_point, start_point).kilometers if not(booker_point is None) and not(start_point is None) else -1
    except:
     pass
    return (res_ip, res_addr,dist_airport1_airport2,dist_booker_airport1,dist_booker_airport2)


 @staticmethod
 def is_zipcode_correct(zipcode='',city='',country=''):
    res=FPSGeoIP.validate_zipcode(zipcode,country)    
    #print 'res from FPSGeoIP:',res

    res_zip = (
                    ('city',1 if res and \
                              unicode(city).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower() in \
                              unicode(res['city']).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower() \
                              else 0
                    ),
                    ('zipcode',1 if res else 0)
               )
    return  res_zip 

if __name__ == '__main__':

 '''  standalone program to validate zipcode and postal addresses  '''

 
 def validate(data,count,entries,validate_zip=None,validate_addr=None,validate_ip=None,validate_proxy=None,test=None):
    if len(entries) > 0:
        data = [data[i] for i in entries]
    counter = 0    
    for entry in data:
            booker = entry['params']['booker']
            counter += 1
            if counter >count: break
            print "Nr.",counter, "***************************"
            print 
            print 
            print booker
            print 
            if (validate_zip): validate_zip_code(booker)
            if (validate_addr): validate_address(booker)
            if (validate_ip): validate__ip(booker)
            if (validate_proxy): validate__proxy(booker)

    return

 def validate_zip_code(booker):
    print "---------------------------"
    res=FPSGeoIP.validate_zipcode(booker['zipcode'],booker['country'])
    print 'validate zipcode result  ->', res
    print  
    print 'city: ',   'Found'  if res and \
            unicode(booker['city']).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower() in \
            unicode(res['city']).translate(FPSGeoIP.table).encode('ascii', 'ignore').lower() \
            else 'Not found'
    print "---------------------------"
    return


 def validate_address(booker):
    print "---------------------------"
    res= FPSGeoIP.validate_address(
            street=booker['street'].lower(),
            #house=booker['houseNr'],
            city= booker['city'].lower(),            
            country=FPSGeoIP.country_codes[booker['country'].lower()],
            zipcode=booker['zipcode'], geocoder=True,do_print=True
            )
    print 'validate address result  ->', res
    print  
    print "---------------------------"
    return

 def validate__ip(booker):
    print "---------------------------"
    res= FPSGeoIP.validate_ip(ip=booker['ip'],
            street=booker['street'],
            city= booker['city'].lower(),
            country_code=booker['country'].lower(),
            country=FPSGeoIP.country_codes[booker['country'].lower()],
            zipcode=booker['zipcode'])
    print 'validate ip result  ->', res
    print  
    print "---------------------------"
    return

 def validate__proxy(booker):
    print "---------------------------"
    res= FPSGeoIP.validate_proxy(ip=booker['ip']
            )
    print 'validate proxy result  ->', res
    print  
    print "---------------------------"
    return


 import argparse
 import read_json  
 parser = argparse.ArgumentParser(description=__doc__)
 
 
 parser.add_argument('-t','--test', help='make a simple test',default=None, required=False, action='store_true')
 parser.add_argument('-c','--count', help='how many entries from file to process',default=40, required=False, action='store',type=int)
 parser.add_argument('-vz','--validate_zip', help='validate zipcode ',default=None, required=False, action='store_true')
 parser.add_argument('-vaddr','--validate_addr', help='validate address ',default=None, required=False, action='store_true')
 parser.add_argument('-vip','--validate_ip', help='validate ip ',default=None, required=False, action='store_true')
 parser.add_argument('-vpr','--validate_proxy', help='validate proxy ',default=None, required=False, action='store_true')
 parser.add_argument('-e','--entries', help=' a list of entries',default=[], nargs='+',type=int)
 parser.add_argument('filename', action='store')

 

 args = parser.parse_args()
 data = read_json.read_fps_data(args.filename)

 print vars(args)
 count=vars(args)['count']
 entries=vars(args)['entries']
 del vars(args)['count']
 del vars(args)['filename']
 del vars(args)['entries']

 # do validation
 if any( 'validate' in  key and value is not None  for key,value in vars(args).items()):
    validate(data,count,entries,**vars(args)) 

 # do a simple test
 if ( not('test' in vars(args) and vars(args)['test'])):  sys.exit(0)
    

 # Test 1: get geo data using IP
 print FPSGeoIP.get_geodata_ip('8.8.8.8') # google DNS
 res= FPSGeoIP.get_geodata_ip('87.118.83.186',False) # Unister IPs
 print res

 # Test 2: Then get Data about the place from long,lat
 print "address of Unister:", FPSGeoIP.get_geodata_lat(res['latitude'],res['longitude'])

 

 # Test 4: country codes: DE
 print FPSGeoIP.country_codes['de']

 
 # Final Test: a validation of a few entries from JSON
 
 counter = 0
 for entry in data:
  booker = entry['params']['booker']
  counter += 1
  if counter >40: break
 
 
  print "Nr.",counter, "***************************"
  print 
  print 
  print booker
  print 
  #if FPSGeoIP.get_geocode(street=booker['street'], house=booker['houseNr'], city= booker['city'].lower(), zipcode=booker['zipcode'],  country=FPSGeoIP.country_codes[booker['country'].lower()]) is None: print "Found None"
  #if FPSGeoIP.get_geocode(street=booker['street'], city= booker['city'].lower(), zipcode=booker['zipcode'], country=FPSGeoIP.country_codes[booker['country'].lower()]) is None: print "Found None"
  print "---------------------------"
  print 'validate address ->  ', FPSGeoIP.validate_address(street=booker['street'].lower(),
            #house=booker['houseNr'],
            city= booker['city'].lower(),            
            country=FPSGeoIP.country_codes[booker['country'].lower()],
            zipcode=booker['zipcode'], geocoder=True
            )

  
  print 
  print "---------------------------"
  print 'validate ip  ->', FPSGeoIP.validate_ip(ip=booker['ip'],
            street=booker['street'],
            city= booker['city'].lower(),
            country_code=booker['country'].lower(),
            country=FPSGeoIP.country_codes[booker['country'].lower()],
            zipcode=booker['zipcode'],
    )

  
  print 
  print "---------------------------"
  print "Calculating distance ...."
  product = entry['params']['product']
  depArpt=product['departureAirport']
  arrArpt=product['arrivalAirport']
  #distance_airports =  FPSGeoIP.calculate_distance_airports(depArpt,arrArpt)
  #geodata_booker = FPSGeoIP.get_lat_long(street=booker['street'], city= booker['city'].lower(), zipcode=booker['zipcode'], country=FPSGeoIP.country_codes[booker['country'].lower()])
  #booker_lat=None
  #booker_long=None
  #if geodata_booker is None: print "Found None"
  #else: 
  #    booker_lat = geodata_booker['lat']
  #    booker_long = geodata_booker['long']
  #distance_booker_depArpt =  FPSGeoIP.calculate_distance(booker_lat,booker_long,depArpt)
  #distance_booker_arrArpt =  FPSGeoIP.calculate_distance(booker_lat,booker_long,arrArpt)
  #print 
  #print "distances: \n between departureAirport(%s) and  arrivalAirport(%s) --> %f,    between booker(%s) and  departureAirport(%s) --> %f, \n        between booker(%s) and  arrivalAirport(%s) --> %f"%(depArpt,arrArpt, distance_airports,booker['lastName'],depArpt,distance_booker_depArpt,booker['lastName'],arrArpt,distance_booker_arrArpt)  
  #print 
  (res_ip, res_addr,dist_airport1_airport2,dist_booker_airport1,dist_booker_airport2) =  FPSGeoIP.calculate_and_validate(
            ip=booker['ip'],
            street=booker['street'],
            city= booker['city'].lower(),            
            country=FPSGeoIP.country_codes[booker['country'].lower()],
            zipcode=booker['zipcode'],airport1=depArpt,airport2=arrArpt,
            geocoder=True
            )
  print res_ip, res_addr
  print 
  print "distances: \n between departureAirport(%s) and  arrivalAirport(%s) --> %f,    between booker(%s) and  departureAirport(%s) --> %f, \n        between booker(%s) and  arrivalAirport(%s) --> %f"%(depArpt,arrArpt,dist_airport1_airport2,booker['lastName'],depArpt,dist_booker_airport1,booker['lastName'],arrArpt,dist_booker_airport2)  
  print   
  print  


 sys.exit(0)
 
