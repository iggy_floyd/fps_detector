# -*- coding: utf-8 -*-


# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import pandas as pd
import time 
import numpy as np


class ZipGeoCode(object):
    names=[
        "country code", 'postal code','place name','admin name1','admin code1', 'admin name2',
        'admin code2' , 'admin name3','admin code3','latitude','longitude','accuracy']
    start = time.time()
    data = pd.read_csv('allCountries.txt',na_values=[' '],keep_default_na = False,sep='\t',header=None,names=names,encoding='utf-8', dtype=np.str)
    print "%d eintries has been loaded for %.3f sec."%(len(data), time.time()-start)
    
    # lowering
    for name in names:
        data[name] = data[name].map(unicode.lower)
    
    @staticmethod
    def searchGeoCodesCity(city,country):  
        city=city.lower()
        country = country.lower()        
        result = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['place name'] == city) & (ZipGeoCode.data['country code'] == country)].index,['latitude','longitude']]
        
        
        #print "in ZipGeoCode::searchGeoCodesCity",city
        #print "in ZipGeoCode::searchGeoCodesCity",country
        #print result
        if len(result)==0: return (None,None)
        
        return (float(result['latitude'].as_matrix()[0]),float(result['longitude'].as_matrix()[0]))
        
    @staticmethod
    def searchGeoCodesPostal(postal,country):  
        
        postal = str(postal)
        country = country.lower()     
        result = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['postal code'] == postal) & (ZipGeoCode.data['country code'] == country)].index,['latitude','longitude']]
        
        
        
        if len(result)==0: return (None,None)
        
        return (float(result['latitude'].as_matrix()[0]),float(result['longitude'].as_matrix()[0]))
    
    @staticmethod
    def ValidatePostal(postal,country,city):  
        
        postal = str(postal)
        country = country.lower()    
        city=city.lower()
        result1 = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['postal code'] == postal) & (ZipGeoCode.data['country code'] == country)].index,['country code']]
        result2 = ZipGeoCode.data.loc[ZipGeoCode.data[(ZipGeoCode.data['postal code'] == postal) & (ZipGeoCode.data['place name'] == city)].index,['place name']]
        
        
        return (int(len(result1)>0),int(len(result2)>0))
    


if __name__ == "__main__":
    
    #print ZipGeoCode.data.head()
    #print ZipGeoCode.searchGeoCodesCity('Schönau','DE')
    #print ZipGeoCode.searchGeoCodesCity('Eichwalde','DE')
    #print ZipGeoCode.searchGeoCodesCity('München','DE')
    print ZipGeoCode.searchGeoCodesCity(u'münchen','de')
    #print ZipGeoCode.searchGeoCodesPostal(15732,'DE')
    #print ZipGeoCode.ValidatePostal(15732,'DE','Eichwalde')
    #print ZipGeoCode.ValidatePostal(15732,'DE','')
