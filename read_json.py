'''

	A simple reader of informarion from the FPS bookings.
	python read_json.py fps_data_1.json

'''

import sys
import json
import time

def read_fps_data(filename):
 ''' read  data from filename '''
 data = []
 with open(filename,'r') as f:
    for line in f:
        data.append(json.loads(line))

 counter=0
 for item in data:
  counter +=1

 print ("Found Nr. of entries in {0}: {1}").format(filename, counter)
 return data




if __name__ == '__main__':
 filename = sys.argv[1]
 #decoder = json.JSONDecoder()
 start_time = time.time()
 data = read_fps_data(filename)
 print("--- %s seconds ellapsed ---" % str(time.time() - start_time))
