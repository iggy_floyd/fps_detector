# -*- coding: utf-8 -*-


# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import pandas as pd
import time 
import numpy as np
import sqlite3
from StringIO import StringIO
import prettytable 
import unicodecsv as  csv

class ZipGeoCodeSQlite(object):
    names=[
        "country code", 'postal code','place name','admin name1','admin code1', 'admin name2',
        'admin code2' , 'admin name3','admin code3','latitude','longitude','accuracy']
    start = time.time()
    data = pd.read_csv('allCountries.txt',na_values=[' '],keep_default_na = False,sep='\t',header=None,names=names,
    encoding='utf-8', 
    dtype=np.str)
    
    print "processed %s : %d eintries has been loaded for %.3f sec."%('allCountries.txt',len(data), time.time()-start)
    
    # lowering
    for name in names:
        data[name] = data[name].map(unicode.lower)
        #data[name] = data[name].map(str.lower)
    
    uri = 'file:allCountries_database?mode=memory&cache=shared'
    #con = sqlite3.connect(uri,uri = True)
    #con = sqlite3.connect(uri)
    con = sqlite3.connect(':memory:')



    cur = con.cursor()
    cur.execute("PRAGMA page_size =32768 ;") 
    cur.execute("PRAGMA journal_mode = OFF ;") 
    cur.execute("PRAGMA synchronous = OFF ;") 
    
     
    cur.execute('CREATE TABLE  allCountries (city varchar(20), country varchar(2), postal number, lat float, long float );')
    print "table allCountries is  created after", time.time()-start, " seconds elapsed"
    
    # filling the database in memory
    start = time.time()
    output = StringIO()
    data[['place name','country code','postal code','latitude','longitude']].to_csv(output, sep=';', encoding='utf-8',index=False)
    output.seek(0)
    csv_reader = csv.reader(output, delimiter=';',encoding='utf-8')
    print "csv_reader is  ready after ",  time.time()-start, " seconds elapsed"
    cur.executemany('INSERT INTO allCountries VALUES (?,?,?,?,?)', csv_reader)
    cur.close()
    con.commit()
    print "processed %s : %d eintries has been loaded for %.3f sec."%('SQlite allCountries',len(data), time.time()-start)
    
    # remove unneeded attributes
    start = None
    output = None
    csv_reader = None
    
    
    @staticmethod
    def searchGeoCodesCity_v2(city,country):  
        city=city.lower()
        country = country.lower()        
        
        _find_city_country = 'SELECT lat, long FROM allCountries WHERE city = ? AND country=? LIMIT 1'
        res = ZipGeoCodeSQlite.con.execute(_find_city_country,(city,country,)).fetchone()        
        if res is None: return (None,None)
                
        
        return res

    
    @staticmethod
    def searchGeoCodesPostal_v2(postal,country):  
        postal = str(postal)
        country = country.lower()        
        
        _find_postal_country = 'SELECT lat, long FROM allCountries WHERE postal = ? AND country=? LIMIT 1'
        res = ZipGeoCodeSQlite.con.execute(_find_postal_country,(postal,country,)).fetchone()        
        if res is None: return (None,None)
                
        
        return res

    @staticmethod
    def ValidatePostal_v2(postal,country,city):  
        
        postal = str(postal)
        country = country.lower()    
        city=city.lower()
        
        _find_city_country = 'SELECT lat, long FROM allCountries WHERE postal = ? AND country=? LIMIT 1'
        _find_postal_country = 'SELECT lat, long FROM allCountries WHERE postal = ? AND city=? LIMIT 1'
        res1 = ZipGeoCodeSQlite.con.execute(_find_city_country,(postal,country,)).fetchone()        
        res2 = ZipGeoCodeSQlite.con.execute(_find_postal_country,(postal,city,)).fetchone()        
                
        return ( 0 if res1 is None else 1, 0 if res2 is None else 1 )
                
        
        
    @staticmethod
    def searchGeoCodesCity(city,country):  
        city=city.lower()
        country = country.lower()        
        result = ZipGeoCodeSQlite.data.loc[ZipGeoCodeSQlite.data[(ZipGeoCodeSQlite.data['place name'] == city) & (ZipGeoCodeSQlite.data['country code'] == country)].index,['latitude','longitude']]
        
        if len(result)==0: return (None,None)
        
        return (float(result['latitude'].as_matrix()[0]),float(result['longitude'].as_matrix()[0]))
        
    @staticmethod
    def searchGeoCodesPostal(postal,country):  
        
        postal = str(postal)
        country = country.lower()     
        result = ZipGeoCodeSQlite.data.loc[ZipGeoCodeSQlite.data[(ZipGeoCodeSQlite.data['postal code'] == postal) & (ZipGeoCodeSQlite.data['country code'] == country)].index,['latitude','longitude']]
        
        
        
        if len(result)==0: return (None,None)
        
        return (float(result['latitude'].as_matrix()[0]),float(result['longitude'].as_matrix()[0]))
    
    @staticmethod
    def ValidatePostal(postal,country,city):  
        
        postal = str(postal)
        country = country.lower()    
        city=city.lower()
        result1 = ZipGeoCodeSQlite.data.loc[ZipGeoCodeSQlite.data[(ZipGeoCodeSQlite.data['postal code'] == postal) & (ZipGeoCodeSQlite.data['country code'] == country)].index,['country code']]
        result2 = ZipGeoCodeSQlite.data.loc[ZipGeoCodeSQlite.data[(ZipGeoCodeSQlite.data['postal code'] == postal) & (ZipGeoCodeSQlite.data['place name'] == city)].index,['place name']]
        
        
        return (int(len(result1)>0),int(len(result2)>0))
    

#### Too SLOW!!!


if __name__ == "__main__":
    

    start = time.time()
    print ZipGeoCodeSQlite.searchGeoCodesCity(u'München','DE')
    print time.time() -start
    
    start = time.time()
    print ZipGeoCodeSQlite.searchGeoCodesCity_v2(u'München','DE')
    print time.time() -start

    start = time.time()
    print ZipGeoCodeSQlite.searchGeoCodesCity_v2(u'München2','DE')
    print time.time() -start

    
    start = time.time()
    print ZipGeoCodeSQlite.searchGeoCodesPostal(15732,'DE')
    print time.time() -start
    
    
    start = time.time()
    print ZipGeoCodeSQlite.searchGeoCodesPostal_v2(15732,'DE')
    print time.time() -start
    
    start = time.time()
    print ZipGeoCodeSQlite.ValidatePostal(15732,'DE','Eichwalde')
    print time.time() -start
    
    start = time.time()
    print ZipGeoCodeSQlite.ValidatePostal_v2(15732,'DE','Eichwalde')
    print time.time() -start
    
    start = time.time()
    print ZipGeoCodeSQlite.ValidatePostal(15732,'DE','')
    print time.time() -start
    
    start = time.time()
    print ZipGeoCodeSQlite.ValidatePostal_v2(15732,'DE','')
    print time.time() -start
    