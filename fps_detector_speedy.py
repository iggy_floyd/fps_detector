import speedy
from speedy import zeromq

def do_something(*args):

	print list(args)
	return list(args)


SERVER_PORT = 8090


class MyServer(speedy.Server):
    def foo(self, handle, request):
	print request
#        handle.done(do_something(request.foo, request.bar))
        handle.done(do_something(request['foo'], request['bar']))

server = MyServer(zeromq.server_socket(('127.0.0.1', SERVER_PORT)))
server.serve() # blocks until server exits
