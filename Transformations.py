#! /usr/bin/python
# -*- coding: utf-8 -*-



#  File:   Transformations.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on Apr 23, 2015, 1:51:18 PM









__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 23, 2015 1:51:18 PM$"

'''
    _add* function are transformers which will be used for transformation
'''

def _addBooking_period(self,datasetframe=None):

        if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return
        
        # Test 8: Add booking period to DataFrame
        f = lambda x: TimeConvert.TimeConvert.difference_days(x['booker_bookingDate'].split()[0],x['product_departureDate'],format='%d.%m.%Y')  
        #f = lambda x: TimeConvert.TimeConvert.difference_days(x['datetime'].split()[0],x['product_departureDate'],format='%d.%m.%Y')  

        df2 = datasetframe.apply(f,axis=1)
        datasetframe['booking_period'] = df2

        return self

def _addLevenshteine(self,datasetframe=None):
    if (datasetframe is None): 
        if (self.dataframe  is not None ):  datasetframe = self.dataframe
        else: return self


 # we will use the similarity instead of the distance
    def LD(x,feature):
            if not('booker_lastName' in x) or x['booker_lastName'] == None  or len(x['booker_lastName'] ) == 0 or pd.isnull(x['booker_lastName']) : return 0.0
            if not(feature in x) or x[feature]  == None  or len(x[feature] ) == 0 or pd.isnull(x[feature]): return 0.0
            #return 1.0-StringMatcher.StringMatcher(None,x['booker_lastName'],x['payer_lastName']).ratio() 
            #return StringMatcher.StringMatcher(None,x['booker_lastName'].lower(),x['payer_lastName'].lower()).ratio() 

            # take into account german and other umlaut
            # see the table http://lwp.interglacial.com/appf_01.htm
            table = {
                0xe4: u'ae', # a diaeresis
                ord(u'ö'): u'oe',
                ord(u'ü'): u'ue',
                ord(u'ß'): u'ss',
                0xe1: u'a',  	# a acute 'á' => 'a'
                0xe0: u'a',     # a grave 'à' => 'a'
                0xe2: u'a',     # a circumflex  'â' => 'a'
                0xe3: u'a',     # a tilde 'ã' => 'a'
                0xe7: u'c',     # c cedilla 'ç' => 'c'
                0xe9: u'e',     # e acute 'é' => 'e'
                0xe8: u'e',     # e grave 'è' => 'e'
                0xea: u'e',     # e circumflex 'ê' => 'e'
                0xed: u'i',     # i acute 'í' => 'i'
                0xec: u'i',     # i grave 'ì' => 'i'
                0xee: u'i',     # i circumflex 'î' => 'i'
                0xef: u'i',     # i diaeresis 'ï' => 'i'
                0xf1: u'n',     # n tilde 'ñ' => 'n'
                0xf2: u'o',     # o grave 'ò' => 'o'
                0xf3: u'o',     # o acute 'ó' => 'o'
                0xf5: u'o',     # o tilde   'õ' => 'o'
                0xf4: u'o',     # o circumflex 'ô' => 'o'
                0xf6: u'o',     # o diaeresis 'ï' => 'o'
                0xf9: u'u',     # u grave 'ù' => 'u'
                0xfa: u'u',     # u acute 'ú' => 'u'
                0xfb: u'u',     # u circumflex 'û' => 'u'
                0xfd: u'y'      # y acute 'ý' => 'y'
            }

            blast = unicode(x['booker_lastName'].lower())
            plast = unicode(x['payer_lastName'].lower())
            blast = blast.translate(table).encode('ascii', 'ignore')
            plast = plast.translate(table).encode('ascii', 'ignore')
            #print >> open("testnames.log","a"), "blast=",blast,"plast=",plast
            

            if min([len(blast),len(plast)]) == 0 : return 0.0

            dist = StringMatcher.StringMatcher(None,blast,plast).distance()

            # take into accound doubled names (separated by ' ' or '-')
            if (dist > 0 and ((' ' in blast+plast) or ('-' in blast+plast))):
                import itertools
                import re
                delimiters = ' ', '-'
                regexPattern = '|'.join(map(re.escape, delimiters))
                blast_list =re.split(regexPattern, blast)
                plast_list =re.split(regexPattern, plast)
                #print >> open("testnames_list.log","a"), "blast=",blast_list,"plast=",plast_list
                distances = [  StringMatcher.StringMatcher(None,r[0],r[1]).distance()  for r in itertools.product(blast_list, plast_list)]
                dist = min(distances)
                


            return 1.0 - min(1.0,float(dist)/float( min([len(blast),len(plast)]) ))

  # we will use the similarity instead of the distance
    def LD_traveller(x,name):
            if  not('booker_lastName' in x) or x['booker_lastName'] == None  or len(x['booker_lastName'] ) == 0 or pd.isnull(x['booker_lastName']) : return 0.0
            if  not(name in x) or  x[name] is None or pd.isnull(x[name]): return 0.0
            
            dict =   x[name]
            if not ('lastName' in dict.keys()):  return 0.0  
            
            # take into account german and other umlaut
            # see the table http://lwp.interglacial.com/appf_01.htm
            table = {
                0xe4: u'ae', # a diaeresis
                ord(u'ö'): u'oe',
                ord(u'ü'): u'ue',
                ord(u'ß'): u'ss',
                0xe1: u'a',  	# a acute 'á' => 'a'
                0xe0: u'a',     # a grave 'à' => 'a'
                0xe2: u'a',     # a circumflex  'â' => 'a'
                0xe3: u'a',     # a tilde 'ã' => 'a'
                0xe7: u'c',     # c cedilla 'ç' => 'c'
                0xe9: u'e',     # e acute 'é' => 'e'
                0xe8: u'e',     # e grave 'è' => 'e'
                0xea: u'e',     # e circumflex 'ê' => 'e'
                0xed: u'i',     # i acute 'í' => 'i'
                0xec: u'i',     # i grave 'ì' => 'i'
                0xee: u'i',     # i circumflex 'î' => 'i'
                0xef: u'i',     # i diaeresis 'ï' => 'i'
                0xf1: u'n',     # n tilde 'ñ' => 'n'
                0xf2: u'o',     # o grave 'ò' => 'o'
                0xf3: u'o',     # o acute 'ó' => 'o'
                0xf5: u'o',     # o tilde   'õ' => 'o'
                0xf4: u'o',     # o circumflex 'ô' => 'o'
                0xf6: u'o',     # o diaeresis 'ï' => 'o'
                0xf9: u'u',     # u grave 'ù' => 'u'
                0xfa: u'u',     # u acute 'ú' => 'u'
                0xfb: u'u',     # u circumflex 'û' => 'u'
                0xfd: u'y'      # y acute 'ý' => 'y'
            }

            plast = dict['lastName']
            blast = unicode(x['booker_lastName'].lower())
            plast = unicode(plast.lower())
            blast = blast.translate(table).encode('ascii', 'ignore')
            plast = plast.translate(table).encode('ascii', 'ignore')
            #print >> open("testnames_traveller.log","a"), "blast=",blast,"plast=",plast
            

            if min([len(blast),len(plast)]) == 0 : return 0.0

            dist = StringMatcher.StringMatcher(None,blast,plast).distance()

            # take into accound doubled names (separated by ' ' or '-')
            if (dist > 0 and ((' ' in blast+plast) or ('-' in blast+plast))):
                import itertools
                import re
                delimiters = ' ', '-'
                regexPattern = '|'.join(map(re.escape, delimiters))
                blast_list =re.split(regexPattern, blast)
                plast_list =re.split(regexPattern, plast)
                #print >> open("testnames_list_travellers.log","a"), "blast=",blast_list,"plast=",plast_list
                distances = [  StringMatcher.StringMatcher(None,r[0],r[1]).distance()  for r in itertools.product(blast_list, plast_list)]
                dist = min(distances)
                


            return 1.0 - min(1.0,float(dist)/float( min([len(blast),len(plast)]) ))

    def LD_Combined(x):

            #LD_Booker_Payer
            LD_Booker_Payer = LD(x,'payer_lastName')
    
            #LD_Booker_Traveller_0
            LD_Booker_Traveller_0 = LD_traveller(x,'traveller_0')
    
            # LD_ALL
            a=[ key for key, value in x.iteritems() if ('traveller_' in key)  and (value != None) and ( type(value) == type({}) ) ]
            if len(a) == 0: return 0.0            
            b = [ LD_traveller(x,key)  for key in a  ]
            LD_ALL =  max(b)
            
            return LD_Booker_Payer,LD_Booker_Traveller_0,LD_ALL
    datasetframe['LD_booker_payer'],datasetframe['LD_booker_first_traveller'],datasetframe['LD_booker_all_travellers'] =\
        zip(*datasetframe.apply(LD_Combined,axis=1))
    
    return self    

def _addNumberTravellers(self,datasetframe=None):


        if (datasetframe is None): 
            if (self.dataframe  is not None ):  datasetframe = self.dataframe
            else: return self

        # Test 10: Add the number of travellers to the DataFrame 
        def f2(x):
            a=dict((key,value) for key, value in x.iteritems() if 'traveller_' in key and value != None and  type(value) == type({}) and not pd.isnull(x[key])  )            
            #print a
            #TransformDataset.counter+=1
            #print "TransformDataset: processed %d "%TransformDataset.counter
            #print len(a)
            return len(a)   

        df2 = datasetframe.apply(f2,axis=1)
        datasetframe['Number_of_travellers'] = df2
        return self

def _addCountryMatches(self,datasetframe=None):
    
    if (datasetframe is None): 
            if (self.dataframe  is not None):  datasetframe = self.dataframe
            else: return self

    
    # Test 11: Add the case when IPCountry  != BinCountry of the booker and payer to the DataFrame


    def f6(x):
        
        booker_ipCountry_country =1
        booker_country_payer_binCountry = 1
        booker_ipCountry_payer_binCountry =1
        
        if not('booker_ipCountry' in x) or x['booker_ipCountry'] is None or pd.isnull(x['booker_ipCountry']):
            booker_ipCountry_country =0
            booker_ipCountry_payer_binCountry =0
            
        if not('booker_country' in x) or x['booker_country'] is None or pd.isnull(x['booker_country']):
            booker_ipCountry_country =0
            booker_country_payer_binCountry = 0
             
        if not('payer_binCountry' in x) or x['payer_binCountry'] is None or pd.isnull(x['payer_binCountry']):
            booker_ipCountry_payer_binCountry = 0
            booker_country_payer_binCountry = 0
        
        booker_ipCountry_country = int( (x['booker_ipCountry'] == x['booker_country']) and (booker_ipCountry_country != 0) )   
        booker_country_payer_binCountry = int( (x['booker_country'] == x['payer_binCountry']) and (booker_country_payer_binCountry != 0) )   
        booker_ipCountry_payer_binCountry = int( (x['booker_ipCountry'] == x['payer_binCountry']) and (booker_ipCountry_payer_binCountry != 0) )   
        
        return   booker_ipCountry_country,booker_country_payer_binCountry,booker_ipCountry_payer_binCountry
             

    datasetframe['booker_ipCountry_country'],datasetframe['booker_country_payer_binCountry'],datasetframe['booker_ipCountry_payer_binCountry'] =\
        zip(*datasetframe.apply(f6,axis=1))
  
    return self


def _addIs(self,datasetframe=None):
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self

    def f7(x):
        
        booker_email = 0 if not('booker_email' in x) or x['booker_email'] is None or pd.isnull(x['booker_email']) else 1
        booker_phone = 0 if not('booker_phone' in x) or x['booker_phone'] is None or pd.isnull(x['booker_phone']) else 1
        booker_birthday = 0 if not('booker_birthday' in x) or x['booker_birthday'] is None or pd.isnull(x['booker_birthday']) else 1
        booker_zipcode = 0 if not('booker_zipcode' in x) or x['booker_zipcode'] is None or pd.isnull(x['booker_zipcode']) else 1
        
        return booker_email,booker_phone,booker_birthday,booker_zipcode
    
    datasetframe['is_email'],datasetframe['is_phone'],datasetframe['is_birthday'], datasetframe['is_zipcode'] =\
        zip(*datasetframe.apply(f7,axis=1))
        
    return self      

def _addAge(self,datasetframe=None):
     if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
     def f7(x): 
         
         booker_age = -100 if not('booker_birthday' in x) or x['booker_birthday'] is None or pd.isnull(x['booker_birthday']) else \
             TimeConvert.TimeConvert.age(x['booker_bookingDate'].split()[0],x['booker_birthday'],format='%d.%m.%Y')
         
         traveller_first_age = -100   if not('traveller_0' in x) or x['traveller_0'] is None or pd.isnull(x['traveller_0']) else 1
         dict_traveller_0 =   x['traveller_0']
         traveller_first_age = -100 if ((not ('birthday' in dict_traveller_0.keys())) and (traveller_first_age <0))  else \
             TimeConvert.TimeConvert.age(x['booker_bookingDate'].split()[0],dict_traveller_0['birthday'],format='%d.%m.%Y')
         
         
         a=dict((key,value) for key, value in x.iteritems() if ('traveller_' in key) and (value != None) and  (type(value) == type({})) and not pd.isnull(x[key])  )            
         traveller_avg_age = -100  if len(a) == 0 else 1
         count = 0.
         totalAge = 0. 
         for key,value in a.items():
             if 'birthday' in value.keys():
                 age = TimeConvert.TimeConvert.age(x['booker_bookingDate'].split()[0],value['birthday'],format='%d.%m.%Y')
                 if age >0:
                     totalAge +=age
                     count += 1.
         
         traveller_avg_age = -100 if traveller_avg_age<0 or count==0 else float(totalAge)/count
         
         
         
         traveller_total_age = -100  if len(a) == 0 else 1
         traveller_avg_age = -100 if traveller_avg_age<0 else float(totalAge)
         
         
         return booker_age,traveller_first_age,traveller_avg_age,traveller_total_age
     
     
     datasetframe['booker_age'],datasetframe['traveller_first_age'],datasetframe['traveller_avg_age'], datasetframe['traveller_total_age'] =\
        zip(*datasetframe.apply(f7,axis=1))
     return self

def _addProductPrice(self,datasetframe=None):    
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if not('product_price' in x) or x['product_price'] is None or pd.isnull(x['product_price']): return -100        
        return float(x['product_price']) if float(x['product_price']) <20000 else 20000


    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['product_price'] = df7
    return self
 

def _addBookerEmailInformation(self,datasetframe=None):

    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self

    def f7(x):
        if  not('booker_email' in x) or x['booker_email'] is None or pd.isnull(x['booker_email']):
            return 'nan','nan'
        
        booker_email_country = x['booker_email'].split('@')[1].split('.')[-1].lower()
        
        # some table to correct user emails
        table = { 'googlemail':'gmail', 'google':'gmail' } 
        provider = x['booker_email'].split('@')[1].lower().replace('.'+booker_email_country,'')
        for key in sorted(table.iterkeys(),reverse=True): provider = provider.replace(key,table[key])

        return booker_email_country,provider
        
        
    datasetframe['booker_email_country'],datasetframe['booker_email_provider'] =\
        zip(*datasetframe.apply(f7,axis=1))
    

    return self
   
def _a_ddValidateZipCode(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if not('booker_zipcode' in x)  or x['booker_zipcode'] is None or pd.isnull(x['booker_zipcode']): return 0        
        if not('booker_country' in x)  or x['booker_country'] is None or pd.isnull(x['booker_country']): return 0
        if not('booker_city' in x)  or x['booker_city'] is None or pd.isnull(x['booker_city']): return 0
        result=FPSGeoIP.is_zipcode_correct( zipcode=x['booker_zipcode'],city= x['booker_city'], country=x['booker_country'])

        # may result to # (('city', 0), ('zipcode', 0)) or (('city', 0), ('zipcode', 1)) or (('city', 1), ('zipcode', 1))            
        return int(str(result[1][1])+str(result[0][1]),2) # return bit-wise word

    datasetframe['is_zipcode_correct'] = datasetframe.apply(f7,axis=1)
      
    
    return self   

def _addAirPortDistance(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
            
            
    def f7(x):
        depArpt= '' if not('product_departureAirport' in x)  or  x['product_departureAirport'] is None or pd.isnull(x['product_departureAirport']) else x['product_departureAirport']
        arrArpt= '' if not('product_arrivalAirport' in x)  or  x['product_arrivalAirport'] is None or pd.isnull(x['product_arrivalAirport']) else x['product_arrivalAirport']
        
        result=FPSGeoIP.airportdistance( airport1=depArpt,airport2=arrArpt)
        
        return result

    datasetframe['airport_dist'] = datasetframe.apply(f7,axis=1)
      
    
    return self   

def _a_ddBookerDistance(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
            
            
    def f7(x):
        street= '' if not('booker_street' in x) or   x['booker_street'] is None or pd.isnull(x['booker_street']) else x['booker_street'].lower()
        city= '' if  not('booker_city' in x) or   x['booker_city'] is None or pd.isnull(x['booker_city']) else x['booker_city'].lower()
        country_code= '' if  not('booker_country' in x) or  x['booker_country'] is None or pd.isnull(x['booker_country']) else x['booker_country'].lower()
        country=FPSGeoIP.country_codes[country_code] if  country_code != '' else ''
        zipcode= '' if  not('booker_zipcode' in x) or x['booker_zipcode'] is None or pd.isnull(x['booker_zipcode']) else x['booker_zipcode']
        
        depArpt= '' if not('product_departureAirport' in x)  or  x['product_departureAirport'] is None or pd.isnull(x['product_departureAirport']) else x['product_departureAirport']
        arrArpt= '' if not('product_arrivalAirport' in x)  or  x['product_arrivalAirport'] is None or pd.isnull(x['product_arrivalAirport']) else x['product_arrivalAirport']
        
        #result=FPSGeoIP.booker_airport_distance(street=street,city=city,country=country,zipcode=zipcode,airport1=depArpt,airport2=arrArpt)
        #result=FPSGeoIP.booker_airport_distance_v2(city=city,airport1=depArpt,airport2=arrArpt)
        
        #booker_lat,booker_long = GeoCodeCity.searchGeoCodes_v2(city.encode('utf-8'),country_code)        
        #if ((booker_lat is None) or (booker_long is None) ):
        #    booker_lat,booker_long = get_lat_long(urlend=city+','+country_code)
            
        result = FPSGeoIP.booker_airport_distance_v3(city=city,country=country_code,airport1=depArpt,airport2=arrArpt)

        print result
        return result

    datasetframe['airport_dist'],datasetframe['booker_depairport_dist'],datasetframe['booker_arrairport_dist'] =   zip(*datasetframe.apply(f7,axis=1))
    
      
    
    return self   


def _addPhoneValidation(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
            
            
    def f7(x):
        
        city= '' if  not('booker_city' in x) or   x['booker_city'] is None or pd.isnull(x['booker_city']) else x['booker_city'].lower()
        country_code= '' if  not('booker_country' in x) or  x['booker_country'] is None or pd.isnull(x['booker_country']) else x['booker_country'].lower()
        booker_phone = '' if not('booker_phone' in x) or x['booker_phone'] is None or pd.isnull(x['booker_phone']) else  x['booker_phone']
        country=FPSGeoIP.country_codes[country_code] if  country_code != '' else ''
        #if (True):
        try:
            
            phone_num = phonenumbers.parse( booker_phone, country_code)
            phone_ok = int(phonenumbers.is_possible_number(phone_num) and phonenumbers.is_valid_number(phone_num))      
            phone_city_ok =  int(unicode(city) == geocoder.description_for_number(phone_num, "en").lower() )

            if phone_city_ok > 0:            
                phone_country_ok = 1
            else:
                phone_country_ok =  int(unicode(country) == geocoder.description_for_number(phone_num, "en").lower() ) 
            


            
            score_phone = phone_ok + (2*phone_country_ok) + (4*phone_city_ok)
            return (phone_ok,phone_city_ok,phone_country_ok,score_phone)
        except:
            return (0,0,0,0)    
        
      
    datasetframe['phone_ok'],datasetframe['phone_city_ok'],datasetframe['phone_country_ok'],datasetframe['booker_valid_phone'] =   zip(*datasetframe.apply(f7,axis=1))
          
    
    return self   


def _a_ddValidateZipCode(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
            
            
    def f7(x):
        
        zipcode = '' if not('booker_zipcode' in x)  or x['booker_zipcode'] is None or pd.isnull(x['booker_zipcode'])  else x['booker_zipcode'].lower()
        city= '' if  not('booker_city' in x) or   x['booker_city'] is None or pd.isnull(x['booker_city']) else x['booker_city'].lower()
        country_code= '' if  not('booker_country' in x) or  x['booker_country'] is None or pd.isnull(x['booker_country']) else x['booker_country'].lower()
        
        return ZipGeoCode.ValidatePostal(zipcode,country_code,city)
                
      
    datasetframe['zipcode_country_ok'],datasetframe['zipcode_city_ok'] =   zip(*datasetframe.apply(f7,axis=1))
          
    
    return self   


def _a_ddTest(self,datasetframe=None):
    
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    
    def f7(x):
        city= '' if  not('booker_city' in x) or   x['booker_city'] is None or pd.isnull(x['booker_city']) else x['booker_city'].lower()
        print type(city), city
        print repr(city.encode('utf-8'))
        return
    
    datasetframe.apply(f7,axis=1)
    