#! /usr/bin/python



#  File:   ModelFeatures.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on May 4, 2015, 1:36:55 PM


__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$May 4, 2015 1:36:55 PM$"


'''

The class is designed to create Model Feature used in the modeling

'''

# System specific modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

from Logger import Logger
from Features import Features
import datetime
import time
import shutil
import dill
import os.path
import sys
import pandas as pd
import json
import copy




class ModelFeatures(Features,Logger):

    '''  ModelFeatures is responsible for:
        
                * reading transformed dataframe and train final transformers
                * storing transformers in memory (for providing to some customer objects later)
                * storing transformeres to pickle file
                * restoring transformeres from pickle file
                * parsing JSON event on-fly
    
    '''
    
    
    def __init__(self,process_name='fps_test',dataframe=None):
        
        super(Features, self).__init__()
        super(Logger, self).__init__()
        
        self.dataframe = dataframe[[feature[0] for feature in self.model_features_v2]       ]
        self.datasettransformed =None
        self.transformers={}
        self.process_name = process_name if (type(process_name) is str) and (len(process_name)>0) else 'fps_fraud_classification'    
        self.addEventHandler('ModelFeaturesCreate')        
        self.addEventHandler('ModelFeaturesBackup')
        self.status_ready=False
        
        
    
    def _createFeatures(self):
        ''' build features '''
        
        if self.dataframe is None:  raise ValueError('MODELFEATURES_DATASET_NOT_READY')     
            
        self.datasettransformed = self.dataframe.copy(deep=True)        
        
        # using v2 features
        for feature in self.model_features_v2:
         name_feature = feature[0]
         transformator = feature[1]      
     
         if transformator is not None:
             if ('function' in str(type(transformator))):                      
                 self.datasettransformed[name_feature] = self.datasettransformed[name_feature].apply(lambda x: transformator(x))
                
                 self.transformers[name_feature] = transformator
                 #print "function",name_feature

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 
                 #print "class",name_feature
                 transformator.fit_transform(self.datasettransformed[name_feature].as_matrix()) 
                 self.datasettransformed[name_feature] = self.datasettransformed[name_feature].apply(lambda x: transformator.transform([x])[0])                                
                 self.transformers[name_feature] = transformator
         else:
            self.transformers[name_feature] = None
        
        self.datasettransformed.to_csv(self.process_name+'_selected_features.csv', index=False,encoding='utf-8')
        
        with open(self.process_name+'_selected_features.pkl', 'wb') as f:
          dill.dump(self.model_features_v2,f)                        
          dill.dump(self.transformers,f)
          
          
        
        return self    



    def transform_data(self,data):
            ''' parsing an data event on-fly '''
            
            if not(self.status_ready): raise ValueError('MODELFEATURES_NOT_READY')
            if ((data is None) or (len(data) == 0)): raise ValueError('MODELFEATURES_TRANSFORM_DATA_WRONGTYPE')
        
            
            
            #print "keys=",self.transformers.keys()
            #print "(1) data=",data.head(1)
            datasettransformed = data.copy(deep=True).loc[:,self.transformers.keys()]
            #datasettransformed = data.copy(deep=True)
            #datasettransformed =datasettransformed[self.transformers.keys()]
            
            #print "(2) datasettransformed=",datasettransformed.head(1)
            
            
            #for feature in data:
            for feature in self.transformers.keys():
                if (self.transformers[feature] is None): continue
                #print feature
                #print self.transformers[feature]
                #print type(self.transformers[feature])
                if hasattr(self.transformers[feature],'transform'):        
                    #print "!!!!1"
                    datasettransformed[feature] = datasettransformed[feature].apply(lambda x: self.transformers[feature].transform([x])[0])
                else: 
                    #print "!!!!2"
                    datasettransformed[feature] = datasettransformed[feature].apply(lambda x: self.transformers[feature](x)) 
            return datasettransformed   
        
        
    def _backup(self,msg,fileprefix,filesuffix):
        
            ''' do a backup of the model features '''
        
            if not(self.status_ready): raise ValueError('MODELFEATURES_NOT_READY') 
            if not(os.path.exists(fileprefix+filesuffix+'.csv')): raise ValueError('MODELFEATURES_NOT_READY')
            if not(os.path.exists(fileprefix+filesuffix+'.pkl')): raise ValueError('MODELFEATURES_NOT_READY')

            # get a suffix for the backup file name
            suffix =  datetime.datetime.now().strftime("_%H_%M_%d_%m_%Y")
            # logging
            self.write2Log(type='ModelFeaturesBackup',msg=msg)
            # backup
            try:
                shutil.copy(fileprefix+filesuffix+'.csv', fileprefix + suffix + filesuffix+'.csv.bak')          
                shutil.copy(fileprefix+filesuffix+'.pkl', fileprefix + suffix + filesuffix+'.pkl.bak')          
            except shutil.Error as e:
                print('Error: %s' % e)
                raise ValueError('MODELFEATURES_BACKUP_FAILURE')
        
            return self
        
        
    def initModelFeatures(self,update=False,doBackup=True):
            '''Create or Update Model Features'''
            
            
            if not(os.path.exists(self.process_name+'_selected_features.csv')) or update:
            
                if doBackup and update: self._backup('Re-create and backup the Model Features...',self.process_name, '_selected_features')
                elif not(update): self.write2Log(type='ModelFeaturesCreate',msg='Create a new set of the Model Features')
                
                self._createFeatures()            
            
            else:    
                print "Model Features dataframe is read from ",self.process_name+'_selected_features.csv',"...."
                self.datasettransformed = pd.read_csv(self.process_name+'_selected_features.csv',na_values=[' '],keep_default_na = False)
                print "Model Features transformers  are read from ",self.process_name+'_selected_features.pkl',"...."
                with open(self.process_name+'_selected_features.pkl', 'rb') as f:
                    self.model_features_v2=dill.load(f)    
                    self.transformers=dill.load(f)
            
            self.status_ready=True        
            
            #return self # not needed any more as we use proxy for background process

    def getTrainSet(self,target):
        
        if not(self.status_ready): raise ValueError('MODELFEATURES_NOT_READY')
        if (self.datasettransformed is None): raise ValueError('MODELFEATURES_TRANSFORMEDDATA_NOT_READY')
        
        
        self.train_cols = copy.copy(self.datasettransformed.columns.values).tolist()
        self.train_cols.remove(target)
        return target,self.train_cols, self.datasettransformed[[target]].as_matrix(),self.datasettransformed[self.train_cols].as_matrix()



if __name__ == "__main__":
    
    
    filename = sys.argv[1]  if len(sys.argv)>1 else "fps_data_3.json"
    
    from DatasetTransformer import DatasetTransformer
    
    # Test 0: create DatasetTransformer
    DT=DatasetTransformer()
    print "Process '%s' is started"%DT.process_name

    
    # Test 1: create a dataframe
    DT.initDataFrame(filename,update=False)

    # Test 2: transformers of the ModelFeature to train
    MF=ModelFeatures(dataframe=DT.dataframe)
    MF.initModelFeatures()
    
    
    # Test 2:   JSON proceeding 
    with open(filename,'r') as f:
        for line in f:
             #print line
             start=time.time()
             data=DT.processJSON(line)                          
             print "time (in s) for data transformation required:", time.time() -start
             print data.head(1)
             start=time.time()
             print '\n\n'
             data=MF.transform_data(data)                          
             print "time (in s) for model feature creation required:", time.time() -start
             print data.head(1)
             
