# -*- coding: utf-8 -*-
'''
	This is a TransformDataset Class designed to transform DataFrame
'''

import pandas as pd
import numpy as np
import DataFrameImputer



class TransformDataset(object):
 '''
	TransformDataset -- to add some new columns  calculated on Dataset.py data
 '''

 def __init__(self,datasetframe=None):

        self.dataframe = datasetframe
        #DataFrameImputer.DataFrameImputer().fit(self.dataframe) 
        #self.dataframe = DataFrameImputer.DataFrameImputer().fit(self.dataframe).transform(self.dataframe)
        self.selected = None
	pass


 def imputtingColumns(self,selected):
        self.selected = selected
        dataframe = self.dataframe[selected]
        dataframe = DataFrameImputer.DataFrameImputer().fit(dataframe).transform(dataframe)
        for x in dataframe: self.dataframe[x] = dataframe[x]

        return self
  

 def addBooking_period(self,datasetframe=None):

        if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return
        
        # Test 8: Add booking period to DataFrame
        f = lambda x: TimeConvert.TimeConvert.difference_days(x['booker_bookingDate'].split()[0],x['product_departureDate'],format='%d.%m.%Y')  
        #f = lambda x: TimeConvert.TimeConvert.difference_days(x['datetime'].split()[0],x['product_departureDate'],format='%d.%m.%Y')  
        df2 = datasetframe.apply(f,axis=1)
        datasetframe['booking_period'] = df2

        return self

 def addLevenshtein(self,datasetframe=None):
 
        if (datasetframe is None): 
            if (self.dataframe  is not None ):  datasetframe = self.dataframe
            else: return self

        

        # Test 9: Add Levenshtein-Distance between family names of booker and payer 

 

        # we will use the similarity instead of the distance
        def LD(x):
            if x['booker_lastName'] == None  or len(x['booker_lastName'] ) == 0 or pd.isnull(x['booker_lastName']) : return 0.0
            if x['payer_lastName']  == None  or len(x['payer_lastName'] ) == 0 or pd.isnull(x['payer_lastName']): return 0.0
            #return 1.0-StringMatcher.StringMatcher(None,x['booker_lastName'],x['payer_lastName']).ratio() 
            #return StringMatcher.StringMatcher(None,x['booker_lastName'].lower(),x['payer_lastName'].lower()).ratio() 

            # take into account german and other umlaut
            # see the table http://lwp.interglacial.com/appf_01.htm
            table = {
                0xe4: u'ae', # a diaeresis
                ord(u'ö'): u'oe',
                ord(u'ü'): u'ue',
                ord(u'ß'): u'ss',
                0xe1: u'a',  	# a acute 'á' => 'a'
                0xe0: u'a',     # a grave 'à' => 'a'
                0xe2: u'a',     # a circumflex  'â' => 'a'
                0xe3: u'a',     # a tilde 'ã' => 'a'
                0xe7: u'c',     # c cedilla 'ç' => 'c'
                0xe9: u'e',     # e acute 'é' => 'e'
                0xe8: u'e',     # e grave 'è' => 'e'
                0xea: u'e',     # e circumflex 'ê' => 'e'
                0xed: u'i',     # i acute 'í' => 'i'
                0xec: u'i',     # i grave 'ì' => 'i'
                0xee: u'i',     # i circumflex 'î' => 'i'
                0xef: u'i',     # i diaeresis 'ï' => 'i'
                0xf1: u'n',     # n tilde 'ñ' => 'n'
                0xf2: u'o',     # o grave 'ò' => 'o'
                0xf3: u'o',     # o acute 'ó' => 'o'
                0xf5: u'o',     # o tilde   'õ' => 'o'
                0xf4: u'o',     # o circumflex 'ô' => 'o'
                0xf6: u'o',     # o diaeresis 'ï' => 'o'
                0xf9: u'u',     # u grave 'ù' => 'u'
                0xfa: u'u',     # u acute 'ú' => 'u'
                0xfb: u'u',     # u circumflex 'û' => 'u'
                0xfd: u'y'      # y acute 'ý' => 'y'
            }

            blast = unicode(x['booker_lastName'].lower())
            plast = unicode(x['payer_lastName'].lower())
            blast = blast.translate(table).encode('ascii', 'ignore')
            plast = plast.translate(table).encode('ascii', 'ignore')
            #print >> open("testnames.log","a"), "blast=",blast,"plast=",plast
            

            if min([len(blast),len(plast)]) == 0 : return 0.0

            dist = StringMatcher.StringMatcher(None,blast,plast).distance()

            # take into accound doubled names (separated by ' ' or '-')
            if (dist > 0 and ((' ' in blast+plast) or ('-' in blast+plast))):
                import itertools
                import re
                delimiters = ' ', '-'
                regexPattern = '|'.join(map(re.escape, delimiters))
                blast_list =re.split(regexPattern, blast)
                plast_list =re.split(regexPattern, plast)
                print >> open("testnames_list.log","a"), "blast=",blast_list,"plast=",plast_list
                distances = [  StringMatcher.StringMatcher(None,r[0],r[1]).distance()  for r in itertools.product(blast_list, plast_list)]
                dist = min(distances)
                


            return 1.0 - min(1.0,float(dist)/float( min([len(blast),len(plast)]) ))

        df2 = datasetframe.apply(LD,axis=1)
        datasetframe['LD_booker_payer'] = df2
        return self

 def addLevenshteinFirstTraveller(self,datasetframe=None):
 
        if (datasetframe is None): 
            if (self.dataframe  is not None ):  datasetframe = self.dataframe
            else: return self

        

        # Test 9: Add Levenshtein-Distance-Similarity between family names of booker and the first traveller

 

        # we will use the similarity instead of the distance
        def LD(x):
            if x['booker_lastName'] == None  or len(x['booker_lastName'] ) == 0 or pd.isnull(x['booker_lastName']) : return 0.0
            if x['traveller_0'] is None or pd.isnull(x['traveller_0']): return 0.0
            dict =   x['traveller_0']
            if not ('lastName' in dict.keys()):  return 0.0  
            
            # take into account german and other umlaut
            # see the table http://lwp.interglacial.com/appf_01.htm
            table = {
                0xe4: u'ae', # a diaeresis
                ord(u'ö'): u'oe',
                ord(u'ü'): u'ue',
                ord(u'ß'): u'ss',
                0xe1: u'a',  	# a acute 'á' => 'a'
                0xe0: u'a',     # a grave 'à' => 'a'
                0xe2: u'a',     # a circumflex  'â' => 'a'
                0xe3: u'a',     # a tilde 'ã' => 'a'
                0xe7: u'c',     # c cedilla 'ç' => 'c'
                0xe9: u'e',     # e acute 'é' => 'e'
                0xe8: u'e',     # e grave 'è' => 'e'
                0xea: u'e',     # e circumflex 'ê' => 'e'
                0xed: u'i',     # i acute 'í' => 'i'
                0xec: u'i',     # i grave 'ì' => 'i'
                0xee: u'i',     # i circumflex 'î' => 'i'
                0xef: u'i',     # i diaeresis 'ï' => 'i'
                0xf1: u'n',     # n tilde 'ñ' => 'n'
                0xf2: u'o',     # o grave 'ò' => 'o'
                0xf3: u'o',     # o acute 'ó' => 'o'
                0xf5: u'o',     # o tilde   'õ' => 'o'
                0xf4: u'o',     # o circumflex 'ô' => 'o'
                0xf6: u'o',     # o diaeresis 'ï' => 'o'
                0xf9: u'u',     # u grave 'ù' => 'u'
                0xfa: u'u',     # u acute 'ú' => 'u'
                0xfb: u'u',     # u circumflex 'û' => 'u'
                0xfd: u'y'      # y acute 'ý' => 'y'
            }

            plast = dict['lastName']
            blast = unicode(x['booker_lastName'].lower())
            plast = unicode(plast.lower())
            blast = blast.translate(table).encode('ascii', 'ignore')
            plast = plast.translate(table).encode('ascii', 'ignore')
            #print >> open("testnames_traveller.log","a"), "blast=",blast,"plast=",plast
            

            if min([len(blast),len(plast)]) == 0 : return 0.0

            dist = StringMatcher.StringMatcher(None,blast,plast).distance()

            # take into accound doubled names (separated by ' ' or '-')
            if (dist > 0 and ((' ' in blast+plast) or ('-' in blast+plast))):
                import itertools
                import re
                delimiters = ' ', '-'
                regexPattern = '|'.join(map(re.escape, delimiters))
                blast_list =re.split(regexPattern, blast)
                plast_list =re.split(regexPattern, plast)
                #print >> open("testnames_list_travellers.log","a"), "blast=",blast_list,"plast=",plast_list
                distances = [  StringMatcher.StringMatcher(None,r[0],r[1]).distance()  for r in itertools.product(blast_list, plast_list)]
                dist = min(distances)
                


            return 1.0 - min(1.0,float(dist)/float( min([len(blast),len(plast)]) ))

        df2 = datasetframe.apply(LD,axis=1)
        datasetframe['LD_booker_first_traveller'] = df2
        return self

 def addLevenshteinAllTraveller(self,datasetframe=None):
 
        if (datasetframe is None): 
            if (self.dataframe  is not None ):  datasetframe = self.dataframe
            else: return self

        

        # Test 9: Add Levenshtein-Distance-Similarity between family names of booker and all travellers

 

        # we will use the similarity instead of the distance
        def LD(x,name):
            if x['booker_lastName'] == None  or len(x['booker_lastName'] ) == 0 or pd.isnull(x['booker_lastName']) : return 0.0
            if x[name] is None or pd.isnull(x[name]): return 0.0
            dict =   x[name]
            if not ('lastName' in dict.keys()):  return 0.0  
            
            # take into account german and other umlaut
            # see the table http://lwp.interglacial.com/appf_01.htm
            table = {
                0xe4: u'ae', # a diaeresis
                ord(u'ö'): u'oe',
                ord(u'ü'): u'ue',
                ord(u'ß'): u'ss',
                0xe1: u'a',  	# a acute 'á' => 'a'
                0xe0: u'a',     # a grave 'à' => 'a'
                0xe2: u'a',     # a circumflex  'â' => 'a'
                0xe3: u'a',     # a tilde 'ã' => 'a'
                0xe7: u'c',     # c cedilla 'ç' => 'c'
                0xe9: u'e',     # e acute 'é' => 'e'
                0xe8: u'e',     # e grave 'è' => 'e'
                0xea: u'e',     # e circumflex 'ê' => 'e'
                0xed: u'i',     # i acute 'í' => 'i'
                0xec: u'i',     # i grave 'ì' => 'i'
                0xee: u'i',     # i circumflex 'î' => 'i'
                0xef: u'i',     # i diaeresis 'ï' => 'i'
                0xf1: u'n',     # n tilde 'ñ' => 'n'
                0xf2: u'o',     # o grave 'ò' => 'o'
                0xf3: u'o',     # o acute 'ó' => 'o'
                0xf5: u'o',     # o tilde   'õ' => 'o'
                0xf4: u'o',     # o circumflex 'ô' => 'o'
                0xf6: u'o',     # o diaeresis 'ï' => 'o'
                0xf9: u'u',     # u grave 'ù' => 'u'
                0xfa: u'u',     # u acute 'ú' => 'u'
                0xfb: u'u',     # u circumflex 'û' => 'u'
                0xfd: u'y'      # y acute 'ý' => 'y'
            }

            plast = dict['lastName']
            blast = unicode(x['booker_lastName'].lower())
            plast = unicode(plast.lower())
            blast = blast.translate(table).encode('ascii', 'ignore')
            plast = plast.translate(table).encode('ascii', 'ignore')
            #print >> open("testnames_traveller.log","a"), "blast=",blast,"plast=",plast
            

            if min([len(blast),len(plast)]) == 0 : return 0.0

            dist = StringMatcher.StringMatcher(None,blast,plast).distance()

            # take into accound doubled names (separated by ' ' or '-')
            if (dist > 0 and ((' ' in blast+plast) or ('-' in blast+plast))):
                import itertools
                import re
                delimiters = ' ', '-'
                regexPattern = '|'.join(map(re.escape, delimiters))
                blast_list =re.split(regexPattern, blast)
                plast_list =re.split(regexPattern, plast)
                #print >> open("testnames_list_travellers.log","a"), "blast=",blast_list,"plast=",plast_list
                distances = [  StringMatcher.StringMatcher(None,r[0],r[1]).distance()  for r in itertools.product(blast_list, plast_list)]
                dist = min(distances)
                


            return 1.0 - min(1.0,float(dist)/float( min([len(blast),len(plast)]) ))

        def LD_ALL(x):
    
            a=[ key for key, value in x.iteritems() if ('traveller_' in key)  and (value != None) and ( type(value) == type({}) ) ]

            if len(a) == 0: return 0.0            
            b = [ LD(x,key)  for key in a  ]
            return max(b)
            
        

        df2 = datasetframe.apply(LD_ALL,axis=1)
        datasetframe['LD_booker_all_travellers'] = df2
        return self


 def addNumberTravellers(self,datasetframe=None):


        if (datasetframe is None): 
            if (self.dataframe  is not None ):  datasetframe = self.dataframe
            else: return self

        # Test 10: Add the number of travellers to the DataFrame 
        def f2(x):
            a=dict((key,value) for key, value in x.iteritems() if 'traveller_' in key and value != None and  type(value) == type({}) and not pd.isnull(x[key])  )            
            #print a
            #TransformDataset.counter+=1
            #print "TransformDataset: processed %d "%TransformDataset.counter
            #print len(a)
            return len(a)   

        df2 = datasetframe.apply(f2,axis=1)
        datasetframe['Number_of_travellers'] = df2
        return self

 def addCountryMatches(self,datasetframe=None):

    if (datasetframe is None): 
            if (self.dataframe  is not None):  datasetframe = self.dataframe
            else: return self

    
    # Test 11: Add the case when IPCountry  != BinCountry of the booker and payer to the DataFrame

    def f3(x):
        if x['booker_ipCountry'] is None or pd.isnull(x['booker_ipCountry']): return np.nan
        if x['booker_country'] is None or pd.isnull(x['booker_country']) : return np.nan  
        return int(x['booker_ipCountry'] == x['booker_country'])
   

    def f4(x):
        if x['booker_country'] is None  or pd.isnull(x['booker_country'])  : return np.nan
        if x['payer_binCountry'] is None or pd.isnull(x['payer_binCountry']) : return np.nan  
        return int(x['booker_country'] == x['payer_binCountry'])

    def f5(x):
        if x['booker_ipCountry']  is None or pd.isnull(x['booker_ipCountry'])  : return np.nan
        if x['payer_binCountry']  is None  or pd.isnull(x['payer_binCountry']): return np.nan 
        return int(x['booker_ipCountry'] == x['payer_binCountry'])


    df3 = datasetframe.apply(f3,axis=1)
    df4 = datasetframe.apply(f4,axis=1)
    df5 = datasetframe.apply(f5,axis=1)
    datasetframe['booker_ipCountry_country'] = df3
    datasetframe['booker_country_payer_binCountry'] = df4
    datasetframe['booker_ipCountry_payer_binCountry'] = df5

    return self

 def addRoute(self,datasetframe=None):

    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self

    # Test 12: Add route information like departureAirport -- arrivalAirport etc to the DataFrame
    def f6(x):
        if x['product_departureAirport'] is None or pd.isnull(x['product_departureAirport']): return np.nan
        if x['product_arrivalAirport'] is None or pd.isnull(x['product_arrivalAirport']) : return np.nan  
        return str(x['product_departureAirport']) + '-' + str(x['product_arrivalAirport'])

    df6 = datasetframe.apply(f6,axis=1)
    datasetframe['route'] = df6
    

    return self

 def addIsEmail(self,datasetframe=None):
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['booker_email'] is None or pd.isnull(x['booker_email']): return 0        
        return 1

    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['is_email'] = df7
    return self


 def addIsPhone(self,datasetframe=None):

    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['booker_phone'] is None or pd.isnull(x['booker_phone']): return 0        
        return 1

    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['is_phone'] = df7
    return self



 def addIsBithday(self,datasetframe=None):

    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['booker_birthday'] is None or pd.isnull(x['booker_birthday']): return 0        
        return 1

    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['is_birthday'] = df7
    return self


 def addIsZipCode(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['booker_zipcode'] is None or pd.isnull(x['booker_zipcode']): return 0        
        return 1

    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['is_zipcode'] = df7
    return self

 def addBookerAge(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['booker_birthday'] is None or pd.isnull(x['booker_birthday']): return -100        
        return TimeConvert.TimeConvert.age(x['booker_bookingDate'].split()[0],x['booker_birthday'],format='%d.%m.%Y')  


    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['booker_age'] = df7
    return self

 def addFirstTravellerAge(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['traveller_0'] is None or pd.isnull(x['traveller_0']): return -100    
        
        dict =   x['traveller_0']
        if not ('birthday' in dict.keys()):  return -100         
        return TimeConvert.TimeConvert.age(x['booker_bookingDate'].split()[0],dict['birthday'],format='%d.%m.%Y')  
        


    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['traveller_first_age'] = df7
    return self

 def addAverageTravellerAge(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):            
        a=dict((key,value) for key, value in x.iteritems() if ('traveller_' in key)  and (value != None))
        if len(a) == 0: return -100    
        count = 0.
        totalAge = 0. 
        for key,value in a.items():
            if type(value) != type({}): continue
            if 'birthday' in value.keys():
                age = TimeConvert.TimeConvert.age(x['booker_bookingDate'].split()[0],value['birthday'],format='%d.%m.%Y')
                totalAge += age if age >0 else 0.
                count += 1. if age >0 else 0.
        return float(totalAge)/float(count) if count >0 else -100 
        



    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['traveller_avg_age'] = df7
    return self

 def addTotalTravellerAge(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):        
    #and (not('traveller_first_age' in key))
        a=dict((key,value) for key, value in x.iteritems() if ('traveller_' in key)  and (value != None))
        if len(a) == 0: return -100            
        totalAge = 0. 
        for key,value in a.items():
            if type(value) != type({}): continue
            if 'birthday' in value.keys():
                age = TimeConvert.TimeConvert.age(x['booker_bookingDate'].split()[0],value['birthday'],format='%d.%m.%Y')
                totalAge += age if age >0 else 0.                
        return float(totalAge)
          
    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['traveller_total_age'] = df7
    return self


 def addProductPrice(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['product_price'] is None or pd.isnull(x['product_price']): return -100        
        return float(x['product_price']) if float(x['product_price']) <20000 else 20000


    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['product_price'] = df7
    return self
 


 def addBookerEmailInformation(self,datasetframe=None):

    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self

    def f7(x):
        if x['booker_email'] is None or pd.isnull(x['booker_email']): return np.nan        
        return x['booker_email'].split('@')[1].split('.')[-1].lower()

    def f8(x):
        if x['booker_email'] is None or pd.isnull(x['booker_email']): return np.nan        
        emailCountry = x['booker_email'].split('@')[1].split('.')[-1].lower()
        
        # some table to correct user emails
        table = { 'googlemail':'gmail', 'google':'gmail' } 
        provider = x['booker_email'].split('@')[1].lower().replace('.'+emailCountry,'')         
        for key, value in table.items(): provider = provider.replace(key,value)        
        #return x['booker_email'].split('@')[1].replace('.'+emailCountry,'')        
        return provider
        

    df7 = datasetframe.apply(f7,axis=1)
    df8 = datasetframe.apply(f8,axis=1)
    
    datasetframe['booker_email_country'] = df7
    datasetframe['booker_email_provider'] = df8

 
 


    return self

 counter = 0
 def addValidation(self,datasetframe=None):
    ''' might be time consuming '''
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self

    def f7(x):
        
        #if x['booker_ip'] is None or pd.isnull(x['booker_ip']): return np.nan                                                                                   )
        ip = x['booker_ip']
        street= '' if  x['booker_street'] is None or pd.isnull(x['booker_street']) else x['booker_street'].lower()
        city= '' if  x['booker_city'] is None or pd.isnull(x['booker_city']) else x['booker_city'].lower()
        country_code= '' if  x['booker_country'] is None or pd.isnull(x['booker_country']) else x['booker_country'].lower()
        country=FPSGeoIP.FPSGeoIP.country_codes[country_code] if  country_code != '' else ''
        zipcode= '' if  x['booker_zipcode'] is None or pd.isnull(x['booker_zipcode']) else x['booker_zipcode']
        depArpt= '' if  x['product_departureAirport'] is None or pd.isnull(x['product_departureAirport']) else x['product_departureAirport']
        arrArpt= '' if  x['product_arrivalAirport'] is None or pd.isnull(x['product_arrivalAirport']) else x['product_arrivalAirport']
        

        
        
        result = FPSGeoIP.FPSGeoIP.calculate_and_validate(ip=ip,street=street,city=city,country=country,zipcode=zipcode,airport1=depArpt,airport2=arrArpt)
    # may give    
    # ((('city', 0), ('country', 0), ('zipcode', 0), ('street', 0)), (('city', 1), ('country', 1), ('zipcode', 1), ('street', 1)), 4189.165237974013, 8492.920179621253, 10767.657119888268)
        # calculate score: important: country, city   and zipcode
        

        TransformDataset.counter+=1
        print "TransformDataset: processed %d "%TransformDataset.counter
        print result
        return (
                (result[0][1][1]+(2 if result[0][0][1]>0 else 0) + (4 if result[0][2][1]>0 else 0) ),
                (result[1][1][1]+(2 if result[1][0][1]>0 else 0) + (4 if result[1][2][1]>0 else 0) + (8 if result[1][3][1]>0 else 0)  ),
                result[2],
                result[3],
                result[4]
                )



    df7 = datasetframe.apply(f7,axis=1)
    #print df7
    df8 = df7.apply(lambda x: x[0])
    df9 = df7.apply(lambda x: x[1])
    df10 = df7.apply(lambda x: x[2])
    df11 = df7.apply(lambda x: x[3])
    df12 = df7.apply(lambda x: x[4])
    
    
    datasetframe['booker_valid_ip'] = df8
    datasetframe['booker_valid_addr'] = df9
    datasetframe['airport_dist'] = df10
    datasetframe['booker_depairport_dist'] = df11
    datasetframe['booker_arrairport_dist'] = df12

    return self

 
 def addValidateZipCode(self,datasetframe=None):
  
    if (datasetframe is None): 
            if (self.dataframe is not None):  datasetframe = self.dataframe
            else: return self
    def f7(x):
        if x['booker_zipcode'] is None or pd.isnull(x['booker_zipcode']): return 0
        if x['booker_country'] is None or pd.isnull(x['booker_country']): return 0
        if x['booker_city'] is None or pd.isnull(x['booker_city']): return 0
        result=FPSGeoIP.FPSGeoIP.is_zipcode_correct( zipcode=x['booker_zipcode'],city= x['booker_city'], country=x['booker_country'])

        # may result to # (('city', 0), ('zipcode', 0)) or (('city', 0), ('zipcode', 1)) or (('city', 1), ('zipcode', 1))

        TransformDataset.counter+=1
        print "TransformDataset: processed %d "%TransformDataset.counter
        print result
        
        return int(str(result[1][1])+str(result[0][1]),2) # return bit-wise word

    df7 = datasetframe.apply(f7,axis=1)
      
    datasetframe['is_zipcode_correct'] = df7
    return self

    

if __name__ == '__main__':

 #Test 1: perform all needed transformation
 ds = Dataset.Dataset()
 ds.loadJson("fps_data_1.json") 
 td = TransformDataset(ds.createDataFrame())
 td.addIsEmail().addIsPhone().addIsBithday().addIsZipCode().addBookerAge().imputtingColumns([
    'booker_ipCountry', 
    'booker_country',
    'payer_binCountry',
    'product_departureAirport',
    'product_arrivalAirport',
    ]).addBooking_period().addLevenshtein().addNumberTravellers().addCountryMatches().addRoute().addBookerEmailInformation()
 

 # list of interesting data columns
 tosave=['booker_ipCountry',
    'booker_country',
    'payer_binCountry',
    'product_departureAirport',
    'product_arrivalAirport',
    'booker_email_country',
    'booker_email_provider', 'booker_ipCountry_country', 'booker_country_payer_binCountry','booker_ipCountry_payer_binCountry',
    'LD_booker_payer','booking_period','Number_of_travellers','isHit','score','booker_lastName','payer_lastName','route',
    'product_price','is_zipcode','is_birthday','is_phone','is_email','booker_age'
    ]


 tosave2=['LD_booker_payer','booker_lastName','payer_lastName','booker_email_country','booker_email_provider']
 tosave2=['product_price','is_zipcode','is_birthday','is_phone','is_email','booker_age']


    

 #print >> open("datatransform.log","w"),  td.dataframe[tosave].head(100) 

 from StringIO import StringIO
 import prettytable    
 output = StringIO()
 dataframe=td.dataframe[tosave2]
 #dataframe=td.dataframe[tosave]
 dataframe.to_csv(output, sep='\t', encoding='utf-8')
 output.seek(0)
 pt = prettytable.from_csv(output)
 print >> open("datatransform.log","w"), pt   ## for debugging


 






