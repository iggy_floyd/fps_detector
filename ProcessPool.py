
#from Queue import Queue
from multiprocessing import Process,Queue
from multiprocessing.managers import BaseManager,NamespaceProxy
import logging
import multiprocessing
import sys
import Logger

import dill


def run_dill_encoded(what):
        fun, args,kargs = dill.loads(what)
        return fun(*args,**kargs)

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          




class Worker(Process):
    """Process executing tasks from a given tasks queue"""
    def __init__(self, tasks):
        #multiprocessing.log_to_stderr(logging.DEBUG)
        Process.__init__(self)    
        self.tasks = tasks
        self.daemon = True
        self.start()
    
    def run(self):
        while True:
            #func, args, kargs = self.tasks.get()
            func,what = self.tasks.get()
            try: 
                #print func
                #func(*args, **kargs)
                func(what)
            except Exception, e: print e
            
            #print multiprocessing.current_process().name
            #sys.stdout.flush()
            #print multiprocessing.current_process().pid
            #sys.stdout.flush()
            #self.tasks.task_done()

class ProcessPool:
    """Pool of threads consuming tasks from a queue"""
    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        for _ in range(num_threads): Worker(self.tasks)

    def add_task(self, func, *args, **kargs):
        """Add a task to the queue"""
        #self.tasks.put((func, args, kargs))
        self.tasks.put((run_dill_encoded,dill.dumps((func, args,kargs))))






class O(object):
    def __init__(self):
        self.status=False
    
    def change_status(self,a,b='busy'):
        self.status=not(self.status)
        print a,b
        sys.stdout.flush()
        return self


class ProcessManager(BaseManager):
    pass


class OProxy(NamespaceProxy):
    # We need to expose the same __dunder__ methods as NamespaceProxy,
    # in addition to the b method.
    _exposed_ = ('__getattribute__', '__setattr__', '__delattr__', 'change_status')


    def change_status(self,a,b='busy'):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('change_status',[a,b])




if __name__ == '__main__':
    from random import randrange
    delays = [randrange(1, 10) for i in range(50)]
    
    from time import sleep
    def wait_delay(d,id,name="Arina"):

        print id,'-->',name,'is sleeping for (%d)sec' % d
        sys.stdout.flush()
        sleep(d)
    
    # 1) Init a Process pool with the desired number of threads
    pool = ProcessPool(20)
    
    for i, d in enumerate(delays):
        # print the percentage of tasks placed in the queue
        print '%.2f%c' % ((float(i)/float(len(delays)))*100.0,'%')
        
	name='Igor'
        # 2) Add the task to the queue
        pool.add_task(wait_delay, d,10,name="Igor")
    
    def work_with_object(obj,d,name=''):
        print "status:",obj.status
        sys.stdout.flush()
        sleep(d)
        print "new status:",obj.change_status(name,b='ready').status
        sys.stdout.flush()
        
    #ProcessManager.register('O',O,exposed=['change_status'])   
    ProcessManager.register('O', O, OProxy)
    PM=ProcessManager()
    PM.start()
    obj=PM.O()
    for i, d in enumerate(delays):
        # print the percentage of tasks placed in the queue
        print '%.2f%c' % ((float(i)/float(len(delays)))*100.0,'%')
        sleep(1)	
        # 2) Add the task to the queue
        print 'current ob status is ', obj.status 
        pool.add_task(work_with_object, obj,d,name='Igor')    

