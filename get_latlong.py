import requests
import json
import time

from urllib import urlencode
from urllib2 import urlopen
from yandex import Yandex
from osm import Osm
from bing import Bing
from keys import bing_key
from geonames import Geonames
from maxmind import Maxmind
from tomtom import Tomtom
from baidu import Baidu

class OpenMapQuest(object):
    """Geocoder using the MapQuest Open Platform Web Services."""
    
    def __init__(self, api_key='', format_string='%s'):
        """Initialize an Open MapQuest geocoder with location-specific
        address information, no API Key is needed by the Nominatim based
        platform.
        
        ``format_string`` is a string containing '%s' where the string to
        geocode should be interpolated before querying the geocoder.
        For example: '%s, Mountain View, CA'. The default is just '%s'.
        """
        
        self.api_key = api_key
        self.format_string = format_string
        self.url = "http://open.mapquestapi.com/nominatim/v1/search?format=json&%s"
    
    def geocode(self, string, exactly_one=True):
        params = {'q': self.format_string % string}
        url = self.url % urlencode(params)
        
        
        page = urlopen(url)
        
        return self.parse_json(page, exactly_one)
    
    def decode_page(self,page):
        ''' Return unicode string of geocoder results.     Nearly all services use JSON, so assume UTF8 encoding unless the
        response specifies otherwise.     '''
    
        if hasattr(page, 'read'): # urllib
        # note getparam in py2
            encoding = page.headers.getparam("charset") or "utf-8"
            return unicode(page.read(), encoding=encoding)
        else: # requests?
            encoding = page.headers.get("charset", "utf-8")
        return unicode(page.content, encoding=encoding)
    
    
    
    def parse_json(self, page, exactly_one=True):
        """Parse display name, latitude, and longitude from an JSON response."""
        if not isinstance(page, basestring):
            page = self.decode_page(page)
        resources = json.loads(page)
        
        if exactly_one and len(resources) != 1:
            from warnings import warn
            warn("Didn't find exactly one resource!" + \
                "(Found %d.), use exactly_one=False\n" % len(resources)
            )
            
        def parse_resource(resource):
            location = resource['display_name']
            
            latitude = resource['lat'] or None
            longitude = resource['lon'] or None
            if latitude and longitude:
                latitude = float(latitude)
                longitude = float(longitude)
            
            return (location, (latitude, longitude))
        
        if exactly_one:
            return parse_resource(resources[0])
        else:
            return [parse_resource(resource) for resource in resources]



def memodict(f):
    """ Memoization decorator for a function taking a single argument """
    
    
    __slots__ = ()

    class memodict(dict):
        def __missing__(self, key):
            self[key]=ret  = f(key)
            return ret 
    return memodict().__getitem__



@memodict
#def get_lat_long(urlend=''):
def get_lat_long(urlend):

        urlend=urlend.lower()
        # google API, fast, 100 ms
        
        urlbase = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address='	

	r = requests.get(urlbase+urlend) # request to google maps api
	r=r.json()
	if r.get('results'):
		for results in r.get('results'):
			latlong = results.get('geometry','').get('location','')
			latitude = latlong.get('lat','')
			longitude = latlong.get('lng','')
			break
		#print latitude, longitude
                return (latitude, longitude)

	else:
		print 'No results from Google'                
        # if no result from google, go to MapQuest  # too slow!!!    , 300-400 ms   
                #map = OpenMapQuest()
                #geodata = map.geocode(urlend)
                #return geodata[1]
                
                
                #location = Yandex(urlend,lang='en-US',timeout=0.1)   # slow --> 200 ms                                             
                #location = Osm(urlend,timeout=1.)   # slow --> 200 ms              --> 300 ms                                
                #location = Bing(urlend,key=bing_key)   # doesn't work                
                location = Geonames(urlend,timeout=0.1)   # slow  170 ms            
                if (location is None or not(location.ok)):
                    location =  Yandex(urlend,lang='en-US',timeout=0.1)
                    
                    
                    
                #location = Maxmind(urlend)   # doesn't work
                #location = Tomtom(urlend)   # very, very slow 450 - 500 ms                
                #location = Baidu(urlend)   # very, very,very slow 1s
                
                
                lat = float(location.lat) if (location.lat) else None
                lng = float(location.lng) if (location.lng) else None                                                
                return (lat,lng)
                
                
                
                
                
                
                

if __name__ == "__main__":

	for i in range(100):
		start = time.time()
		#print get_lat_long(urlend = 'Zurich,Switzerland')
                print get_lat_long('Zurich,ch')
		print  time.time() -start
