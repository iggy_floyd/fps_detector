#! /usr/bin/python



#  File:   Transformer.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on Apr 23, 2015, 12:59:09 PM


'''
   All possible features to be extracted from database or created during the dataframe transformation
   Model features. Only a set of all features should be used in the modeling

'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 23, 2015 12:59:09 PM$"



# Transformers of the model features
import Normalizer # my own transformer
from sklearn import preprocessing # a set of transformers from sklearn
from sklearn.pipeline import Pipeline,FeatureUnion
from TransformWrapper import TransformWrapperLambda, TransformWrapperTransformer # my own wrappers for sklearn transformers

from  copy import copy,deepcopy



class Features(object):
    '''features of the bookings '''
    
    
    @property
    def features(self):
        return  [
        
                    'status',
                    'booker_ipCountry',
                    'booker_country',
                    'payer_binCountry',
                    'product_departureAirport',
                    'product_arrivalAirport',
                    'booker_email_country',
                    'booker_email_provider', 'booker_ipCountry_country', 
                    'booker_country_payer_binCountry',
                    'booker_ipCountry_payer_binCountry',
                    'LD_booker_payer',
                    'Number_of_travellers',
                    'isHit',                                                            
                    'score',
                    'booking_period',
                    
              
                    'product_price',
                    'is_zipcode',
                    'is_birthday',
                    'is_phone',
                    'is_email',
                    'booker_age',
                    'traveller_first_age',
                    'traveller_avg_age',
                    'traveller_total_age',
                    'LD_booker_first_traveller',
                    'LD_booker_all_travellers',  
                    'product_airline',
                    'product_typeOfFlight',                    
                    'airport_dist',
                    #'booker_depairport_dist',
                    #'booker_arrairport_dist',
                    'phone_ok',
                    'phone_city_ok',
                    'phone_country_ok',
                    'booker_valid_phone',
                    
                    # unsupported now                                        
                    #'zipcode_country_ok',
                    #'zipcode_city_ok'

                    # unsupported now                    
                    #'booker_valid_ip',                                        
                    #'booker_valid_addr',
                                      
                  
           ]
           
    

    
    @property
    def model_features(self):        
        ''' as original designed transformers of the  model features'''
        
        self._model_features =  [
        
                ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
                ('LD_booker_payer',None),
                ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
                ('booker_valid_phone',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
                ('booking_period',preprocessing.MinMaxScaler()), # !!!
                ('LD_booker_all_travellers',None), # test !!!
                ('LD_booker_first_traveller',None), # bad
                ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
                ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
                ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
                ('booker_age',preprocessing.StandardScaler()), # bad 
                ('product_price',preprocessing.MinMaxScaler()), # bad 
                ('airport_dist',preprocessing.MinMaxScaler()),    
           ]
        return   self._model_features
    
    
    @model_features.setter
    def model_features(self,value):
        self._model_features = value
    

    @property
    def model_features_v2(self):
                ''' feature-transformations  based on StdScaler of the sklearn '''
                
                self._model_features_v2 = None
                status_pipeline =  Pipeline([
                        ('discr',TransformWrapperLambda( lambda x: 1 if x=='CHECKED_OK' else 0)),                        
                ]) 



                numeric_pipe=Pipeline([                    
                    ('float',TransformWrapperLambda(lambda x: float(x))),
                    ('std',preprocessing.StandardScaler())
                ])
                
                label_pipe=Pipeline([
                  ('lb',TransformWrapperTransformer(preprocessing.LabelEncoder())),
                  ('float',TransformWrapperLambda(lambda x: float(x))),
                  ('std',preprocessing.StandardScaler())
                ])


                self._model_features_v2 = [
        
                    ('status', status_pipeline),
                    ('LD_booker_payer',None),
                    ('traveller_avg_age',deepcopy(numeric_pipe)),  # !!!
                    ('booker_valid_phone',deepcopy(numeric_pipe)), # perhaps bad !!!
                    ('booking_period',deepcopy(numeric_pipe)), # !!!
                    ('LD_booker_all_travellers',None), # test !!!
                    ('LD_booker_first_traveller',None), # bad
                    ('product_typeOfFlight',deepcopy(label_pipe)), # perhaps bad !!!
                    ('product_arrivalAirport',deepcopy(label_pipe)), # !!!
                    ('product_departureAirport',deepcopy(label_pipe)), # !!!   
                    ('booker_age',deepcopy(numeric_pipe)), # bad 
                    ('product_price',deepcopy(numeric_pipe)), # bad 
                    ('airport_dist',deepcopy(numeric_pipe)),    
            ]

                return self._model_features_v2



    @model_features_v2.setter
    def model_features_v2(self,value):
        self._model_features_v2 = value
    
    
    def getFeatures(self):
        return self.features
    
    
    
    def __str__(self):        
        return  str(self.__class__)+'\n' +'\n'.join(self.features)
    
    




if __name__ == "__main__":
    # Test
    print Transformer()


  
                 